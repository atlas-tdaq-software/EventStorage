#ifndef ADLER32THREAD_H_
#define ADLER32THREAD_H_

#include <mutex>
#include <condition_variable>
#include <thread>

/* Class to encapsulate the thread logic
 * The implementation was simplified as much as possible in order to minimize
 * latencies.
 *
 * Usage:
 *   Adler32Thread ath;
 *   ath.compute_checksum(data, length); // request async checksum computation
 *
 *   // Do some other stuff while the thread compute the checksum in parallel
 *
 *   auto checksum = ath.get_result(); // blocking call
 *
 * If the result is not consumed (via a call to get_result) between two
 * calls to compute_checksum, it is lost.
 */
class Adler32Thread
{
public:
	Adler32Thread();
	Adler32Thread(const Adler32Thread &) = delete;
	Adler32Thread(Adler32Thread &&) = delete;

	// Stops the thread
	~Adler32Thread();

	void stop();

	// do not call with nullptr
	void compute_checksum(unsigned char const * data, int len);

	unsigned long get_result();

private:
	void compute_loop();

	unsigned char const * data_;
	int length_;
	unsigned long result_;
	bool exit_;
	std::mutex mutex_;
	std::condition_variable condvar_;
	std::thread thread_;
};

#endif /* ADLER32THREAD_H_ */
