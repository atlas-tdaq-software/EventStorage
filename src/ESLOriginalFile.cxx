#include "ESLOriginalFile.h"
#include "EventStorage/EventStorageIssues.h"
#include "DataReaderController.h"
#include "compression/compression.h"
#include "compression/DataBuffer.h"

#include <stdio.h>
#include <string.h>

ESLOriginalFile::ESLOriginalFile( fRead * m_fR) : EventStackLayer(m_fR)
{
  m_me = "ESLOriginal"; 

  ERS_DEBUG(3, identify() << " Constructor called ");
  
  uint64_t cpos = m_fR->getPosition();
  m_fR->setPositionFromEnd(0);
  m_cfilesize = m_fR->getPosition();
  m_fR->setPosition(cpos);

  m_endOfFile = false;
  m_fer_read = false;
  m_advance_fer_updated = false;

  m_compression = EventStorage::NONE;
  m_uncompressed = new compression::DataBuffer();

}
 
ESLOriginalFile::~ESLOriginalFile() 
{
  delete m_uncompressed;
  ERS_DEBUG(3, identify() <<" destroyed ") ; 
}
	
 
EventStackLayer* ESLOriginalFile::openNext()
{
  return 0;
  //does nothing
}


DRError ESLOriginalFile::getData(unsigned int &eventSize, char **event, int64_t pos, bool memAlreadyAlloc, int64_t allocSizeInBytes)
{
  ERS_DEBUG(2,"Entered ESLOriginalFile::getData().");
  eventSize=0;
  
  int64_t oldPos = m_fR->getPosition();
  if(pos>0) m_fR->setPosition(pos);


  data_separator_record dsr = data_separator_pattern;
  
  if(unfile_record(&dsr,&data_separator_pattern) == 0){
      ERS_DEBUG(1,"No Event Start record found.");
      m_fR->setPosition(oldPos);
      m_error=true;	
      EventStorage::ES_NoEventFound ci(ERS_HERE, "No Event Start record found at this position. ");
      throw ci;
  }

  eventSize=dsr.data_block_size;
  unsigned int blocksize = eventSize;
 
  uint64_t startofevent = m_fR->getPosition(); // we will need this in case of reading failure.
  
  ERS_DEBUG(2,"DATA SEPARATOR RECORD " 
	    << string_record(&dsr,&data_separator_pattern) << std::endl
	    << "Expected event size " << eventSize);
 
  char* buffer;
  if (!memAlreadyAlloc) {
    buffer = new char[eventSize];
    if(NULL == buffer) {
      EventStorage::ES_AllocatingMemoryFailed ci(ERS_HERE, "ESLOriginalFile tried to allocate memory but failed. Abort.");
      throw ci;
      
    }
    (*event) = buffer;
  } else {
    buffer = (*event);
    if (eventSize>allocSizeInBytes) {
      EventStorage::ES_AllocatedMemoryTooLittle ci(ERS_HERE, "The size of the memory block you gave to readData is insufficient. please increase it and try again. ");
      m_fR->setPosition(oldPos);
      throw ci;
    }
  }
 
 	
  //this bunch of code checks if we read beyond the end of the file
  uint64_t cpos = m_fR->getPosition();
  ERS_DEBUG(3, "currentposition" << cpos) ;

  uint64_t remaining = m_cfilesize - cpos;
  
  if (remaining < eventSize) {
    ERS_DEBUG(3,"Requested " << eventSize 
	      << " bytes but only remains " << remaining << " in file ");
    m_error = true;
    
    m_fR->readData(buffer,remaining);
    
    m_fR->setPosition(cpos);
    
    eventSize = remaining;
    
    EventStorage::ES_OutOfFileBoundary ci(ERS_HERE, "Trying to read more data than remains in the file. This could mean that either your event is truncated, or that the event size record of this event is corrupted. If you still want to read the data, catch this exception and proceed. The data block contains the rest of the data. ");
    throw ci;

    return DRNOOK;
  }
 	
  m_fR->setPosition(cpos);
  
  m_fR->readData(buffer,eventSize);
  
  ERS_DEBUG(3,"Finished reading the event.");

  //Decompress, if needed

  if (m_compression == EventStorage::ZLIB){

    uint32_t rawsize;

    try{
      compression::zlibuncompress(*m_uncompressed, 
				  rawsize,
				  buffer,
				  eventSize);
    }catch(compression::CompressionIssue& ex){
      ERS_LOG(ex);
      return DRNOOK;
    }
    

    eventSize = rawsize;

    if (!memAlreadyAlloc) {
      
      delete[] buffer;
      buffer = new char[rawsize];
      if(NULL == buffer) {
	EventStorage::ES_AllocatingMemoryFailed ci(ERS_HERE, "ESLOriginalFile tried to allocate memory but failed. Abort.");
	throw ci;
	
      }
      (*event) = buffer;
      ::memcpy(buffer, m_uncompressed->handle(), rawsize);
      
    }else{
      
      if (rawsize > allocSizeInBytes) {
	EventStorage::ES_AllocatedMemoryTooLittle ci(ERS_HERE, "The size of the memory block you gave to readData is insufficient. Please increase it and try again.");
	m_fR->setPosition(oldPos);
	throw ci;
      } else {
	::memcpy(buffer, m_uncompressed->handle(), rawsize);
      }
    }
  }

  m_latest_dsr = dsr;

  //CHECK FOR END OF FILE REACHED
  if( /*m_fR->isEoF()*/  m_cfilesize<=m_fR->getPosition()) {
     //isEoF only tells you an end of file if you already hit the eof. it doesn't tell you if ou are exactly at the limit. therefore we check like that
    ReadingIssue ci(ERS_HERE,"no event record found where expected. A problem with file format. The file may not have been properly closed.");
    ers::warning(ci); 
    m_error = true;
    m_finished = true;
		
    
    EventStorage::ES_NoEndOfFileRecord rci(ERS_HERE, "End of File reached directly after Event. No end of File record found where expected. The file may not have been properly closed. To proceed anyway, catch this exception. However, the requested Data has been read correctly.");
    throw rci;
	
    
    return DRNOOK;
  }

  //CHECK FOR VALID RECORD AFTERWARDS
  uint32_t nextword = m_fR->readuint32_t();
  //WE ARE EXPECTING either an EOF record or a dataseparator record
  
  ERS_DEBUG(3,"NEXTWORD "<< std::hex << nextword << std::dec);
	
  if (nextword != FILE_END_MARKER &&
      nextword != EVENT_RECORD_MARKER) {
    //this means we have a truncated event inside a file, or the event separator record didn't indicate the correct size
		
    //Starting recovery

    ERS_DEBUG(1,"Entering Recovery ");

		
    uint64_t recovery_offset_bytes = 0;
    uint64_t currentblocksize = eventSize;
    
    bool done = false; 
    
    uint32_t word_by_word;
    bool fullblockread = 0; //this variable holds the info if the full block of the user has already been read
    
    while(!done){
      if(!memAlreadyAlloc &&	
	 (recovery_offset_bytes+sizeof(uint32_t))> currentblocksize){
	ERS_DEBUG(2,"NEED MORE " << recovery_offset_bytes << " " 
		  << currentblocksize);

	uint64_t old_block_size = currentblocksize;
	
	currentblocksize *= 2; //double the block
	
	uint64_t remaininginfile = m_cfilesize - startofevent;
	currentblocksize = std::min(currentblocksize, remaininginfile);
	
	if ( currentblocksize > (100*1024*1024) ) {
	  EventStorage::ES_WrongEventSize rci(ERS_HERE, "EventStorage found an Event which is either truncated, or the event separator record does not indicate the correct size. Additional, no Data separator record or end of file record was found within the HARD LIMIT of 100MB from the start of the event. The data until this limit is present in the datablock you gave to readData. In order to use the data, catch this exception. ");
	  throw rci;				
	}
	
	if (currentblocksize == old_block_size) {
	  EventStorage::ES_WrongEventSize rci(ERS_HERE, "EventStorage found an Event which is either truncated, or the event separator record does not indicate the correct size. Additional, no Data separator record or end of file record was found. The data until the  end of the file is present in the datablock you gave to readData. In order to use the data, catch this exception. ");
	  throw rci;				
	}
				
	//we are now outside the self-made memory block. we need to read further.
	delete [] buffer; //first delete the old memory block
	buffer = new char [currentblocksize];
	(*event) = buffer;
	m_fR->setPosition(startofevent);
	m_fR->readData(buffer,currentblocksize);
	
	
      } else if(memAlreadyAlloc && 
		(recovery_offset_bytes+sizeof(uint32_t))> blocksize){
	// in this case, we will in one go fill the whole memory block
	
	if (fullblockread) {
	  EventStorage::ES_WrongEventSize rci(ERS_HERE, "EventStorage found an Event which is either truncated, or the event separator record does not indicate the correct size. Additional, no Data separator record or end of file record was found within the memory black which was given to readData . Either increase the size of the memory block, but it is more likely that the file is corrupted ");
	  throw rci;				
	}

	m_fR->setPosition(startofevent);
	int64_t remaininginfile = m_cfilesize - startofevent;
	
	ERS_DEBUG(2, allocSizeInBytes<< " " << remaininginfile << " bytes " );
	
	
	int64_t readm = std::min(allocSizeInBytes, remaininginfile);
	
	
	ERS_DEBUG(2,"prereading " << readm << " bytes " );
	m_fR->readData(buffer,readm);
	blocksize=readm;
	if(readm>= allocSizeInBytes) fullblockread = true;
      }

      ::memcpy(&word_by_word, buffer+recovery_offset_bytes, sizeof(uint32_t));

      if ( word_by_word == EVENT_RECORD_MARKER || 
	   word_by_word == FILE_END_MARKER ){
	eventSize = recovery_offset_bytes;
	//if next EOF/ESR is found, we need to set 
	//the position correctly in order to be bale 
	//to read further if exception was caught
	m_fR->setPosition(startofevent +  eventSize); 
	
	ERS_DEBUG(3,"Going to position after recovery "<< (startofevent +  eventSize) << ". Eventsize is " << eventSize);
		
	done = true;

	if (word_by_word == FILE_END_MARKER) {
	  m_finished = true;
	  m_endOfFile = true;
	  
	}
      }
      recovery_offset_bytes++;
    }

    ERS_DEBUG(3,"After recovery before throwing " << m_fR->getPosition());
    
    EventStorage::ES_WrongEventSize rci(ERS_HERE, "EventStorage found an Event which is either truncated, or the event separator record does not indicate the correct size. The data until the next data seperator record or end of file record is present in the datablock you gave to readData. In order to use the data, catch this exception. ");
  	throw rci;
	
  }
		
  // check for end of file record
  if(unfile_record(&m_latest_fer,&file_end_pattern) != 0) {
    ERS_DEBUG(3,"Found end of file record after an event.");
    m_endOfFile = true;
    m_fer_read = true;
    m_finished = true;
    
    // check end of run
  }

  return DROK;
}



EventStackLayer* ESLOriginalFile::loadAtOffset(int64_t position, EventStackLayer *old)
{
  if(old){}; //Avoid compilation warning

  if (m_cfilesize <=  position)
  {
 		EventStorage::ES_OutOfFileBoundary ci(ERS_HERE, "Trying to jump outside Filesize Boundary. Abort.");
  	throw ci;
 
  }
  m_fR->setPosition(position);
  return this;
}

	
void ESLOriginalFile::prepare()
{
  ERS_DEBUG(2,"Read metadata from the beginning of the file.");
		 
  file_start_record tmpfsr = file_start_pattern;
  tmpfsr.version = 0x0;
		 
		 
  if(unfile_record(&m_latest_fsr,&tmpfsr) == 0) {
      ERS_DEBUG(2,"File start record not found after fopen.");
      EventStorage::ES_WrongFileFormat ci(ERS_HERE, "File start record not found after fopen.. ESLOriginalFile reported: File Format is not known. Abort");
      throw ci;
  }
  
  ERS_DEBUG(2,"File start record found after fopen. version "<< m_latest_fsr.version);
  
  m_latest_fns=unfile_name_strings();
		
  if(m_latest_fns.appName=="there was") {
    ERS_DEBUG(3,"File name strings not found in file.");     
    EventStorage::ES_WrongFileFormat ci(ERS_HERE, "ESLOriginalFile reported: File name strings not found in file.");
    throw ci;
  } else {
    ERS_DEBUG(3,"File name strings found.");         
  }
		
  readOptionalFreeMetaDataStrings();
  
  checkCompression();

  if (m_latest_fsr.version == 0x2) {
    ERS_DEBUG(3,"Version 2 found. Switching to compability mode.");
    
    v2_internal_run_parameters_record v2_internal_rpr;
    if(unfile_record(&v2_internal_rpr,&v2_run_parameters_pattern) == 0) {
      ERS_DEBUG(3,"Run parameters record not found in file.");
      EventStorage::ES_WrongFileFormat ci(ERS_HERE, "ESLOriginalFile reported: Old Run parameters record not found in file.");
      throw ci;
    } else {
      ERS_DEBUG(3,"Run parameters record found.");
      m_internal_latest_rpr.marker=v2_internal_rpr.marker;              
      m_internal_latest_rpr.record_size=v2_internal_rpr.record_size;
      m_internal_latest_rpr.run_number=v2_internal_rpr.run_number;          
      m_internal_latest_rpr.max_events=v2_internal_rpr.max_events;          
      m_internal_latest_rpr.rec_enable=v2_internal_rpr.rec_enable;          
      m_internal_latest_rpr.trigger_type=v2_internal_rpr.trigger_type;        
      m_internal_latest_rpr.detector_mask_1of4=v2_internal_rpr.detector_mask;
      m_internal_latest_rpr.detector_mask_2of4=0;
      m_internal_latest_rpr.detector_mask_3of4=0;
      m_internal_latest_rpr.detector_mask_4of4=0;
      m_internal_latest_rpr.beam_type=v2_internal_rpr.beam_type;           
      m_internal_latest_rpr.beam_energy=v2_internal_rpr.beam_energy;
    }

  } else if (m_latest_fsr.version == 0x5) {
    ERS_DEBUG(3,"Version 5 found. Switching to compability mode.");

    v5_internal_run_parameters_record v5_internal_rpr;
    if(unfile_record(&v5_internal_rpr,&v5_run_parameters_pattern) == 0) {
      ERS_DEBUG(3,"Run parameters record not found in file.");
      EventStorage::ES_WrongFileFormat ci(ERS_HERE, "ESLOriginalFile reported: Old Run parameters record not found in file.");
      throw ci;
    } else {
      ERS_DEBUG(3,"Run parameters record found.");
      m_internal_latest_rpr.marker=v5_internal_rpr.marker;              
      m_internal_latest_rpr.record_size=v5_internal_rpr.record_size;
      m_internal_latest_rpr.run_number=v5_internal_rpr.run_number;          
      m_internal_latest_rpr.max_events=v5_internal_rpr.max_events;          
      m_internal_latest_rpr.rec_enable=v5_internal_rpr.rec_enable;          
      m_internal_latest_rpr.trigger_type=v5_internal_rpr.trigger_type;      
      m_internal_latest_rpr.detector_mask_1of4=v5_internal_rpr.detector_mask_1of2;
      m_internal_latest_rpr.detector_mask_2of4=v5_internal_rpr.detector_mask_2of2;
      m_internal_latest_rpr.detector_mask_3of4=0;
      m_internal_latest_rpr.detector_mask_4of4=0;
      m_internal_latest_rpr.beam_type=v5_internal_rpr.beam_type;           
      m_internal_latest_rpr.beam_energy=v5_internal_rpr.beam_energy;
    }

  } else if (m_latest_fsr.version == 0x6) {
    ERS_DEBUG(3,"New version found.");
    
    if(unfile_record(&m_internal_latest_rpr,&run_parameters_pattern) == 0) {
      ERS_DEBUG(1,"Run parameters record not found in file.");
      EventStorage::ES_WrongFileFormat ci(ERS_HERE, "ESLOriginalFile reported: Run parameters record not found in file.");
      throw ci;
    } else {
      ERS_DEBUG(3,"Run parameters record found.");
    }
  } else {
    EventStorage::ES_WrongFileFormat ci(ERS_HERE, "ESLOriginalFile reported: File Format Version is not known. Abort");
    throw ci;
  }

  ERS_DEBUG(2,"All metadata OK at the beginning of the file.");

  if(unfile_record(&m_latest_fer,&file_end_pattern) != 0) 
    {
      //ERS_WARNING("No event data. This file contains only metadata. "
      //  <<"End of file metadata found after the run parameters record.");
      ERS_DEBUG(3,"End Record right after metadata. This OriginalFile contains only metadata.");

      m_endOfFile = true;
      m_fer_read = true;
	
    }
  ERS_DEBUG(1,"File " << fileName() << " ready for reading."); 
}

bool ESLOriginalFile::doneLoading()
{
  return true; //an original file is not further merging anything
}

	
bool ESLOriginalFile::moreEvents() 
{
  ERS_DEBUG(3, m_me <<" more events: "<< !m_endOfFile) ; 
  return (!m_endOfFile && !m_error);
}


void ESLOriginalFile::readOptionalFreeMetaDataStrings() 
{
	
  ERS_DEBUG(3,"See if we have metadata strings.");
	
  int64_t pos = m_fR->getPosition();
	
  uint32_t tst;
  m_fR->readData((char *)(&tst),sizeof(uint32_t));
	
  if(tst != EventStorage::free_strings_marker) {
    ERS_DEBUG(3,"Optional block of strings is not present.");
    m_fR->setPosition(pos);
    return;
  }
		
  m_fmdStrings.clear();
  uint32_t nstrings=0;
  m_fR->readData((char *)(&nstrings),sizeof(uint32_t));
	
  ERS_DEBUG(3,"Expect " << nstrings << " metadata strings.");
	
  for(unsigned int i=0; i!=nstrings; ++i) {
    // read one string
    uint32_t slen=0;
    m_fR->readData((char *)(&slen),sizeof(uint32_t));
    char *cst=new char[slen];
    m_fR->readData(cst,slen);
	
    std::string st(cst,slen);
    m_fmdStrings.push_back(st);
    ERS_DEBUG(3,"String " << i 
	      << " of length " << slen 
	      << " is " << st);
			
    delete[] cst;
	
    // 4-byte align
    char buf[4];
    char ns = slen % 4;
    if(ns) m_fR->readData(buf,4-ns); 
  }
}

void ESLOriginalFile::checkCompression()
{
  
  std::string comp = extractFromMetaData(EventStorage::compressiontag+"=");

  if(comp != ""){
    m_compression = string_to_type(comp);
    ERS_DEBUG(1,"Compressed file found. FMS: "<< comp << " Type: " 
	      << m_compression);
  }
}


unsigned int ESLOriginalFile::fileStartDate() const 
{
  return m_latest_fsr.date;
}

unsigned int ESLOriginalFile::fileStartTime() const 
{
  return m_latest_fsr.time; 
}


std::vector<std::string> ESLOriginalFile::freeMetaDataStrings() const
{
  return m_fmdStrings;
}

EventStorage::CompressionType ESLOriginalFile::compression() const {
  return m_compression;
}

//Run parameter get records
unsigned int ESLOriginalFile::runNumber() const {
  return m_internal_latest_rpr.run_number;
}

     
unsigned int ESLOriginalFile::maxEvents() const {
  return  m_internal_latest_rpr.max_events;
}

     
unsigned int ESLOriginalFile::recEnable() const {
  return  m_internal_latest_rpr.rec_enable;
}

     
unsigned int ESLOriginalFile::triggerType() const {
  return m_internal_latest_rpr.trigger_type;
}

   
std::bitset<128> ESLOriginalFile::detectorMask() const {
    
  uint64_t tmp = m_internal_latest_rpr.detector_mask_4of4;
  tmp <<= 32;
  tmp |= m_internal_latest_rpr.detector_mask_3of4;
  
  std::bitset<128> mask(tmp);
  mask <<= 64;
  
  tmp = m_internal_latest_rpr.detector_mask_2of4;
  tmp <<= 32;
  tmp |= m_internal_latest_rpr.detector_mask_1of4;

  mask |= tmp;
  return mask;
}

unsigned int ESLOriginalFile::beamType() const {
  return m_internal_latest_rpr.beam_type;
}

unsigned int ESLOriginalFile::beamEnergy() const {
  return  m_internal_latest_rpr.beam_energy;
}


unsigned int ESLOriginalFile::fileEndDate() 
{

  if(!m_advance_fer_updated) {
    m_latest_fer = currentFileFER();
    m_advance_fer_updated = true;
  }

  return m_latest_fer.date;
}

    
unsigned int ESLOriginalFile::fileEndTime() 
{

  if(!m_advance_fer_updated) {
    m_latest_fer = currentFileFER();
    m_advance_fer_updated = true;
  }

  return m_latest_fer.time;
}

    
unsigned int ESLOriginalFile::eventsInFile() 
{

  if(!m_advance_fer_updated) {
    m_latest_fer = currentFileFER();
    m_advance_fer_updated = true;
  }

  return m_latest_fer.events_in_file;
}

 
unsigned int ESLOriginalFile::dataMB_InFile() 
{

  if(!m_advance_fer_updated) {
    m_latest_fer = currentFileFER();
    m_advance_fer_updated = true;
  }

  return m_latest_fer.data_in_file;
}

   
unsigned int ESLOriginalFile::eventsInFileSequence() 
{

  if(!m_advance_fer_updated) {
    m_latest_fer = currentFileFER();
    m_advance_fer_updated = true;
  }

  return m_latest_fer.events_in_run; 
}

  
unsigned int ESLOriginalFile::dataMB_InFileSequence() 
{

  if(!m_advance_fer_updated) {
    m_latest_fer = currentFileFER();
    m_advance_fer_updated = true;
  }

  return m_latest_fer.data_in_run;
}


unsigned int ESLOriginalFile::fileSizeLimitInDataBlocks() const 
{
  return m_latest_fsr.sizeLimit_dataBlocks; 
}

unsigned int ESLOriginalFile::fileSizeLimitInMB() const 
{
  return m_latest_fsr.sizeLimit_MB; 
}

std::string ESLOriginalFile::appName() const 
{
  return m_latest_fns.appName;
}

std::string ESLOriginalFile::fileNameCore() const 
{
  return m_latest_fns.fileNameCore;
}




    
unsigned int ESLOriginalFile::fileStatusWord() 
{

  if(!m_advance_fer_updated) {
    m_latest_fer = currentFileFER();
    m_advance_fer_updated = true;
  }

  return m_latest_fer.status; 
}

bool ESLOriginalFile::endOfFileSequence()
{
  if (m_error) return true;
  ERS_DEBUG(3, identify() << " status word has value " << this->fileStatusWord());
  return this->fileStatusWord();

}

unsigned int ESLOriginalFile::nextInSequence(unsigned int current)
{
  return current+1; //stupid, but original file format does not support next/prev
}

	
file_end_record ESLOriginalFile::currentFileFER() {
  ERS_DEBUG(3,"ESLOriginalFile::currentFileFER() called.");

  int64_t pos = m_fR->getPosition();

  //forward to the end and reverse a bit now and 
  int64_t back=sizeof(file_end_pattern);
  m_fR->setPositionFromEndSafe(-back);

  file_end_record cfFER=file_end_pattern;

  if (unfile_record(&cfFER,&file_end_pattern)!= 0)
    {
      m_advance_fer_updated = true;
    };
  
  // rewind back
  m_fR->setPosition(pos);

  if(cfFER.date==0)
    {
      ERS_DEBUG(3,"No end of file record found");
  		EventStorage::ES_NoEndOfFileRecord ci(ERS_HERE, "ESLOriginalFile reported: File end record not found in file. If you still want to read data from this file, either catch these excpetions or don't ask for information inside the EOF Record");
  		throw ci;

    }
  return cfFER;
}

internal_run_parameters_record ESLOriginalFile::getRunParameterrecord()
{
  return m_internal_latest_rpr;
}

std::string ESLOriginalFile::GUID() const 
{
  ERS_DEBUG(2,"DataReaderBase::GUID() called.");
  if(m_fmdStrings.size()==0) {
    ERS_DEBUG(2,"No metadata strings in this file. Return empty GUID string.");
    return std::string("");
  }
  std::string guid = m_fmdStrings[0];
  std::string ptn("GUID=");
  std::string::size_type pos=guid.find(ptn);
  if(pos==std::string::npos) {
    ERS_DEBUG(2,"No GUID in metadata strings of this file. Return empty GUID string.");
    return std::string("");
  }
  pos+=ptn.size();
  guid=guid.substr(pos,guid.size());
 
  ERS_DEBUG(2,"Returning "<<guid);
  return guid;
}

uint32_t ESLOriginalFile::lumiblockNumber()
{
  ERS_DEBUG(2,"DataReaderBase::lumiblockNumber() called.");
  std::string lumistring = extractFromMetaData("LumiBlock=");
  if (lumistring =="")
    {	
      ERS_DEBUG(2,"No Lumiblock Info found. returning 0");
      return 0;
    }
  std::istringstream i(lumistring);
  i >> m_lumiblockNumber;

  return m_lumiblockNumber;
}

std::string ESLOriginalFile::stream()
{
  m_stream = extractFromMetaData("Stream=");
  return m_stream;
}

std::string ESLOriginalFile::projectTag()
{
  m_projectTag = extractFromMetaData("Project=");
  return m_projectTag;
}


file_name_strings ESLOriginalFile::unfile_name_strings() 
{

  int64_t pos = m_fR->getPosition();

  uint32_t tst;

  EventStorage::file_name_strings nst;
  nst.appName="there was"; nst.fileNameCore="a problem";

  m_fR->readData((char *)(&tst),sizeof(uint32_t));
  if(tst != EventStorage::file_name_strings_marker) {
    m_fR->setPosition(pos);
    return nst;
  }

  uint32_t size=0;
  m_fR->readData((char *)(&size),sizeof(uint32_t));

  char *csn=new char[size];
  m_fR->readData(csn,size);
  std::string name(csn,size);
  delete [] csn;

  char buf[4];
  char ns = size % 4;
  if(ns) m_fR->readData(buf,4-ns);

  m_fR->readData((char *)(&size),sizeof(uint32_t));
  
  char *cst=new char[size];
  m_fR->readData(cst,size);
  std::string tag(cst,size);
  delete [] cst;

  ns = size % 4;
  if(ns) m_fR->readData(buf,4-ns);

  nst.appName = name;
  nst.fileNameCore = tag;

  return nst;
}

std::string ESLOriginalFile::extractFromMetaData(std::string token)
{
  ERS_DEBUG(2,"DataReaderBase::extractFromMetaData() called: " << token);
  if(m_fmdStrings.size()==0) 
    {
      ERS_DEBUG(2,"No metadata strings in this file. Return empty string");
      return "";
    }
  for (unsigned int i=0; i< m_fmdStrings.size(); i++)
    {
      std::string cs = m_fmdStrings.at(i);
      std::string::size_type pos=cs.find(token);
      if(pos==std::string::npos) 
  	{ /*not foundokay */}
      else
  	{
	  pos+=token.size();
	  std::string result=cs.substr(pos,cs.size());
	  return result;
  	}
    }
  ERS_DEBUG(2,"No "<< token <<" found in metadata strings of this file. Return emptystring.");
  return "";
}

