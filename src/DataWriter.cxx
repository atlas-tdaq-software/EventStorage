#include <time.h>
#include <sstream>
#include <stdlib.h>
#include "Guid.h"
#include "ers/ers.h"

#include <boost/shared_ptr.hpp>

#include "EventStorage/DataWriter.h"
#include "EventStorage/DataWriterCallBack.h"
#include "EventStorage/FileNameCallback.h"
#include "EventStorage/RawFileName.h"
#include "EventStorage/EventStorageIssues.h"
#include "EventStorage/SimpleFileName.h"
#include "compression/DataBuffer.h"
#include "compression/compression.h"
#include "./EventStorageInternalRecords.h"
#include "Adler32MT.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>

using namespace std; 
using namespace EventStorage; 

class DataWriter::impl {
  
public:
  impl(const std::string writingPath,     
       const std::string fileNameCore,    
       const run_parameters_record rPar, 
       const std::vector<std::string> fmdStrings, 
       const unsigned int startIndex = 1,
       const CompressionType compression = NONE,
       const unsigned int compLevel = 1,
       const int adler32mt_nb_threads = Adler32MTDefaults::NB_THREADS,
       const uint32_t adler32mt_threshold = Adler32MTDefaults::THRESHOLD);

  impl(const std::string writingPath,   
       boost::shared_ptr<FileNameCallback> theFNCB,   
       const run_parameters_record rPar, 
       const std::string project,
       const std::string streamType,
       const std::string streamName,
       const std::string stream,
       const unsigned int lumiBlockNumber,
       const std::string applicationName,
       const std::vector<std::string> fmdStrings,
       const CompressionType compression = NONE,
       const unsigned int compLevel = 1,
       const int adler32mt_nb_threads = Adler32MTDefaults::NB_THREADS,
       const uint32_t adler32mt_threshold = Adler32MTDefaults::THRESHOLD);

  impl(const std::string writingPath,   
       const std::string fileNameCore,     
       const run_parameters_record rPar, 
       const std::string project,
       const std::string streamType,
       const std::string streamName,
       const std::string stream,
       const unsigned int lumiBlockNumber,
       const std::string applicationName,
       const std::vector<std::string> fmdStrings,
       const CompressionType compression = NONE,
       const unsigned int compLevel = 1,
       const int adler32mt_nb_threads = Adler32MTDefaults::NB_THREADS,
       const uint32_t adler32mt_threshold = Adler32MTDefaults::THRESHOLD);
  
  ~impl();

private:
  impl();

  void initDW(const std::string writingPath,   
	      boost::shared_ptr<FileNameCallback> theFNCB,   
	      const run_parameters_record rPar, 
	      const std::string project,
	      const std::string streamType,
	      const std::string streamName,
	      const std::string stream,
	      const unsigned int lumiBlockNumber,
	      const std::string applicationName,
	      const std::vector<std::string> fmdStrings,
	      const CompressionType compression,
	      const unsigned int compLevel);


  DWError putData_implementation(const unsigned int& entries, 
				 const struct iovec * my_iovec, 
				 uint32_t& sizeToDisk,
				 bool precompressed = false);


  // date and time utility, other private methods
  void date_timeAsInt(uint32_t &getDate, uint32_t &getTime) const;
  void openNextFile();
  bool fileExists(const std::string& name) const;
  void setRunParamsRecord(const run_parameters_record& rPar);

  //methods to write records to files
  void file_record(void *ri, const void *pi);
  void file_record(const EventStorage::file_name_strings& nst);
  void file_record(const EventStorage::freeMetaDataStrings& fmdStrings);

  // some parsing is needed to fill metadata
  std::string getAppName(const std::string& fileNameCore) const;
  std::string getElement(const std::string& fileNameCore, 
			 const std::string& element) const;
  uint64_t getFileSize(const std::string& fileNameCore) const;

  //methods to add the Global Unique ID to the files
  void spaceForGuid();
  void replaceGuid();

public:
  DWError setMaxFileNE(const unsigned int& maxFileNE);    

  DWError setMaxFileMB(const unsigned int& maxFileMB);    

  bool good() const; // check if open and last write went OK

  void cd(const std::string& writingPath);

  bool inTransition() const;

  DWError putData(const unsigned int& dataSize, const void *event);

  DWError putPrecompressedData(const unsigned int& dataSize, 
			       const void *event);

  DWError putPrecompressedData(const unsigned int& entries, 
			       const struct iovec* my_iovec);

  DWError putData(const unsigned int& entries, const struct iovec* my_iovec);

  DWError putData(const unsigned int& dataSize, const void *event, uint32_t& sizeToDisk);

  DWError putData(const unsigned int& entries, const struct iovec* my_iovec, 
		  uint32_t& sizeToDisk);

  DWError closeFile();

  DWError nextFile();

  std::string nameFile(const FileStatus& fs) const;

  int64_t getPosition() const;

  unsigned int eventsInFile() const; 

  unsigned int eventsInFileSequence() const;
 
  unsigned int dataMB_InFile() const; 

  unsigned int dataMB_InFileSequence() const; 
 
  void registerCallBack(DataWriterCallBack *pUserCBclass);
  void unregisterCallBack(DataWriterCallBack *pUserCBclass);

  EventStorage::CompressionType getCompression() const;

  void setGuid(const std::string& Guid);

private:
  std::fstream m_cFile;   // current file

  bool m_cFileOpen;        // Is a file open at the moment?
  bool m_openFailed;       // Has the lest open operation failed?
    
  file_start_record m_file_start_record;          // parameters of the current file
  file_name_strings m_file_name_strings;          // two significant strings in file name 
  internal_run_parameters_record m_internal_run_parameters_record;  // parameters of the run
  file_end_record m_file_end_record;              // more parameters of the file

  std::string m_guid; //the GUID
  std::string m_next_guid;

  std::string m_writePath;       // path to write
  std::string m_nextWritePath;   // next path to write
  std::string m_fileNameCore;    // most of the file name 

  std::string        m_project;           // fields needed at some point
  std::string        m_streamType;        //
  std::string        m_streamName;        //
  std::string        m_stream;
  unsigned int       m_lumiBlockNumber;
  std::string        m_applicationName;

  CompressionType    m_compression;
  unsigned int       m_complevel;
  compression::DataBuffer         *m_compressed;
  
  double             m_cFileMB; // megabytes so far in the file, for adding up
  long double        m_runMB;   // megabytes so far in the run
  uLong              m_check;   //running checksum

  int64_t            m_latestPosition; // offset of the latest written event

  std::vector<DataWriterCallBack*> mp_callBacks; // pointer to call-back, for reporting file names

  freeMetaDataStrings m_fmdStrings; // optional vector of strings, not written out if empty  

  boost::shared_ptr<FileNameCallback> m_filenamecallback;

  Adler32MT m_adler32mt;
};


// constructors
DataWriter::impl::impl(const string writingPath,   
		       boost::shared_ptr<FileNameCallback> theFNCB,   
		       const run_parameters_record rPar, 
		       const std::string project,
		       const std::string streamType,
		       const std::string streamName,
		       const std::string stream,
		       const unsigned int lumiBlockNumber,
		       const std::string applicationName,
		       const std::vector<std::string> fmdStrings,
		       const CompressionType compression,
		       const unsigned int compLevel,
		       const int adler32mt_nb_threads,
		       const uint32_t adler32mt_threshold)
	: m_adler32mt(adler32mt_nb_threads, adler32mt_threshold)
{
  initDW(writingPath, theFNCB, rPar, project, streamType, streamName, stream, lumiBlockNumber, applicationName, fmdStrings, compression, compLevel);
}

DataWriter::impl::impl(const string writingPath,   
		       const string fileNameCore,     
		       const run_parameters_record rPar, 
		       const freeMetaDataStrings fmdStrings, 
		       const unsigned int startIndex,
		       const CompressionType compression,
		       const unsigned int compLevel,
		       const int adler32mt_nb_threads,
		       const uint32_t adler32mt_threshold)
	: m_adler32mt(adler32mt_nb_threads, adler32mt_threshold)
{
  boost::shared_ptr<daq::RawFileName> 
    fileName(new daq::RawFileName(fileNameCore));

  if ( fileName->hasValidCore() ) {
    m_project         = fileName->project();
    m_streamType      = fileName->streamType();
    m_streamName      = fileName->streamName();
    m_stream          = fileName->stream();
    m_lumiBlockNumber = fileName->lumiBlockNumber();
    m_applicationName = fileName->applicationName();
  } else {
    std::stringstream mystream;
    mystream <<  "FileName " << fileName->fileName() 
	     << " has failed interpretation."
	     << " The file header will have no values for "
	     << " project, stream, lumiBlock and appName";
    ERS_DEBUG(1,mystream.str());
    // EventStorage::RawFileNameIssue myissue(ERS_HERE, mystream.str().c_str() );
    // ers::warning(myissue);
    m_project         = "";
    m_streamType      = "";
    m_streamName      = "";
    m_stream          = ""; 
    m_lumiBlockNumber = 0;
    m_applicationName = "";
  }

  fileName->setFileSequenceNumber(startIndex);

  initDW(writingPath, fileName, rPar, m_project, m_streamType, m_streamName,
	 m_stream, m_lumiBlockNumber, m_applicationName, 
	 fmdStrings, compression,compLevel);

}


DataWriter::impl::impl(const string writingPath,   
		       const string fileNameCore,     
		       const run_parameters_record rPar, 
		       const std::string project,
		       const std::string streamType,
		       const std::string streamName,
		       const std::string stream,
		       const unsigned int lumiBlockNumber,
		       const std::string applicationName,
		       const std::vector<std::string> fmdStrings,
		       const CompressionType compression,
		       const unsigned int compLevel,
		       const int adler32mt_nb_threads,
		       const uint32_t adler32mt_threshold)
	: m_adler32mt(adler32mt_nb_threads, adler32mt_threshold)
{
  boost::shared_ptr<FileNameCallback> fileName(new SimpleFileName(fileNameCore));
  initDW(writingPath, fileName, rPar, project, streamType, streamName, stream, lumiBlockNumber, applicationName, fmdStrings, compression, compLevel);
  
}

// destructor    
DataWriter::impl::~impl()
{
  ERS_DEBUG(1,"DataWriter::~DataWriter() called.");
  
  m_file_end_record.status = 1; // This identifies the end of the sequence.
  if(m_cFileOpen) closeFile();
  
  delete m_compressed;

  ERS_DEBUG(1,"DataWriter::~DataWriter() finished.");
}


void DataWriter::impl::initDW(const string writingPath,   
			      boost::shared_ptr<FileNameCallback> theFNCB,   
			      const run_parameters_record rPar, 
			      const std::string project,
			      const std::string streamType,
			      const std::string streamName,
			      const std::string stream,
			      const unsigned int lumiBlockNumber,
			      const std::string applicationName,
			      const std::vector<std::string> fmdStrings,
			      const CompressionType compression,
			      const unsigned int compLevel) {
  m_project         = project;
  m_streamType      = streamType;
  m_streamName      = streamName;
  m_stream          = stream;
  m_lumiBlockNumber = lumiBlockNumber;
  m_applicationName = applicationName;

  m_filenamecallback = theFNCB;

  reset_record(&m_file_start_record,&file_start_pattern);
  reset_record(&m_internal_run_parameters_record,&run_parameters_pattern);
  reset_record(&m_file_end_record,&file_end_pattern);
  m_cFileMB = 0.0;
  m_runMB = 0.0;
  m_latestPosition = -1;

  m_writePath = writingPath;
  m_nextWritePath = "";
  
  m_file_name_strings.appName = m_applicationName;
  m_file_name_strings.fileNameCore = m_filenamecallback->getCoreName();

  setRunParamsRecord(rPar);

  m_file_start_record.sizeLimit_MB = 0;
  m_file_start_record.sizeLimit_dataBlocks = 0;
  date_timeAsInt(m_file_start_record.date,m_file_start_record.time);
  
  m_cFileOpen=false;
  m_openFailed = false;
  
  mp_callBacks.clear();

  m_fmdStrings = fmdStrings;

  m_compression = compression;
  m_complevel = compLevel;
  m_compressed = new compression::DataBuffer();

  // add stream, project, compression, lumiBlock 
  // (if they are valid) to meta-data:
  m_fmdStrings.push_back("Stream=" + m_stream );
  m_fmdStrings.push_back("Project=" + m_project );
  std::ostringstream o;
  o << m_lumiBlockNumber;
  m_fmdStrings.push_back("LumiBlock=" + o.str());
  
  if(m_compression != EventStorage::NONE){
    m_fmdStrings.push_back(EventStorage::compressiontag + 
			   "=" + EventStorage::type_to_string(m_compression));
  }

  m_check = ::adler32(0L, Z_NULL, 0);

  m_guid = "";
  m_next_guid = "";
  this->spaceForGuid();
  this->openNextFile();
}


//date and time utility
void DataWriter::impl::
date_timeAsInt(uint32_t &getDate, uint32_t &getTime) const {
    long a_time;
    time(&a_time);
    
    struct tm*  t;
    t = localtime( &a_time);
    
    getDate= 1000000*t->tm_mday+
	     10000*(1+t->tm_mon)+
	     1900+t->tm_year;

    getTime= 10000*t->tm_hour+
	     100*t->tm_min+
	     t->tm_sec ;
}


bool DataWriter::impl::good() const {

    if(m_openFailed) return false;

    if(m_cFileOpen) 
    {
      return m_cFile.good();
    }
    else
    {
	return true;
    }
}


// configuration
DWError DataWriter::impl::setMaxFileMB(const unsigned int& maxFileMB) {
    m_file_start_record.sizeLimit_MB = maxFileMB;
    return DWOK;
}

DWError DataWriter::impl::setMaxFileNE(const unsigned int& maxFileNE) {
    m_file_start_record.sizeLimit_dataBlocks = maxFileNE;
    return DWOK;
}

void DataWriter::impl::setRunParamsRecord(const run_parameters_record& rPar) {

    m_internal_run_parameters_record.marker = rPar.marker; 
    m_internal_run_parameters_record.record_size = rPar.record_size; 
    m_internal_run_parameters_record.run_number = rPar.run_number; 
    m_internal_run_parameters_record.max_events = rPar.max_events;
    m_internal_run_parameters_record.rec_enable = rPar.rec_enable;
    m_internal_run_parameters_record.trigger_type = rPar.trigger_type;
    m_internal_run_parameters_record.detector_mask_1of4 = 
      rPar.detector_mask_LS & 0xFFFFFFFF;
    m_internal_run_parameters_record.detector_mask_2of4 = 
      (rPar.detector_mask_LS >> 32); 
    m_internal_run_parameters_record.detector_mask_3of4 = 
      rPar.detector_mask_MS & 0xFFFFFFFF; 
    m_internal_run_parameters_record.detector_mask_4of4 = 
      (rPar.detector_mask_MS >> 32); 
    m_internal_run_parameters_record.beam_type = rPar.beam_type;
    m_internal_run_parameters_record.beam_energy = rPar.beam_energy;
}

// output of data
DWError DataWriter::impl::
putData(const unsigned int& dataSize, const void *event) {

  uint32_t todisk;
  struct iovec iov;
  iov.iov_base = const_cast<void *>(event);
  iov.iov_len = dataSize;
  return this->putData(1, &iov, todisk);
}

// output of scattered data using iovec
DWError DataWriter::impl::
putData(const unsigned int& entries, const struct iovec * my_iovec) {
  uint32_t todisk;
  return this->putData(entries, my_iovec, todisk);
}

DWError DataWriter::impl::
putData(const unsigned int& dataSize, const void *event, uint32_t& sizeToDisk) {
  
  struct iovec iov;
  iov.iov_base = const_cast<void *>(event);
  iov.iov_len = dataSize;
  return this->putData(1, &iov, sizeToDisk);
}

DWError DataWriter::impl::
putData(const unsigned int& entries, const struct iovec * my_iovec, uint32_t& sizeToDisk) {
  return this->putData_implementation(entries, my_iovec, sizeToDisk);
}

DWError DataWriter::impl::
putPrecompressedData(const unsigned int& dataSize, const void *event) {

  struct iovec iov;
  iov.iov_base = const_cast<void *>(event);
  iov.iov_len = dataSize;
  return this->putPrecompressedData(1, &iov);
}

DWError DataWriter::impl::putPrecompressedData(const unsigned int& entries, 
					       const struct iovec* my_iovec) {

  uint32_t todisk;
  return this->putData_implementation(entries, my_iovec, todisk, true);
}

DWError DataWriter::impl::
putData_implementation(const unsigned int& entries, 
		       const struct iovec * my_iovec, 
		       uint32_t& sizeToDisk, bool precompressed) {

  ERS_DEBUG(3,"DataWriter::putData called for an iovec.");
  if(!m_cFileOpen) openNextFile();

  if(!m_cFileOpen) return DWNOOK;
  
  if(!m_cFile.good()) return DWNOOK;
  
  // calculate events size
  unsigned int dataSize = 0;
  for(unsigned int i = 0; i<entries; i++) {
    dataSize += my_iovec[i].iov_len;
  }


  
  sizeToDisk = dataSize;
  
  
  // apply limits 
  if(m_file_start_record.sizeLimit_MB>0) { 
    double eventMB=static_cast<double>(dataSize)/(1024*1024);
    double dataMB=m_cFileMB+eventMB;
    if(static_cast<unsigned int>(dataMB+0.5) > 
       m_file_start_record.sizeLimit_MB) {
      ERS_DEBUG(3,"Approaching file size limit in MB.");
      nextFile();
    }
  }
  
  if((m_file_start_record.sizeLimit_dataBlocks>0) && 
     (m_file_end_record.events_in_file >= m_file_start_record.sizeLimit_dataBlocks)){
    ERS_DEBUG(3,"Approaching file size limit in number of events.");
    nextFile();
  }

  //Here we should compress, so that we have the effective size
  //for the event record 
  if(m_compression == ZLIB && !precompressed){
    try{
      compression::zlibcompress(*m_compressed, sizeToDisk, 
				entries, my_iovec, dataSize, m_complevel);
    }catch(compression::CompressionIssue& ex){
      WritingIssue ci(ERS_HERE, "Data compression failed.", ex);
      ers::error(ci);
      return DWNOOK;
    }
  }
  
  // prepare the event record
  data_separator_record dsR = data_separator_pattern;
  dsR.data_block_number = m_file_end_record.events_in_run + 1;
  dsR.data_block_size = sizeToDisk;
  
  // remember the position
  m_latestPosition = m_cFile.tellg();
  
  // write the event record followed by event data    
  file_record(&dsR,&data_separator_pattern);
  
  
  if(m_compression != ZLIB || precompressed){
    for (unsigned int i =0; i<entries; i++){
      m_cFile.write(static_cast<const char*>(my_iovec[i].iov_base),
		    my_iovec[i].iov_len);
      
      m_check = m_adler32mt.adler32(m_check,
			  static_cast<const Bytef *>(my_iovec[i].iov_base),
			  my_iovec[i].iov_len);
    }
  }else{
    m_cFile.write(static_cast<const char *>(m_compressed->handle()), sizeToDisk);
    m_check = m_adler32mt.adler32(m_check,
			static_cast<const Bytef *>(m_compressed->handle()),
			sizeToDisk);
  }
  
  // update statistics
  m_cFileMB += static_cast<double>(sizeToDisk)/(1024*1024);
  m_runMB += static_cast<double>(sizeToDisk)/(1024*1024);
  m_file_end_record.events_in_file++;
  m_file_end_record.events_in_run++;
  
  ERS_DEBUG(3,"Event "<< m_file_end_record.events_in_run<<" is written at offset "<<m_latestPosition);
  
  if(!m_cFile.good()) return DWNOOK;
  
  return DWOK;    
}

int64_t DataWriter::impl::getPosition() const {
  return m_latestPosition;
}

// statistics
unsigned int DataWriter::impl::eventsInFile() const {
  return m_file_end_record.events_in_file;
}

unsigned int DataWriter::impl::eventsInFileSequence() const {
  return m_file_end_record.events_in_run;
}

unsigned int DataWriter::impl::dataMB_InFile() const {
  return static_cast<unsigned int>(m_cFileMB+0.5);
}

unsigned int DataWriter::impl::dataMB_InFileSequence() const {
  return static_cast<unsigned int>(m_runMB+0.5);
}

// closes the current file    
DWError DataWriter::impl::closeFile() {
  ERS_DEBUG(3,"DataWriter::closeFile() called.");

  // do statistics
  m_file_end_record.data_in_file = static_cast<unsigned int>(m_cFileMB+0.5);
  m_file_end_record.data_in_run  = static_cast<unsigned int>(m_runMB+0.5);

  date_timeAsInt(m_file_end_record.date,m_file_end_record.time);

  // write eof record
  file_record(&m_file_end_record,&file_end_pattern);

  m_cFile.close();
  std::ostringstream oss;
  oss.width(8);
  oss.fill('0');
  oss << std::hex << std::uppercase <<m_check;
  std::string checksum = oss.str();
  m_check = ::adler32(0L, Z_NULL, 0);

  // rename the file
  ERS_DEBUG(3,"Rename file " << nameFile(UNFINISHED) << endl
	    << "to " << nameFile(FINISHED));
  ::rename(nameFile(UNFINISHED).c_str(), nameFile(FINISHED).c_str());

  // check if the file is there
  if(fileExists(nameFile(FINISHED)) && m_cFileOpen) 
    {
      ERS_DEBUG(2,"Finished file "<<nameFile(FINISHED));
      for (DataWriterCallBack* & cb: mp_callBacks) {
        ERS_DEBUG(3,"Execute callback from DataWriter: cb = " << std::hex << cb << std::dec);
        cb->FileWasClosed(
                 m_filenamecallback->getCurrentFileName(),
                 nameFile(FINISHED),
                 m_streamType,
                 m_streamName,
                 m_applicationName,
                 m_guid,
                 checksum,
                 m_file_end_record.events_in_file,
                 m_internal_run_parameters_record.run_number,
                 m_file_start_record.file_number,
                 m_lumiBlockNumber,
                 getFileSize(nameFile(FINISHED))
                 );
      }
      
      // now we can substitute new directory, if specified
      if(m_nextWritePath != "") {
	m_writePath=m_nextWritePath;
	m_nextWritePath="";
	ERS_DEBUG(3,"New writing path became effective. The path is "
		  <<m_writePath);
      }
      m_cFileOpen = false;

      try{
	m_filenamecallback->advance();
      }catch(EventStorage::ES_SingleFile &e){
	//If opennext file is called, it 
	// will fail there
      }
      return DWOK; 
    }
  return DWNOOK;
}

// closes the current file and opens another one   
DWError DataWriter::impl::nextFile() {
  ERS_DEBUG(2,"DataWriter::nextFile() called.");

  DWError we=closeFile();
   
/*  if (m_freename)
  {
    EventStorage::ES_SingleFile ci(ERS_HERE, "DataWriter called with freename = 1. DataWriter wants to go to next file, but cannot, since indexing is forbidden. Either deactive freename (so files can be indexed, or increase the file size limit (or turn it off)");
    throw ci;  
  }*/

  openNextFile();
  if(m_openFailed) we=DWNOOK; 
  return we;
}

bool DataWriter::impl::fileExists(const string& name) const {
  ifstream test(name.c_str(),ios::binary | ios::in);
  bool isThere = test.good();
  test.close();
    
  return isThere;
}

void DataWriter::impl::openNextFile() {
  m_cFileMB=0.0;
  m_file_end_record.events_in_file=0;
  
  try{
    m_file_start_record.file_number = m_filenamecallback->getIndex();
  }catch(EventStorage::ES_SingleFile &e){
    m_file_start_record.file_number++;
  }
  date_timeAsInt(m_file_start_record.date,m_file_start_record.time);

  ERS_DEBUG(2,"Trying to open file "<<nameFile(UNFINISHED));
  
  while(fileExists(nameFile(UNFINISHED)) 
	|| fileExists(nameFile(FINISHED))) 
    {
    
      if(fileExists(nameFile(UNFINISHED))) {
	string err = "found an existing file with name " + nameFile(UNFINISHED);
	WritingIssue ci(ERS_HERE, err.c_str());
	ers::warning(ci);
	
      } else {
	
	string err = "found an existing file with name " + nameFile(FINISHED);
	WritingIssue ci(ERS_HERE, err.c_str());
	ers::warning(ci);
      }
      m_filenamecallback->fileAlreadyExists();
      
      m_file_start_record.file_number = m_filenamecallback->getIndex();
      
      ostringstream oss;
      oss << "increase file number to ";
      oss << m_file_start_record.file_number;
      WritingIssue ci(ERS_HERE, oss.str().c_str());
      ers::warning(ci);
    }
  ERS_DEBUG(2,"OK to write file with number "<<m_file_start_record.file_number);

  ERS_DEBUG(2,"Opening file: " << nameFile(UNFINISHED));

  m_cFile.clear();
  m_cFile.open(nameFile(UNFINISHED).c_str(),
	       ios::out | ios::app | ios::binary);

  m_check = ::adler32(0L, Z_NULL, 0);

  file_record(&m_file_start_record,&file_start_pattern);
  file_record(m_file_name_strings);

  this->replaceGuid();

  ERS_DEBUG(3,"Writing "<<m_fmdStrings.size()<<" metadata strings.");
  if(m_fmdStrings.size()>0) file_record(m_fmdStrings);
  file_record(&m_internal_run_parameters_record,&run_parameters_pattern);
  ERS_DEBUG(3,"Status of the file. good() returns "<<m_cFile.good());
  m_cFileOpen = m_cFile.good();
  m_openFailed = !m_cFile.good();
  
  if(m_cFileOpen) {
    for (DataWriterCallBack* & cb: mp_callBacks) {
      ERS_DEBUG(3,"Execute callback from DataWriter: cb = " << std::hex << cb << std::dec);
      cb->FileWasOpened(
          m_filenamecallback->getCurrentFileName(),
          nameFile(UNFINISHED),
          m_streamType,
          m_streamName,
          m_applicationName,
          m_guid,
          m_internal_run_parameters_record.run_number,
          m_file_start_record.file_number,
          m_lumiBlockNumber
      );
    }
  }
}

string DataWriter::impl::nameFile(const FileStatus& fs) const {
  ostringstream n;
  n << m_writePath << "/";
  n << m_filenamecallback->getCurrentFileName((FINISHED == fs)?false:true);
  string name = n.str();
  return name;	    
}

void DataWriter::impl::cd(const string& writingPath) {
  m_nextWritePath = writingPath;
  ERS_DEBUG(3,"Directory changed to "<<m_nextWritePath
	    <<" (effective for hext file).");
}

bool DataWriter::impl::inTransition() const {

  bool inT=false;
  if((m_nextWritePath != "") && 
     (m_nextWritePath != m_writePath)) inT=true;
  return inT;

}

void DataWriter::impl::file_record(void *ri, const void *pi) {

  uint32_t *record = (uint32_t *)ri;
  uint32_t *pattern = (uint32_t *)pi;
  int size=pattern[1];

  for(int i=0; i<size; i++) {
    if(pattern[i] != 0) {
      m_cFile.write((char *)(pattern+i),sizeof(uint32_t));
      // Don't call multithread impl for 4 bytes
      m_check = ::adler32(m_check,(const Bytef*)(pattern+i),sizeof(uint32_t));
    } else {
      m_cFile.write((char *)(record+i),sizeof(uint32_t));
      // Don't call multithread impl for 4 bytes
      m_check = ::adler32(m_check,(const Bytef*)(record+i),sizeof(uint32_t));
    }
  }
}


void DataWriter::impl::file_record(const EventStorage::file_name_strings& nst) {
  
  const char *cName = nst.appName.c_str();
  const char *cTag = nst.fileNameCore.c_str();

  uint32_t sizeName =  nst.appName.size();
  uint32_t sizeTag =  nst.fileNameCore.size();

  m_cFile.write((char *)(&EventStorage::file_name_strings_marker),sizeof(uint32_t));
  // Don't call multithread impl for 4 bytes
  m_check =  ::adler32(m_check,(const Bytef*)
		     (&EventStorage::file_name_strings_marker),
		     sizeof(uint32_t));

  m_cFile.write((char *)(&sizeName),sizeof(uint32_t));
  // Don't call multithread impl for 4 bytes
  m_check =  ::adler32(m_check,(const Bytef*)
		     (&sizeName),
		     sizeof(uint32_t));
  m_cFile.write(cName,sizeName);
  // Don't call multithread impl for so few bytes
  m_check =  ::adler32(m_check,(const Bytef*)
		     (cName),
		     sizeName);

  char ns = sizeName % 4;
  if(ns){ 
    m_cFile.write("    ",4-ns);
    // Don't call multithread impl for so few bytes
    m_check = ::adler32(m_check,(const Bytef*)"    ",4-ns);
  }

  m_cFile.write((char *)(&sizeTag),sizeof(uint32_t));
  // Don't call multithread impl for 4 bytes
  m_check = ::adler32(m_check,(const Bytef*)(&sizeTag),sizeof(uint32_t));
  m_cFile.write(cTag,sizeTag);
  // Don't call multithread impl for so few bytes
  m_check = ::adler32(m_check,(const Bytef*)cTag,sizeTag);

  ns = sizeTag % 4;
  if(ns){ 
    m_cFile.write("    ",4-ns);
    m_check = ::adler32(m_check,(const Bytef*)"    ",4-ns);
  }

}

void DataWriter::impl::
file_record(const EventStorage::freeMetaDataStrings& fmdStrings) {
  ERS_DEBUG(2,"Writing the metadata strings.");

  m_cFile.write((char *)(&EventStorage::free_strings_marker),sizeof(uint32_t));
  // Don't call multithread impl for 4 bytes
  m_check = ::adler32(m_check, (const Bytef*)
		    (&EventStorage::free_strings_marker),
		    sizeof(uint32_t));

  uint32_t nstrings = fmdStrings.size();
  m_cFile.write((char *)(&nstrings),sizeof(uint32_t));
  // Don't call multithread impl for 4 bytes
  m_check = ::adler32(m_check, (const Bytef*)
		    (&nstrings),
		    sizeof(uint32_t));

  vector<string>::const_iterator it;
  for(it=fmdStrings.begin(); it!=fmdStrings.end(); ++it) {

    const char *cst = it->c_str();
    uint32_t slen =  it->size();
   
    m_cFile.write((char *)(&slen),sizeof(uint32_t));
    // Don't call multithread impl for 4 bytes
    m_check = ::adler32(m_check,(const Bytef*)(&slen),sizeof(uint32_t));
    m_cFile.write(cst,slen);
    // Don't call multithread impl for so few bytes
    m_check = ::adler32(m_check,(const Bytef*)cst,slen);
    char ns = slen % 4;
    if(ns){ 
      m_cFile.write("    ",4-ns);
      // Don't call multithread impl for so few bytes
      m_check = ::adler32(m_check,(const Bytef*)"    ",4-ns);
    }
  }
}

void DataWriter::impl::registerCallBack(DataWriterCallBack *pUserCBclass) {
  if (pUserCBclass == nullptr) return;
  mp_callBacks.push_back(pUserCBclass);
  pUserCBclass->FileWasOpened(
      m_filenamecallback->getCurrentFileName(),
      nameFile(UNFINISHED),
      m_streamType,
      m_streamName,
      m_applicationName,
      m_guid,
      m_internal_run_parameters_record.run_number,
      m_file_start_record.file_number,
      m_lumiBlockNumber
  );
}

void DataWriter::impl::unregisterCallBack(DataWriterCallBack *pUserCBclass) {
  if (pUserCBclass == nullptr) return;
  for (std::vector<DataWriterCallBack*>::const_iterator it(mp_callBacks.begin()); it != mp_callBacks.end(); ++it) {
    if (*it == pUserCBclass) {
      it = mp_callBacks.erase(it);
      break;
    }
  }
}

void DataWriter::impl::spaceForGuid() {
  string e("");
  m_fmdStrings.insert(m_fmdStrings.begin(),e);
}

void DataWriter::impl::replaceGuid() {
  using namespace std;
  
  if (m_next_guid == ""){
    poolCopy::Guid g(true);
    m_guid = g.toString();
  }else{
    m_guid = m_next_guid;
    m_next_guid = "";
  }
  
  string e1="GUID=" + m_guid;
  m_fmdStrings[0]=e1;
}

void DataWriter::impl::setGuid(const std::string& Guid) {
  m_next_guid = Guid;
}

uint64_t DataWriter::impl::getFileSize(const std::string& fileNameCore) const {
  struct stat64 tmp;
  ::stat64(fileNameCore.c_str(), &tmp);
  
  return tmp.st_size;
}

EventStorage::CompressionType DataWriter::impl::getCompression() const {
  return m_compression;
}


DataWriter::DataWriter(const std::string writingPath,     
		       const std::string fileNameCore,    
		       const run_parameters_record rPar, 
		       const std::vector<std::string> fmdStrings, 
		       const unsigned int startIndex,
		       const CompressionType compression,
		       const unsigned int compLevel,
		       const int adler32mt_nb_threads,
		       const uint32_t adler32mt_threshold)
  : pimpl(new DataWriter::impl(writingPath,
			       fileNameCore,
			       rPar,
			       fmdStrings,
			       startIndex,
			       compression,
			       compLevel,
			       adler32mt_nb_threads,
			       adler32mt_threshold)){}



DataWriter::DataWriter(const std::string writingPath,   
		       boost::shared_ptr<FileNameCallback> theFNCB,   
		       const run_parameters_record rPar, 
		       const std::string project,
		       const std::string streamType,
		       const std::string streamName,
		       const std::string stream,
		       const unsigned int lumiBlockNumber,
		       const std::string applicationName,
		       const std::vector<std::string> fmdStrings,
		       const CompressionType compression,
		       const unsigned int compLevel,
		       const int adler32mt_nb_threads,
		       const uint32_t adler32mt_threshold)
  : pimpl(new DataWriter::impl(writingPath,
			       theFNCB,
			       rPar,
			       project,
			       streamType,
			       streamName,
			       stream,
			       lumiBlockNumber,
			       applicationName,
			       fmdStrings,
			       compression,
			       compLevel,
			       adler32mt_nb_threads,
			       adler32mt_threshold)){}

DataWriter::DataWriter(const std::string writingPath,   
		       const std::string fileNameCore,     
		       const run_parameters_record rPar, 
		       const std::string project,
		       const std::string streamType,
		       const std::string streamName,
		       const std::string stream,
		       const unsigned int lumiBlockNumber,
		       const std::string applicationName,
		       const std::vector<std::string> fmdStrings,
		       const CompressionType compression,
		       const unsigned int compLevel,
		       const int adler32mt_nb_threads,
		       const uint32_t adler32mt_threshold)
  : pimpl(new DataWriter::impl(writingPath,
			       fileNameCore,
			       rPar,
			       project,
			       streamType,
			       streamName,
			       stream,
			       lumiBlockNumber,
			       applicationName,
			       fmdStrings,
			       compression,
			       compLevel,
			       adler32mt_nb_threads,
			       adler32mt_threshold)){}

DataWriter::~DataWriter(){}

DWError DataWriter::setMaxFileNE(const unsigned int& maxFileNE) {
  return pimpl->setMaxFileNE(maxFileNE);
}

DWError DataWriter::setMaxFileMB(const unsigned int& maxFileMB) {
  return pimpl->setMaxFileMB(maxFileMB);
}

bool DataWriter::good() const {
  return pimpl->good();
}

void DataWriter::cd(const std::string& writingPath){
  return pimpl->cd(writingPath);
}

bool DataWriter::inTransition() const {
  return pimpl->inTransition();
}

DWError DataWriter::putData(const unsigned int& dataSize, const void *event) {
  return pimpl->putData(dataSize, event);
}

DWError DataWriter::putPrecompressedData(const unsigned int& dataSize, 
					 const void *event) {
  return pimpl->putPrecompressedData(dataSize, event);
}

DWError DataWriter::putPrecompressedData(const unsigned int& entries, 
					 const struct iovec* my_iovec) {
  return pimpl->putPrecompressedData(entries, my_iovec);
}

DWError DataWriter::
putData(const unsigned int& entries, const struct iovec* my_iovec) {
  return pimpl->putData(entries, my_iovec);
}

DWError DataWriter::putData(const unsigned int& dataSize, const void *event, uint32_t& sizeToDisk) {
  return pimpl->putData(dataSize, event, sizeToDisk);
}

DWError DataWriter::
putData(const unsigned int& entries, const struct iovec* my_iovec, 
	uint32_t& sizeToDisk) {
  return pimpl->putData(entries, my_iovec, sizeToDisk);
}

DWError DataWriter::closeFile() {
  return pimpl->closeFile();
}

DWError DataWriter::nextFile() {
  return pimpl->nextFile();
}

std::string DataWriter::nameFile(const FileStatus& fs) const {
  return pimpl->nameFile(fs);
}

int64_t DataWriter::getPosition() const {
  return pimpl->getPosition();
}

unsigned int DataWriter::eventsInFile() const {
  return pimpl->eventsInFile();
}

unsigned int DataWriter::eventsInFileSequence() const {
  return pimpl->eventsInFileSequence();
}

unsigned int DataWriter::dataMB_InFile() const {
  return pimpl->dataMB_InFile();
}

unsigned int DataWriter::dataMB_InFileSequence() const {
  return pimpl->dataMB_InFileSequence();
}

void DataWriter::registerCallBack(DataWriterCallBack *pUserCBclass) {
  pimpl->registerCallBack(pUserCBclass);
}

void DataWriter::unregisterCallBack(DataWriterCallBack *pUserCBclass) {
  pimpl->unregisterCallBack(pUserCBclass);
}

EventStorage::CompressionType DataWriter::getCompression() const {
  return pimpl->getCompression();
}

void DataWriter::setGuid(const std::string& Guid) {
  pimpl->setGuid(Guid);
}
