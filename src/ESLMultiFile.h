#ifndef ESLMULTIFILE_H
#define ESLMULTIFILE_H


#include "EventStorage/fRead.h"
#include "EventStackLayer.h"
#include "EventStorage/RawFileName.h"
#include <vector>

using namespace EventStorage;


class ESLMultiFile : public EventStackLayer
{
 protected:	
  ESLMultiFile();

 public:
  ESLMultiFile(fRead* fR);

		
  EventStackLayer* openNext();
		
  void prepare();
  bool doneLoading();
		
  bool moreEvents() ;
  bool handlesContinuation()  ;

  void setContinuationFile(std::string next) ;

  void advance();

  unsigned int getSum();
		
  //distinct methods
  void setFile(std::string filename);
  void setFiles(std::vector<std::string> filenames);

		

 private:
  std::vector<std::string> m_filenames;
  unsigned int m_currentFileIndex;
  unsigned int m_sumsize;
  daq::RawFileName *m_rawfilename;

};


#endif 

