// Dear emacs, this is -*- c++ -*-

/**
 * @file MergedRawFile.h
 * @author <a href="mailto:Kostas.Kordas@cern.ch">Kostas KORDAS</a>
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Defines the EventStorage::MergedRawFile
 */

#include <stdlib.h>
#include <iostream>
#include <string>
#include <iomanip>
#include <sstream>
#include <zlib.h>
#include <stdint.h>
#include <sys/uio.h>
#include <fstream>

#include "Guid.h"
#include "MergedRawFile.h"
#include "EventStorage/DataReader.h"
#include "EventStorage/pickDataReader.h"
#include "EventStorage/RawFileName.h"
#include "EventStorage/EventStorageIssues.h"
#include "ers/ers.h"

// Define the constants as 32 bit words
const uint32_t
MergedRawFile::FileMarker = 0x1ba2baba;
const uint32_t
MergedRawFile::FileVersion = 0x1;


MergedRawFile::MergedRawFile ()
{
  //nothing
  m_header_data = 0;
}


MergedRawFile::MergedRawFile (bool read)
{
  if (read){};			// just to avoid the warning of unused "read"
  m_header_data = 0;
}

//superconstructor
MergedRawFile::MergedRawFile (std::vector < std::string > listOfFiles,
			      std::string mergedFileNameCore,
			      uint64_t maxFileSizeinBytes)
{
  m_header_data = 0;

  m_file_size_limit_MB = maxFileSizeinBytes / 1024 / 1024;

  m_num_events_in_sequence = 0;
  uint64_t currentsize = 0;
  unsigned int sequencenumber = 1;
  m_num_contained_events = 0;
  m_merged_filename = mergedFileNameCore;


  //calculationg how many merged files we will get
  uint64_t ccsize = 0;
  unsigned int nummf = 1;
  uint64_t maximum_of_all_FileSizes = 0;
  for (unsigned int cF = 0; cF < listOfFiles.size (); cF++)
    {
      uint64_t fileSize = calcfilesize (listOfFiles.at (cF));
      if (fileSize > maximum_of_all_FileSizes)
	maximum_of_all_FileSizes = fileSize;
      ccsize += fileSize;

      if (maxFileSizeinBytes != 0 && ccsize > maxFileSizeinBytes)
	{
	  nummf++;
	  ccsize = 0;
	}
    }

  if (maxFileSizeinBytes != 0
      && maxFileSizeinBytes < maximum_of_all_FileSizes)
    {
      std::ostringstream os;
      os <<
	" maxFileSizeinBytes is too small to merge multiple individual original files. "
	 << " It needs to be at least " << maximum_of_all_FileSizes / 1024 /
	1024 << " Mbytes." <<
	" (but if you do this bare minimum you'll be merging one file at a time. Put more!)";
      EventStorage::WritingIssue myissue (ERS_HERE, os.str ().c_str ());
      throw myissue;
    }

  //nummf now contains the number of reuslting merge files


  for (unsigned int cF = 0; cF < listOfFiles.size (); cF++)
    {
      DataReader *mDR = pickDataReader (listOfFiles.at (cF));
      uint64_t fileSize = calcfilesize (listOfFiles.at (cF));
      uint32_t runNumber = mDR->runNumber ();
      uint32_t numEvents = mDR->eventsInFile ();
      //m_lumi_block_number = mDR->lumiblockNumber ();
      if ( cF == 0 ) m_lumi_block_number = mDR->lumiblockNumber ();
      
      m_stream = mDR->stream ();
      m_project = mDR->projectTag ();
      delete mDR;		// my responsibility

      std::string fileName = listOfFiles.at (cF);

      m_run_number = runNumber;

      ERS_DEBUG(0, "DataReader Info for " << fileName << std::
		endl << "   fileSize " << fileSize << std::
		endl << "   runNumber " << runNumber << std::
		endl << "   numEvents " << numEvents);


      currentsize += fileSize;
      if (maxFileSizeinBytes != 0 && currentsize > maxFileSizeinBytes)
	{
	  unsigned int last = (sequencenumber == 1 ? 1 : sequencenumber - 1);
	  m_MFChecksums.push_back (this->dumptofile (currentsize, nummf,
						     sequencenumber, sequencenumber + 1, last, mergedFileNameCore));
	  currentsize = fileSize;
	  m_numcontained_events_in_merge_files.push_back(m_num_contained_events);
	  m_num_contained_events = 0;
	  m_ContainedFileInfo_vec.clear ();
	  sequencenumber++;
	}
      m_num_contained_events += numEvents;
      m_num_events_in_sequence += numEvents;

      ContainedFileInfo *obj = new ContainedFileInfo ();
      obj->set_file_size (fileSize);
      obj->set_run_number (runNumber);
      obj->set_num_events (numEvents);
      obj->set_file_name (fileName);

      m_ContainedFileInfo_vec.push_back (*obj);
      delete obj;
    }

  m_MFChecksums.push_back (dumptofile (currentsize, nummf, sequencenumber, sequencenumber, (sequencenumber == 1 ? 1 : sequencenumber - 1), mergedFileNameCore));	//dump the last file also
  m_numcontained_events_in_merge_files.push_back(m_num_contained_events);
}

MergedRawFile::MergedRawFile (std::vector < std::string > input_files,
			      std::vector < uint64_t > input_files_sizes,
			      std::string out_file_name,
			      uint32_t file_size_limit_MB,
			      uint32_t this_file_sequence_number,
			      uint32_t previous_file_sequence_number,
			      uint32_t next_file_sequence_number,
			      std::string project = "")
{
  for (unsigned int c = 0; c < input_files.size (); c++)
    {
      ContainedFileInfo *tr = new ContainedFileInfo ();
      tr->set_file_size (input_files_sizes.at (c));
      tr->set_file_name (input_files.at (c));

      m_ContainedFileInfo_vec.push_back (*tr);	//works?
      delete tr;


    }

  m_header_data = 0;

  m_header_vec.clear ();

  m_file_start_marker = FileMarker;
  m_file_version_number = FileVersion;

  m_num_contained_files = input_files.size ();

  m_merged_filename = out_file_name;
  m_merged_filename_vec = string_to_uint32vec (m_merged_filename);
  m_file_size_limit_MB = file_size_limit_MB;

  m_this_file_sequence_number = this_file_sequence_number;
  m_previous_file_sequence_number = previous_file_sequence_number;
  m_next_file_sequence_number = next_file_sequence_number;

  m_project = project;
  m_project_vec = string_to_uint32vec (m_project);


  //the arbitrary stuff

  m_GUID = "1223435-43534-546345-2345";
  m_GUID_vec = string_to_uint32vec (m_GUID);

  m_file_size64 = 0xcefaaddedeadfaceLL;
  m_file_size_LSB = (m_file_size64 & 0xFFFFFFFF);
  m_file_size_MSB = (m_file_size64 >> 32) & 0xFFFFFFFF;

  m_run_number = 32767;
  m_lumi_block_number = 65535;

  m_stream = "stream";
  m_stream_vec = string_to_uint32vec (m_stream);

  m_extra_info_vec = string_to_uint32vec ("*dubiosemessageinmergefiles*");

  m_num_contained_events = 255;

  //pack

  packHeader ();
}



MergedRawFile::MergedRawFile (std::vector < ContainedFileInfo > FileInfoArray,
			      std::string out_file_name,
			      uint32_t file_size_limit_MB,
			      uint32_t number_of_files_in_sequence,
			      uint32_t this_file_sequence_number,
			      uint32_t previous_file_sequence_number,
			      uint32_t next_file_sequence_number,
			      uint32_t contained_events,
			      uint32_t runnumber,
			      uint32_t lumiblock,
			      uint64_t size,
			      uint32_t eventsInSequence,
			      std::string guid,
			      std::string stream, std::string project)
{

  m_header_data = 0;

  m_ContainedFileInfo_vec = FileInfoArray;

  m_header_vec.clear ();

  m_file_start_marker = FileMarker;
  m_file_version_number = FileVersion;

  m_num_contained_files = FileInfoArray.size ();
  m_num_events_in_sequence = eventsInSequence;

  m_merged_filename = out_file_name;
  m_merged_filename_vec = string_to_uint32vec (m_merged_filename);
  m_file_size_limit_MB = file_size_limit_MB;

  m_num_files_in_sequence_number = number_of_files_in_sequence;
  m_this_file_sequence_number = this_file_sequence_number;
  m_previous_file_sequence_number = previous_file_sequence_number;
  m_next_file_sequence_number = next_file_sequence_number;

  m_project = project;
  m_project_vec = string_to_uint32vec (m_project);

  date_timeAsInt (m_open_date, m_open_time);

  //the arbitrary stuff

  m_GUID = guid;
  m_GUID_vec = string_to_uint32vec (m_GUID);

  m_file_size64 = size;
  m_file_size_LSB = (m_file_size64 & 0xFFFFFFFF);
  m_file_size_MSB = (m_file_size64 >> 32) & 0xFFFFFFFF;

  m_run_number = runnumber;
  m_lumi_block_number = lumiblock;


  m_stream = stream;
  m_stream_vec = string_to_uint32vec (m_stream);

  m_extra_info_vec = string_to_uint32vec ("");

  m_num_contained_events = contained_events;

  //pack

  packHeader ();
}




// Destructor
MergedRawFile::~MergedRawFile ()
{
  delete[] m_header_data;
}

// other methods

/**
 *  fill packed forms of the Header in the specified order
 */
void
MergedRawFile::packHeader ()
{
  m_header_vec.clear ();

  // fill the vector first
  //
  m_header_vec.push_back (m_file_start_marker);
  m_header_vec.push_back (m_file_version_number);
  m_header_vec.push_back (0);	// put correct size here, when rest is filled
  m_header_vec.push_back (m_file_size_LSB);
  m_header_vec.push_back (m_file_size_MSB);
  m_header_vec.push_back (m_file_size_limit_MB);
  m_header_vec.push_back (m_num_contained_events);
  m_header_vec.push_back (m_num_events_in_sequence);
  m_header_vec.push_back (m_num_contained_files);
  m_header_vec.push_back (m_open_date);
  m_header_vec.push_back (m_open_time);
  //
  m_header_vec.push_back (m_run_number);
  m_header_vec.push_back (m_lumi_block_number);
  // 
  m_header_vec.push_back (m_num_files_in_sequence_number);
  m_header_vec.push_back (m_this_file_sequence_number);
  m_header_vec.push_back (m_previous_file_sequence_number);
  m_header_vec.push_back (m_next_file_sequence_number);
  //
  m_header_vec.push_back (m_GUID_vec.size ());
  for (unsigned int i = 0; i != m_GUID_vec.size (); i++)
    {
      m_header_vec.push_back (m_GUID_vec[i]);
    }
  //
  m_header_vec.push_back (m_merged_filename_vec.size ());
  for (unsigned int i = 0; i != m_merged_filename_vec.size (); i++)
    {
      m_header_vec.push_back (m_merged_filename_vec[i]);
    }
  // 

  m_header_vec.push_back (m_project_vec.size ());
  for (unsigned int i = 0; i != m_project_vec.size (); i++)
    {
      m_header_vec.push_back (m_project_vec[i]);
    }


  //
  m_header_vec.push_back (m_stream_vec.size ());
  for (unsigned int i = 0; i != m_stream_vec.size (); i++)
    {
      m_header_vec.push_back (m_stream_vec[i]);
    }
  //


  m_header_vec.push_back (m_extra_info_vec.size ());
  for (unsigned int i = 0; i != m_extra_info_vec.size (); i++)
    {
      m_header_vec.push_back (m_extra_info_vec[i]);
    }
  //
  /*
    m_header_vec.push_back( m_containedFiles_info_vec.size() );
    for (unsigned int i=0; 
    i != m_containedFiles_info_vec.size(); i++) {
    m_header_vec.push_back(m_containedFiles_info_vec[i]);
    }*/

  int sizeAllFileInfos = 0;

  for (unsigned int q = 0; q < m_ContainedFileInfo_vec.size (); q++)
    {
      std::vector < uint32_t > bla = m_ContainedFileInfo_vec.at (q).pack ();
      sizeAllFileInfos += bla.size ();
      sizeAllFileInfos += 1;	//one forr the length as pushed on line 329
      //m_header_vec.push_back(bla.size());           
    }
  uint64_t tmpsize = m_header_vec.size () * 4 + sizeAllFileInfos * 4;


  uint64_t concatfilesize = 0;
  for (unsigned int q = 0; q < m_ContainedFileInfo_vec.size (); q++)
    {
      m_ContainedFileInfo_vec.at (q).set_file_start_offset (tmpsize);
      tmpsize += m_ContainedFileInfo_vec.at (q).file_size_bytes ();
      concatfilesize += m_ContainedFileInfo_vec.at (q).file_size_bytes ();
      std::vector < uint32_t > bla = m_ContainedFileInfo_vec.at (q).pack ();
      m_header_vec.push_back (bla.size ());
      m_header_vec.insert (m_header_vec.end (), bla.begin (), bla.end ());
    }

  //get the size now and update the relative words in the header 
  m_header_size = m_header_vec.size ();

  concatfilesize += m_header_size * 4;	//the size of the WOHLE FILE


  uint32_t LSB = (concatfilesize & 0xFFFFFFFF);
  uint32_t MSB = (concatfilesize >> 32) & 0xFFFFFFFF;


  m_header_vec[2] = m_header_size;
  m_header_vec[3] = LSB;
  m_header_vec[4] = MSB;

  // next, pack Header into a continuous memory block (bytestream)
  //
  m_header_data = new uint32_t[m_header_size];
  uint32_t *p = m_header_data;
  for (unsigned int i = 0; i != m_header_vec.size (); i++){
    *p++ = m_header_vec[i];
  }

}

void
MergedRawFile::unpackHeader (uint32_t * data)
{


  // do reverse operation from packHeader: 
  if (data[0] != MergedRawFile::FileMarker)
    {
      // complain 
      return;
    }


  m_header_size = data[2];



  for (unsigned int i = 0; i != m_header_size; ++i)
    {
      m_header_vec.push_back (data[i]);
    }

  uint32_t *p = data;

  m_file_start_marker = *p++;	// data[0]
  m_file_version_number = *p++;	// data[1]

  m_header_size = *p++;		//data[2]  // ATTENTION: do not think to skip this!

  m_file_size_LSB = *p++;	// data[3]
  m_file_size_MSB = *p++;	// data[4]
  m_file_size64 = 0;
  m_file_size64 = m_file_size_MSB;
  m_file_size64 = m_file_size64 << 32;
  m_file_size64 = m_file_size64 + m_file_size_LSB;	// CHECK THIS !!!

  m_file_size_limit_MB = *p++;	//
  m_num_contained_events = *p++;	//


  m_num_events_in_sequence = *p++;
  m_num_contained_files = *p++;	//


  m_open_date = *p++;		// 
  m_open_time = *p++;		//

  m_run_number = *p++;		//
  m_lumi_block_number = *p++;	//

  m_num_files_in_sequence_number = *p++;
  m_this_file_sequence_number = *p++;	//
  m_previous_file_sequence_number = *p++;	//
  m_next_file_sequence_number = *p++;	//

  m_GUID = data_to_string (&p);
  m_merged_filename = data_to_string (&p);
  m_project = data_to_string (&p);
  m_stream = data_to_string (&p);
  m_extra_info_vec = data_to_vector (&p);




  for (unsigned int n = 0; n < m_num_contained_files; n++)
    {

      uint32_t length = *p++;
      (void)length; // when compiling in NON debug mode a warning was complaining that length was unused
      ERS_DEBUG (2, " length of entry is: " << length);
      uint32_t offsLW = *p++;
      uint32_t offsHW = *p++;

      uint32_t sizeLW = *p++;
      uint32_t sizeHW = *p++;


      uint64_t offset = offsHW;
      offset = offset << 32;
      offset += offsLW;

      uint64_t size = sizeHW;
      size = size << 32;
      size += sizeLW;

      ContainedFileInfo tmpcfbla;
      tmpcfbla.set_file_start_offset (offset);
      tmpcfbla.set_file_size (size);

      tmpcfbla.set_file_name (data_to_string (&p));
      m_ContainedFileInfo_vec.push_back (tmpcfbla);

    }



  /**
   // word after m_next_file_sequence_number is number of 
  
   for (unsigned int i=0; i != *p++; ++i) { // field after
   // fill the vectors  
   }
 
  
   m_GUID = guid;
   m_GUID_vec = string_to_uint32vec(m_GUID);

   m_merged_filename = merged_filename;
   m_merged_filename_vec = string_to_uint32vec(m_merged_filename);

   m_project = projetTag;
   m_project_vec = string_to_uint32vec(m_project);
  
   m_stream = stream;
   m_stream_vec = string_to_uint32vec(m_stream);

   m_extra_info_vec = vec_extra;
   m_containedFiles_info_vec = vec_contained;
  */

  return;
}


std::vector < uint64_t > MergedRawFile::filesizes ()
{
  std::vector < uint64_t > sz;
  for (unsigned int i = 0; i < m_ContainedFileInfo_vec.size (); i++)
    {
      sz.push_back (m_ContainedFileInfo_vec.at (i).file_size_bytes ());
    }
  return sz;
}

std::vector < uint64_t > MergedRawFile::fileoffsets ()
{
  std::vector < uint64_t > sz;
  for (unsigned int i = 0; i < m_ContainedFileInfo_vec.size (); i++)
    {
      sz.push_back (m_ContainedFileInfo_vec.at (i).file_start_offset ());
    }
  return sz;
}

std::vector < std::string > MergedRawFile::filenames()
{
  std::vector < std::string > sz;
  for (unsigned int i = 0; i < m_ContainedFileInfo_vec.size (); i++)
    {
      sz.push_back (m_ContainedFileInfo_vec.at(i).get_file_name());
    }
  return sz;
}



void
MergedRawFile::print ()
{
  std::ostringstream os;
  for (unsigned int i = 0; i < m_header_size; ++i)
    {
      os << i << ":0x" << std::hex << m_header_vec[i] << std::dec
	 << "(" << m_header_vec[i] << ") " << std::endl;
    }
  os << std::ends;

  ERS_LOG("MergedRawFile contains the following "
	  << m_header_size << " 32bit words:\n" << os.str ().c_str ());
}

uLong
MergedRawFile::write (std::ofstream & outF)
{

  uLong checksumofheader;
  checksumofheader = adler32 (0L, Z_NULL, 0);


  for (unsigned int i = 0; i < m_header_size; ++i)
    {
      //outF << packet[i] << std::endl;
      // put data into file, 1 item a time:
      outF.write ((char *) &m_header_vec[i], sizeof (uint32_t));
      checksumofheader =
	adler32 (checksumofheader, (const Bytef *) &m_header_vec[i],
		 sizeof (uint32_t));

      //  outF.write( (char *)&m_header_packet[i], sizeof(uint32_t) );

    };

  return checksumofheader;
}


uLong
MergedRawFile::writeFiles ()
{
  uLong curmergefile_check;	//running checksum

  uint64_t buffersize = 100 * 1024 * 1024;

  std::ofstream outF2 (m_merged_filename.c_str (),
		       std::ios::out | /*std::ios::app | */ std::ios::binary);

  curmergefile_check = write (outF2);

  for (unsigned int file = 0; file < m_ContainedFileInfo_vec.size (); file++)
    {

      std::string filename =
	m_ContainedFileInfo_vec.at (file).get_file_name ();
      std::ifstream readfile;	// current file

      uint64_t size = m_ContainedFileInfo_vec.at (file).file_size_bytes ();


      readfile.open (filename.c_str (), std::ios::binary | std::ios::in);
      //readfile.exceptions ( std::ifstream::eofbit | std::ifstream::failbit | std::ifstream::badbit );

      ERS_DEBUG(0, "Writing File: " << m_ContainedFileInfo_vec.at (file).
		get_file_name () << " to " << m_merged_filename);

      char *data = new char[buffersize];

      uLong curfile_check;	//running checksum
      curfile_check = adler32 (0L, Z_NULL, 0);

      m_OFFileSizes.push_back (size);

      while (size != 0)
	{
	  uint64_t bufferbytes = std::min (size, buffersize);
	  size -= bufferbytes;
	  readfile.read (data, bufferbytes);
	  //calculating the checksum

	  curfile_check =
	    adler32 (curfile_check, (const Bytef *) data, bufferbytes);
	  curmergefile_check =
	    adler32 (curmergefile_check, (const Bytef *) data, bufferbytes);


	  outF2.write (data, bufferbytes);
	}
      m_OFChecksums.push_back (curfile_check);

      m_ContainedFileInfo_vec.at (file).set_checksum (curfile_check);
      //this is the easy way, but it doesnt support checksums
      //outF2 << readfile.rdbuf();
      readfile.close ();

      delete[] data;

    }

  outF2.close ();
  return curmergefile_check;
}



void
MergedRawFile::extract (const std::vector < std::string > &vec_file_names)
{
  //  if the vector is empty, extract all individual files
  if (vec_file_names.size () == 0){};	// just to avoid the warning of unused vec
  return;
}





uLong
MergedRawFile::dumptofile (uint64_t currentsize, unsigned int numberMerge,
			   uint32_t sn, uint32_t snnext, uint32_t snprev,
			   std::string mfn)
{
  //poolCopy::Guid a_guid(true);

  std::ostringstream oss;
  oss << mfn << "._";
  oss << std::setw (daq::RAWFILENAME_FILE_NUMBER_LENGTH) << std::
    setfill ('0');
  oss << sn;
  oss << ".data";

  ERS_DEBUG(0,"Creating mergefile: " << oss.str () << std::endl
	    << "SeqN " << sn << ", "
	    << "NextSeqN " << snnext << ", " << "PrevSeqN " 
	    << snprev << std::endl);


  poolCopy::Guid a_guid (true);


  std::string myguid = a_guid.toString ();
  ERS_DEBUG(0,"guid:: " << myguid);
  ERS_DEBUG(0,"lbnr:: " << m_lumi_block_number);


  MergedRawFile *tmp = new MergedRawFile (m_ContainedFileInfo_vec,
					  oss.str (),
					  m_file_size_limit_MB,
					  numberMerge,
					  sn,
					  snprev,
					  snnext,
					  m_num_contained_events,
					  m_run_number,
					  m_lumi_block_number,
					  currentsize,
					  m_num_events_in_sequence,
					  myguid,
					  m_stream,
					  m_project);

  m_outFileNames.push_back (oss.str ());
  m_guids.push_back (myguid);

  uLong ck = tmp->writeFiles ();
  std::vector < ContainedFileInfo > tmpfvec = tmp->getFileList ();	//just for the offsets

  std::vector < uint64_t > offsets;
  std::vector < uint64_t > sizes = tmp->getOFSizes ();

  m_OFSizes.push_back (sizes);

  for (unsigned int z = 0; z < tmpfvec.size (); z++)
    {
      offsets.push_back (tmpfvec.at (z).file_start_offset ());

    }


  m_mergeOffsets.push_back (offsets);

  std::vector < uLong > ofthis = tmp->getOFChecksums ();

  m_OFChecksumsAll.push_back (ofthis);


  delete tmp;
  return ck;
}


std::vector < uLong > MergedRawFile::getOFChecksums ()
{
  return m_OFChecksums;

}

std::vector < std::vector < uLong > >MergedRawFile::getOFChecksumsAll ()
{
  return m_OFChecksumsAll;

}


std::vector < uint64_t > MergedRawFile::getOFSizes ()
{
  return m_OFFileSizes;

}

std::vector < std::vector < uint64_t > >MergedRawFile::getOFSizesAll ()
{
  return m_OFSizes;

}


std::vector < uLong > MergedRawFile::getMFChecksums ()
{
  return m_MFChecksums;

}

std::vector < std::string > MergedRawFile::getOutFileNames ()
{
  return m_outFileNames;
}

std::vector < std::string > MergedRawFile::getGUIDS ()
{
  return m_guids;
}



std::vector < std::vector < uint64_t > >MergedRawFile::getMFOffsets ()
{
  return m_mergeOffsets;
}

std::vector < ContainedFileInfo > MergedRawFile::getFileList ()
{
  return m_ContainedFileInfo_vec;
}


std::string data_to_string (uint32_t ** ptr)
{
  std::vector < uint32_t > data = data_to_vector (ptr);

  std::string tr = uint32vec_to_string (data);

  return tr;
}

std::vector < uint32_t > data_to_vector (uint32_t ** ptr)
{
  uint32_t strlength = **ptr;
  (*ptr)++;
  std::vector < uint32_t > vv;
  while (strlength != 0)
    {
      vv.push_back (**ptr);
      strlength--;
      (*ptr)++;
    }
  return vv;
}


std::string uint32vec_to_string (std::vector < uint32_t > &v)
{

  std::string s;

  for (unsigned int i = 0; i != v.size (); ++i)
    {
      s += v[i];
    }

  return s;
}


/**
 * helper methods
 */
std::vector < uint32_t > string_to_uint32vec (std::string s)
{

  std::vector < uint32_t > v;

  const char *tmp = s.c_str ();

  for (unsigned int i = 0; i != s.size (); ++i)
    {
      v.push_back (tmp[i]);
    }


  return v;
}

uint64_t
calcfilesize (std::string filename)
{
  uint64_t begin, end;
  std::ifstream myfile (filename.c_str ());
  begin = myfile.tellg ();
  myfile.seekg (0, std::ios::end);
  end = myfile.tellg ();
  myfile.close ();
  return (end - begin);
}


void
date_timeAsInt (uint32_t & getDate, uint32_t & getTime)
{
  long a_time;
  time (&a_time);

  struct tm *t;
  t = localtime (&a_time);

  getDate = 1000000 * t->tm_mday +
    10000 * (1 + t->tm_mon) + 1900 + t->tm_year;

  getTime = 10000 * t->tm_hour + 100 * t->tm_min + t->tm_sec;
}
