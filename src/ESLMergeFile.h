#ifndef ESLMERGEFILE_H
#define ESLMERGEFILE_H


#include "EventStorage/fRead.h"
#include "EventStackLayer.h"
#include "MergedRawFile.h"
#include "EventStorage/RawFileName.h"


class RawFileName;

class ESLMergeFile : public EventStackLayer
{
 public:
  ESLMergeFile( fRead * fR);
  ~ESLMergeFile();

		
		
  EventStackLayer* openNext();
  EventStackLayer* loadAtOffset(int64_t position, EventStackLayer *old);
		
  void prepare();
  bool doneLoading();
		
  bool moreEvents() ;

  std::string nextInContinuation() ;

  void advance();

  bool endOfFileSequence() ;
  void setContinuationFile(std::string next) ;
	
  std::string GUID() const;
		
  unsigned int runNumber() const ;
  unsigned int maxEvents() const ;
  unsigned int recEnable() const ;
  unsigned int triggerType() const ;
  std::bitset<128> detectorMask() const ;
  unsigned int beamType() const ;
  unsigned int beamEnergy() const ;
  std::vector<std::string> freeMetaDataStrings() const;
  CompressionType compression() const;

  unsigned int fileStartDate() const; 

  unsigned int fileStartTime() const;


  unsigned int fileEndDate();
  ///< Date when writing has stopped.
  unsigned int fileEndTime() ;
  ///< Time when writing has stopped.
  unsigned int eventsInFile() ;
  ///< Number of events in this file.
  unsigned int dataMB_InFile() ;
  ///< Number of megabytes in this file.
  unsigned int eventsInFileSequence() ;
  ///< Number of events in this file sequence written so far.
  unsigned int dataMB_InFileSequence();
  ///< Number of MB in this file sequence written so far. 
  //	  unsigned int fileStatusWord() ;         ///< Indicates an end of sequence. 

  unsigned int fileSizeLimitInDataBlocks() const ; 
  unsigned int fileSizeLimitInMB() const ;


  std::string appName() const;
  std::string fileNameCore() const;

		
  unsigned int nextInSequence(unsigned int current) ;
  uint32_t lumiblockNumber();
  std::string stream();
  std::string projectTag();

	
 private:
  EventStackLayer* openNextImpl(bool skiptoevents = false);
  void parseHeader();
  MergedRawFile* m_currentHeader;
  daq::RawFileName* m_RawFileName;

  std::vector<uint64_t> m_FileSizes;
  std::vector<uint64_t> m_FileOfsts;
		
  unsigned int m_numEncapsulatedFiles;
  unsigned int m_currentEncapsulatedFile;
  std::string m_guid;

  void setRPR(internal_run_parameters_record trpr);
  void setFMDS(EventStorage::freeMetaDataStrings tfmds);
  void setCompression(EventStorage::CompressionType comp);

  //these are the records of the first encapsulated file
  EventStorage::freeMetaDataStrings m_enc1_fmdStrings;	
  internal_run_parameters_record m_enc1_internal_latest_rpr;
  EventStorage::CompressionType m_enc1_compression;

};

#endif
