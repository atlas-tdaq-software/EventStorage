#ifndef MERGED_RAW_FILE
#define MERGED_RAW_FILE

/**
 * @file MergedRawFile.h
 * @author <a href="mailto:Kostas.Kordas@cern.ch">Kostas KORDAS</a>
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Defines the EventStorage::MergedRawFile
 *
 * Encoding:
 *  uin32_t word  Description
 *  ------------  ------------------------------------------------------------
 *     0                   file start marker
 *     1                   version number
 *     2                   header size
 *     3                   file size, part 1 (Least Significant Bits = LSB)
 *     4                   file size, part 2 (Most Sisgificant Bits  = MSB)
 *     5                   file size limit MB
 *     6                   number of contained events
 *     7                   number of contained files = q
 *     8                   file open date
 *     9                   file open time
 *    10                   run number
 *    11                   luminosity block number
 *    12                   this file sequence number (a.k.a: partition number)
 *    13                   previous file sequence number
 *    14                   next file sequence number
 * J0= 3                   number of GUID words = j
 *    J0+1                  GUID word 0
 *     ...
 *    J0+j                  GUID word j-1
 * K0=J0+j+8               number of filename words = k
 *    K0+1                  filename word 0
 *    ...
 *    K0+k                  filename work k-1
 * M0=K0+k+1               number of project tag words = m
 *    M0+1                  project tag word 0
 *    ...
 *    M0+m                  project tag work m-1
 * N0=M0+m+1               number of stream words = n
 *    N0+1                  stream word 0
 *    ...
 *    N0+n                  stream word n-1
 * E0=N0+n+1               number of words with extra info (if any) = e
 *    E0+1                  extrra info word 0
 *    ...
 *    E0+e                  extra info word e-1
 * P0=E0+e+1               number of words about the "q" contained files = p
 * L1=P0+1                  location(offset) of contained file 0, part 1 (LSB)
 *    L1+1                  location(offset) of contained file 0, part 2 (MSB)
 *    L1+2                  size of contained file 0, part 1 (LSB)
 *    L1+3                  size of contained file 0, part 2 (LSB)
 * W1=L1+4                  number of filename words for conained file 0 = r1
 *    W1+1                    filename word 0 about contained file 0
 *    ...
 *    W1+r1                   filename word r1-1 about contained file 0
 *    ...
 * LQ=P0+1+5*(q-1)+Sum(r_i) location(offset) of contained file q-1, part 1(LSB)
 *    LQ+1                  offset of contained file q-1, part 2 (MSB)
 *    LQ+2                  size of contained file q-1, part 1 (LSB)
 *    LQ+3                  size of contained file q-1, part 2 (LSB)
 * WQ=LQ+4                  number of filename words for conained file 0 = rq
 *    WQ+1                    filename word 0 about contained file 0
 *    ...
 *    WQ+rq                   filename word rq-1 about contained file 0
 * F1=P0+p+1                first word of 0th included file
 *  ...
 * FP=P0+p+1+Sum(sizeFiles) first word of contained file q-1
 *
 * Z0=P0+p+1+Sum(sizeFiles) last word of merged file
 *
 */
#include <iostream>
#include <iomanip>
#include <sstream>
#include <stdint.h>
#include <string>
#include <vector>
#include <boost/shared_array.hpp>
#include <fstream>
#include <time.h>
#include <zlib.h>
#include <stdlib.h>


std::string uint32vec_to_string(std::vector<uint32_t> & v);
std::vector<uint32_t> string_to_uint32vec(std::string  s);
std::string data_to_string(uint32_t** ptr);
std::vector<uint32_t> data_to_vector(uint32_t** ptr);
uint64_t calcfilesize(std::string filename);
void date_timeAsInt(uint32_t &getDate, uint32_t &getTime);


/**
 * Helper class
 */
class ContainedFileInfo {
 public:
  // write constructor: store the information in a vector of words
 ContainedFileInfo(const std::string& fnm, uint64_t fsz=10, uint64_t fso=0) :
  m_runnumber(0),
    m_num_events(0),
    m_file_name(fnm),
    m_file_size_bytes(fsz),
    m_file_start_offset(fso),
    offsets_done(false) {
    // use string to vector method (string_to_uint32vec in class below) here
    // --------- append info into m_file_info_vec
    
    // open the file to read its size
    // --------- append info into m_file_info_vec
    
    // the offset is left 0
    // --------- append info into m_file_info_vec
    
  };
  
 ContainedFileInfo() :
  m_runnumber(0),
    m_num_events(0),
    m_file_size_bytes(0),
    m_file_start_offset(0),
    m_checksum(0),
    offsets_done(false)
      {};

  // read constructor: given a vector of words, reconstrut the information
 ContainedFileInfo(std::vector<uint32_t> vec_info) :
  m_runnumber(0),
    m_num_events(0),
    m_file_name("file"),
    m_file_size_bytes(vec_info[0]),
    m_file_start_offset(0),
    offsets_done(false)
      {};
  
  //destructor
  ~ContainedFileInfo() {};

  //get & set methods:
  uint64_t file_size_bytes() { return m_file_size_bytes; };
  uint64_t file_start_offset() { return m_file_start_offset; };
  std::string get_file_name() { return m_file_name; };

 
  void set_file_start_offset(uint64_t fso)  { 	m_file_start_offset = fso;   };  
  void set_file_size(uint64_t fso) { 	m_file_size_bytes = fso; };
  void set_file_name(const std::string& fn )  { m_file_name = fn; };
  void set_run_number( uint32_t rn )  { m_runnumber = rn; };
  void set_num_events( uint32_t ne )  { m_num_events = ne; };
  void set_checksum( uLong thechecksum) { m_checksum = thechecksum; };
		
  std::vector<uint32_t> pack()
    {
      std::vector<uint32_t> tmp;
      tmp.clear();

      uint32_t file_offset_LSB = (m_file_start_offset & 0xFFFFFFFF);
      uint32_t file_offset_MSB = (m_file_start_offset >> 32)  & 0xFFFFFFFF;

      tmp.push_back(file_offset_LSB);
      tmp.push_back(file_offset_MSB);
	
		
      uint32_t file_size_LSB = (m_file_size_bytes & 0xFFFFFFFF);
      uint32_t file_size_MSB = (m_file_size_bytes >> 32)  & 0xFFFFFFFF;

      tmp.push_back(file_size_LSB);
      tmp.push_back(file_size_MSB);

      std::vector<uint32_t> sfn = string_to_uint32vec(m_file_name);
		
      tmp.push_back(sfn.size());
		
      tmp.insert(tmp.end(), sfn.begin(), sfn.end());
		
      return tmp;
		
    }
	

 private:
  // representation
  uint32_t m_runnumber;
  uint32_t m_num_events;
  std::string m_file_name;
  uint64_t m_file_size_bytes;
  uint64_t m_file_start_offset;
  uLong m_checksum;
  std::vector<uint32_t> m_file_info_vec;
  bool offsets_done;

};

/**
 * Defines how to serialise, deserialise and ask questions to the
 * Merged File Header. Such a Header is created to be put on the top of 
 * a "merged/concatenated" file, containing individual raw files, 
 * appended one after the other
 */
class MergedRawFile {
    
 public:

  /**
   * Constructors
   *==============
   */

  /**
   * Constructs a default MergedRawFile for tests.
   *
   */
  MergedRawFile();
  
  MergedRawFile(bool read);

  //the superconstructor: give list of files receive (list of) merged file(s)
  // default maxFileSizeinBytes=0 means: try to merge all files in one
  MergedRawFile( std::vector<std::string> listOfFiles,
		 std::string mergedFileNameCore,
		 uint64_t maxFileSizeinBytes=0);
 

  //a constructor for dummy header constructin
  MergedRawFile (std::vector<std::string> input_files,
		 std::vector<uint64_t> input_files_sizes,
		 std::string out_file_name,
		 uint32_t file_size_limit_MB,
		 uint32_t this_file_sequence_number,
		 uint32_t previous_file_sequence_number,
		 uint32_t next_file_sequence_number,
		 std::string project);
	
  //a constructor to create a defined mergefiles
  MergedRawFile(std::vector<ContainedFileInfo> FileInfoArray,
		std::string out_file_name,
		uint32_t file_size_limit_MB,
		uint32_t number_of_files_in_sequence, 
		uint32_t this_file_sequence_number,
		uint32_t previous_file_sequence_number,
		uint32_t next_file_sequence_number,
		uint32_t contained_events,
		uint32_t runnumber,
		uint32_t lumiblock,
		uint64_t size,
		uint32_t eventsInSequence,
		std::string guid,
		std::string stream,
		std::string project);
					

					
  /***
   * Constructs the MergedRawFile, given the input one by one:
   *
   * @param m_file_start_marker
   * @param m_file_version_number
   * @param m_header_size
   * @param m_file_size
   * @param m_file_size_limit_MB
   * @param m_nContained_files
   * @param m_file_open_date
   * @param m_file_open_time
   * @param m_file_sequence_number
   * @param m_file_continuation_flag
   * @param m_merged_filename
   * @param m_projetTag
   * @param m_stream
   * 
   * @param m_n_info_words
   * @param m_vec_words_info Set of words (32-bits each)
   *
   */

  /*


  MergedRawFile (std::string guid,
  uint64_t file_size,
  uint32_t file_size_limit_MB,
  uint32_t nContained_events,
  uint32_t nContained_files,
  uint32_t file_open_date,
  uint32_t file_open_time,
  std::string merged_filename,
  std::string stream,
  uint32_t run_number,
  uint32_t lumi_block,
  uint32_t this_file_sequence_number,
  uint32_t previous_file_sequnce_number,
  uint32_t next_file_sequence_number,
  const std::vector<uint32_t>& vec_contained,
  std::string projectTag="",
  const std::vector<uint32_t>& vec_extra=0);

  */

  /***
   * Contructs the Header from scratch, by fishing the info into the given 
   * files to be merged
   *
   * Input:
   *   std::vector<string>   input_files
   *   std::string           output_filename
   *   uint32_t              fileSizeLimit_MB
   *   uin32_t               file sequence number
   *   uint32_t              continuation flag
   *   std::string           project tag
   * Output:
   *   constructs header quitely
   */


  /**
   * Read constructor: create the Header object starting from a bytestream
   *
   * @param data
   */

  /*
    MergedRawFile (const uint32_t * data);
    MergedRawFile (const std::string mergedFileName);
    MergedRawFile (const std::ifstream & outFile);
  */


  /**
   * Copy constructor
   *
   * @param header Existing MergedRawFile
   */
  /*
    MergedRawFile (const MergedRawFile& header);
  */
		    
  /**
   * Destructor
   *============
   */
  ~MergedRawFile();
    
  /**
   * Get and Set methods
   */
  uint32_t file_start_marker() const {return m_file_start_marker;};
  uint32_t file_version_number() const {return m_file_version_number;};

  uint32_t header_size() const {return m_header_size;};
  void     header_size(const uint32_t hs) {m_header_size =hs;};

  uint64_t file_size() const {return m_file_size64;};
  void     file_size(const uint64_t fs) {m_file_size64 = fs;};

  uint32_t file_size_limit_MB() const {return m_file_size_limit_MB;};
  void     file_size_limit_MB(const uint32_t fsl) {m_file_size_limit_MB=fsl;};

  std::vector<uint32_t> header_vec() const {return m_header_vec;};
  void header_vec(const std::vector<uint32_t>& vec) {m_header_vec = vec;};
  
 
  /**
   * Return a pointer to the first info word.
   * In case of empty array returns NULL.
   */
  uint32_t * header_data() const {return m_header_data;};
  void header_data(uint32_t * data) { m_header_data = data;};
    
  /**
   * Return a vector of info words. Requires
   * a copy operation
   */
  // std::vector<uint32_t> info_words_vector() const;
  

  //std::vector<uint32_t> string_to_uint32vec( std::string & s );
  //std::string uint32vec_to_string( std::vector<uint32_t> & v );
 
  /**
   * Fill up all packed forms for the header (vector, array, continious memory)
   */
  void packHeader();

  /**
   * Given a bytestream, reconstruct  the Header info and 
   * fill up all packed forms for the header (vector, array, continious memory)
   */
  void unpackHeader(uint32_t * data);
  
  /**
   * Print Header content
   *
   */
  void print();
 
  /**
   * Write Header content to file
   *
   */
  uLong write(std::ofstream & outFile);
  uLong writeFiles();

  /**
   * Extract the files given in the vector of filenames.
   * If list of files to extract is empty, extract them all
   */
  void extract( const std::vector<std::string> & vec_file_names );

	
  std::string guid()	{ return m_GUID; }
  uint64_t filesize()	{ return m_file_size64; }
  uint32_t file_size_limit_MB()	{ return m_file_size_limit_MB; }
  uint32_t num_contained_events() {return m_num_contained_events; }
  uint32_t num_events_in_sequence() {return m_num_events_in_sequence; }
  uint32_t num_contained_files() {return m_num_contained_files; }
  uint32_t open_date() {return m_open_date; }
  uint32_t open_time() {return m_open_time;}

  std::vector<uint32_t> numcontained_events_in_merge_files() {return m_numcontained_events_in_merge_files; }


  std::string merged_filename() {return m_merged_filename;}
  std::string project() {return m_project;}
  std::string stream() {return m_stream;}
  uint32_t run_number()	{ return m_run_number; }
  uint32_t lumi_block_number()	{ return m_lumi_block_number; }

  uint32_t num_files_in_sequence_number()	{ return m_num_files_in_sequence_number; }

  uint32_t this_file_sequence_number()	{ return m_this_file_sequence_number; }
  uint32_t previous_file_sequence_number()	{ return m_previous_file_sequence_number; }
  uint32_t next_file_sequence_number()	{ return m_next_file_sequence_number; }
	
  std::vector<uint32_t> extra_info_vec() { return m_extra_info_vec; };

  uLong dumptofile(uint64_t currentsize, unsigned int numberMerge, uint32_t sn, uint32_t snnext, uint32_t snprev,  std::string mfn );

  std::vector<uint64_t> filesizes();
  std::vector<uint64_t> fileoffsets();
  std::vector < std::string > filenames();

  std::vector<uLong> getOFChecksums();
  std::vector< std::vector<uLong> > getOFChecksumsAll();
  std::vector<uint64_t> getOFSizes();

  std::vector<uLong> getMFChecksums();
  std::vector<std::string> getOutFileNames();
  std::vector<std::string> getGUIDS();
	
  std::vector< std::vector<uint64_t> > getMFOffsets();
  std::vector< std::vector<uint64_t> > getOFSizesAll();
  std::vector<ContainedFileInfo> getFileList();


 public: // constants
  static const uint32_t FileMarker;
  static const uint32_t FileVersion;

 private: // representation
  uint32_t m_file_start_marker;       ///< the file marker
  uint32_t m_file_version_number;     ///< the file version = 1
  uint32_t m_header_size;            
  std::string m_GUID;
  std::vector<uint32_t> m_GUID_vec;
  uint64_t m_file_size64;
  uint32_t m_file_size_LSB;           ///< two words: [0]=LSB; [1]=MSB
  uint32_t m_file_size_MSB;
  uint32_t m_file_size_limit_MB;
  uint32_t m_num_contained_events;
  uint32_t m_num_events_in_sequence;
  uint32_t m_num_contained_files;
  uint32_t m_open_date;
  uint32_t m_open_time;
  //
  std::string m_merged_filename;
  std::vector<uint32_t> m_merged_filename_vec;
  //
  std::string m_project;
  std::vector<uint32_t> m_project_vec;
  // 
  std::string m_stream;
  std::vector<uint32_t> m_stream_vec; 
  //
  uint32_t m_run_number;
  uint32_t m_lumi_block_number;
  //
  uint32_t m_num_files_in_sequence_number;
  uint32_t m_this_file_sequence_number;
  uint32_t m_previous_file_sequence_number;
  uint32_t m_next_file_sequence_number;
  //
  std::vector<uint32_t> m_extra_info_vec;
  //
  // std::vector<uint32_t> m_containedFiles_info_vec;
  std::vector<ContainedFileInfo> m_ContainedFileInfo_vec;

  // packed forms for the header:
  uint32_t * m_header_data;  ///< Host the header (must be deleted if owned)
  std::vector<uint32_t> m_header_vec; ///< vector form of header
  // boost::shared_array<uint32_t> m_header_array; ///< array form of header


  std::vector<uLong> m_OFChecksums;
  std::vector<uint64_t> m_OFFileSizes;
  std::vector< uLong > m_MFChecksums;
  std::vector< std::vector<uLong> > m_OFChecksumsAll;

  std::vector<std::string> m_outFileNames;
  std::vector<std::string> m_guids;
	
  std::vector< std::vector<uint64_t> > m_mergeOffsets;
  std::vector< std::vector<uint64_t> > m_OFSizes;
  std::vector<uint32_t> m_numcontained_events_in_merge_files;
	
};

#endif // MERGED_RAW_FILE
