#include "EventStorage/fRead.h"
#include "ESLMultiFile.h"
#include "ESLMergeFile.h"
#include "ESLOriginalFile.h"
#include "DataReaderController.h"
#include "MergedRawFile.h"
#include "EventStorage/RawFileName.h"

ESLMultiFile::ESLMultiFile(fRead* fR) : EventStackLayer(fR){
  m_me = "ESLMulti"; 
  ERS_DEBUG(3, identify() << " Constructor called ");
  m_currentFileIndex = 0 ;
  m_sumsize = 0;
  m_rawfilename = NULL;
}  

ESLMultiFile::ESLMultiFile(){ 
  m_rawfilename = NULL;
}  

	
//This methods takes the next string from the vector, opens, the file and tries to look at the starting marker. According to this, it decides if the file is an originalfile or a mergefile.
	
EventStackLayer* ESLMultiFile::openNext()
{
		
  EventStackLayer* nextESL = 0;
		
  //m_currentFileIndex++; //this is already done in moreEvents;

 
  ERS_DEBUG(3, " Closing open file ");
  if ( m_fR->isOpen() ) m_fR->closeFile();

  std::string next = m_filenames.at(m_currentFileIndex);

  ERS_DEBUG(3, this->identify() << " Trying to open next file: " << next);

  m_fR->openFile(next);
		
  ERS_DEBUG(3, this->identify() << " file opened " << next);
		
		
  m_fR->setCurrentEndOfFile(-1);

  ERS_DEBUG(3, this->identify() << " set current end of " << next);


  //m_rawfilename = new daq::RawFileName(next);

  uint32_t marker = m_fR->readuint32_t();


  ERS_DEBUG(3, this->identify() << " reading start " << next);

		
  if ( marker == FILE_START_MARKER) //It's an Original File
    {
      ERS_DEBUG(1, this->identify() << " found original file");

      ESLOriginalFile* tmpofp = new ESLOriginalFile(m_fR);
      tmpofp -> setFile(next);
      tmpofp -> handleOffset();
      tmpofp -> setResponsibleForMetaData();
			
      nextESL = tmpofp;
    }
  else	if ( marker == MergedRawFile::FileMarker ) //It's a Merged File
    {

      ERS_DEBUG(1, this->identify() << " found merge file");

      ESLMergeFile* tmpmf = new ESLMergeFile(m_fR);
      tmpmf -> setFile(next);
      tmpmf -> handleOffset();
      tmpmf -> setResponsibleForMetaData();
			
      nextESL = tmpmf;
    }
  else
    {
  		EventStorage::ES_WrongFileFormat ci(ERS_HERE, "file is of no know format. Abort.");
  		throw ci;
      
    }
		
		
  nextESL->prepare();
  try
  {
  	m_sumsize += nextESL->dataMB_InFile();
	}
	catch(...)
	{
	
	}

  return nextESL;
		
}

bool ESLMultiFile::handlesContinuation()
{
  return true;
}
	
void ESLMultiFile::prepare()
{
  return;
}

bool ESLMultiFile::doneLoading()
{
  //multifile cannot be the last level, since it has to load either the mergefile or the originalfile.
  return false;
}

void ESLMultiFile::advance()
{
  m_currentFileIndex++;	
  ERS_DEBUG(1, this->identify() << " advancing to next file " << m_currentFileIndex);
}
	
bool ESLMultiFile::moreEvents() 
{
  ERS_DEBUG(3, this->identify() << " asked for more events ");


  if (m_filenames.size() == 0 ) return false;
  if (m_currentFileIndex > m_filenames.size()-1) 
    {
      m_finished = true;
      return false;
    }
  return true;
}
	
unsigned int ESLMultiFile::getSum()
{
  return m_sumsize;
	
}


void ESLMultiFile::setContinuationFile(std::string next) 
{
  ERS_DEBUG(2, " Pushing : " << next << " on " << identify());
  m_filenames.push_back(next);
  ERS_DEBUG(2, " size now : " << m_filenames.size() );
}

void ESLMultiFile::setFile(std::string filename)
{
  m_filenames.push_back(filename);
}

void ESLMultiFile::setFiles(std::vector<std::string> filenames)
{
  m_filenames = filenames;
}



