#include "Adler32Thread.h"

#include "zlib.h"

#include "ers/ers.h"

Adler32Thread::Adler32Thread()
	: data_(nullptr)
	, length_(0)
	, result_(0)
	, exit_(false)
{
  try {
    thread_ = std::thread(&Adler32Thread::compute_loop, this);
  } catch (std::system_error const & exc) {
    // Improve default exception: it would just print "Resource temporarily
    // unavailable" with no context which is difficult to understand.
    ERS_LOG("Adler32Thread: system_error: could not create thread: " << exc.what());
    throw; // re-throw the exception anyway
  }
}

Adler32Thread::~Adler32Thread() {
	stop();
	thread_.join();
}

void Adler32Thread::stop() {
	std::unique_lock<std::mutex> mlock(mutex_);
	exit_ = true;
	mlock.unlock();
	condvar_.notify_one();
}

void Adler32Thread::compute_checksum(unsigned char const * data, int len) {
	if (!data) return;
	std::unique_lock<std::mutex> mlock(mutex_);
	data_ = data;
	length_ = len;
	mlock.unlock();
	condvar_.notify_one();
}

unsigned long Adler32Thread::get_result() {
	std::unique_lock<std::mutex> mlock(mutex_, std::defer_lock_t());
	while (true) {
		mlock.lock();
		if (data_ == nullptr) {
			break;
		}
		mlock.unlock();
	}

	unsigned long const res = result_;
	result_ = 0;
	return res;
}

void Adler32Thread::compute_loop() {
	while (!exit_) {
		std::unique_lock<std::mutex> mlock(mutex_);
		while (!data_ && !exit_) {
			condvar_.wait(mlock);
		}
		if (exit_)
			return;

		result_ = adler32(1L, data_, length_);
		data_ = nullptr;
		length_ = 0;
	}
}
