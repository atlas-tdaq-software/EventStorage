#include "ESLOriginalFile.h"
#include "MergedRawFile.h"
#include "ESLMergeFile.h"
#include "DataReaderController.h"
#include "EventStorage/RawFileName.h"




ESLMergeFile::ESLMergeFile( fRead* fR) : EventStackLayer(fR)
{
  m_me = "ESLMerge"; 
  m_currentEncapsulatedFile=0;
  m_currentHeader = new MergedRawFile(false);

  ERS_DEBUG(3, identify() << " Constructor called ");

  m_RawFileName = 0;
  m_enc1_compression = EventStorage::NONE;
}
 
ESLMergeFile::~ESLMergeFile()
{
  delete m_currentHeader;
  delete m_RawFileName;
}
 
void ESLMergeFile::advance()
{
  m_currentEncapsulatedFile++;	
}
	
EventStackLayer* ESLMergeFile::openNext()
{
  return this->openNextImpl(true);
}

 
EventStackLayer* ESLMergeFile::openNextImpl(bool skiptoevents)
{

  EventStackLayer* result;
		
  //m_currentEncapsulatedFile++; //this is done in advance() method
		
		
  ERS_DEBUG(1,"ESLMergeFile jumps to enc file " << m_currentEncapsulatedFile << ", total num of enc files " << m_numEncapsulatedFiles);

  ERS_DEBUG(1,"ESLMergeFile: Jumping to beginning of file " << m_currentEncapsulatedFile << " at offset " << m_FileOfsts.at( m_currentEncapsulatedFile ) << " with size " << m_FileSizes.at( m_currentEncapsulatedFile ) );

  m_fR->setPosition( m_FileOfsts.at( m_currentEncapsulatedFile ) );
		
		
  uint64_t endofffset =  (uint64_t)m_FileOfsts.at( m_currentEncapsulatedFile ) + (uint64_t)m_FileSizes.at( m_currentEncapsulatedFile );
			
  //	ERS_DEBUG(1,"JUMPING TO OFFSET " << m_FileOfsts.at( m_currentEncapsulatedFile ));

		
			
  m_fR->setCurrentEndOfFile( endofffset );
		
  ESLOriginalFile *nextOFile = new ESLOriginalFile(m_fR);
  nextOFile -> prepare();
  result = nextOFile;

  if (m_currentEncapsulatedFile==0) 
    {
      //if this is the first encapsulated file, mergefiles copies the run parameter records from it
      this->setRPR(nextOFile->getRunParameterrecord());
      this->setFMDS(nextOFile->freeMetaDataStrings()); //Merged Files provide FMDS from first file
      this->setCompression(nextOFile->compression());
    }

  /*
    If we are asked to do so ('skiptoevents'), we skip all the internal 
    files that do not contain events. Anyway, given the merged file 
    interface, they are essentially invisible outside. If the last file
    is empty, we return it, since there is no alternative.
   */
  if(skiptoevents && !nextOFile->moreEvents() && 
     m_currentEncapsulatedFile < (m_numEncapsulatedFiles-1)){
    ERS_DEBUG(1,"File has no events. Skip it! Current file index: " 
	      << m_currentEncapsulatedFile
	      << " #Files: " << m_numEncapsulatedFiles);
    delete result;
    this->advance();
    result = this->openNextImpl(true);
  }

		
  return result;
}


EventStackLayer* ESLMergeFile::loadAtOffset(int64_t position, EventStackLayer *old)
{

  if ( (uint64_t)position > m_currentHeader->filesize() )
    {
			EventStorage::ES_OutOfFileBoundary ci(ERS_HERE, "Trying to jump outside Filesize Boundary. Abort.");
  	throw ci;

    }
  ERS_DEBUG(3, identify() << " tries to jump to index " << position);
		
  //the following block of code identifies in ehic encapsulated file the searched index lies, it then opens it.
  bool done = false;
  unsigned int it = 0;
  int whichfile=-1;
		
  ERS_DEBUG(3, identify() << " tries to jump to index " << position << " " << m_FileOfsts.size() );

		
  while ( it < m_FileOfsts.size() && !done )
    {
      //it++;

      ERS_DEBUG(3, "Probing " << m_FileOfsts.at(  it  ) << " " << m_FileOfsts.at( it+1 ) << " ");
 			
      if 	( ( m_FileOfsts.size()==(it+1)  ||  m_FileOfsts.at( it+1 )  > (uint64_t)position   )	&&
		  ( m_FileOfsts.at(  it  ) <= (uint64_t)position) )
	{
	  done = true;
	  whichfile = it;
	}
      it++;
    }
		
  //it would b nicer to have a method like openFile(int num), but this does the trick
  EventStackLayer *gen;
  
  if ( static_cast<int>(m_currentEncapsulatedFile) == whichfile)
  {
  	gen = old;
  }
  else
  {
    m_currentEncapsulatedFile = whichfile ;
    gen = this->openNextImpl();
  }
  
  m_fR->setPosition(position);

  return gen;
		
	
}

	
void ESLMergeFile::prepare()
{
  //see below
  parseHeader();
		
}

bool ESLMergeFile::doneLoading()
{
  //mergefile cannot be the last level, since it encapsulates original files,
  return false;
}

	
bool ESLMergeFile::moreEvents() 
{
  if (m_numEncapsulatedFiles <= m_currentEncapsulatedFile ) 
    {
      m_finished = true;
      return false;
			
    }
  return true;		
}

unsigned int ESLMergeFile::nextInSequence(unsigned int current)
{
  if (current){};
  ERS_DEBUG(2,"ESLMErge nextInSequence called " << m_currentHeader->next_file_sequence_number() );

  return m_currentHeader->next_file_sequence_number();
}

bool ESLMergeFile::endOfFileSequence() 
{
		
  ERS_DEBUG(2,"ESLMErge endOfFileSequence called " << m_currentHeader->next_file_sequence_number() << " " << m_currentHeader->this_file_sequence_number());
		
  if (m_currentHeader->next_file_sequence_number() ==
      m_currentHeader->this_file_sequence_number() ) return true;
  else return false;
}

void ESLMergeFile::setContinuationFile(std::string next) 
{
  //not used
  if (next == "") ERS_DEBUG(2, "called with empty sttring");
}

void ESLMergeFile::parseHeader()
{


  /*
    ERS_DEBUG(1,"Parsing MergeFile Header");
			
    if(unfile_record(&m_last_merge_start_pattern,&merge_start_pattern) == 0) 
    {
    mythrowE( "No MergedFile found. Abort");
    }			
			
    m_numEncapsulatedFiles = m_last_merge_start_pattern.num_merged_files;
			
    for ( int a = 0; a < m_numEncapsulatedFiles ; a++ )
    {
    uint64_t tmp_size;
    uint64_t tmp_offset;
	
    ERS_DEBUG(3,"Reading file size of File " << a);
    m_fR->readData((char *)(&tmp_size),sizeof(uint64_t));
    ERS_DEBUG(3,"Reading file offset of File " << a);
    m_fR->readData((char *)(&tmp_offset),sizeof(uint64_t));
    ERS_DEBUG(3,"Got Size " << tmp_size << " " << " Offset " << tmp_offset);
    m_FileSizes.push_back(tmp_size);
    m_FileOfsts.push_back(tmp_offset);
		
    }
  */
			
  int64_t op = m_fR->getPosition();

  int hsiz = 0; 	 
  for (unsigned int z = 0 ; z< 3 ; z++)
    {
      uint32_t word = 0; 
      m_fR->readData((char*)&word,sizeof(uint32_t));
      if ( z == 0 && word!=MergedRawFile::FileMarker)
	{
  	EventStorage::ES_WrongFileFormat ci(ERS_HERE, "No MergedFile found. Abort");
  	throw ci;

	}
      hsiz = word;
    }
  m_fR->setPosition(op);	

  uint32_t* data = new uint32_t[hsiz];  
  //reserving space for header data		
  m_fR->readData((char*)data,hsiz*sizeof(uint32_t));
  //reading the header data
		
  m_currentHeader->unpackHeader(data);
		
  delete[] data;
		
  m_FileSizes = m_currentHeader->filesizes();
  m_FileOfsts = m_currentHeader->fileoffsets();
	
  std::string nm = m_currentHeader->merged_filename();
	
  m_RawFileName = new daq::RawFileName(nm);
	
  m_numEncapsulatedFiles = m_currentHeader->num_contained_files();
  ERS_DEBUG(1,"giz " <<m_numEncapsulatedFiles<<" encfiles");
	
}

void ESLMergeFile::setRPR(internal_run_parameters_record trpr)
{
  m_enc1_internal_latest_rpr = trpr;
}

void ESLMergeFile::setFMDS(EventStorage::freeMetaDataStrings tfmds)
{
  m_enc1_fmdStrings = tfmds;
}
	
void ESLMergeFile::setCompression(EventStorage::CompressionType comp){
  m_enc1_compression = comp;
}


std::string ESLMergeFile::GUID() const
{
  return m_currentHeader->guid();
}

unsigned int ESLMergeFile::fileStartDate() const 
{
  return m_currentHeader->open_date();
}

unsigned int ESLMergeFile::fileStartTime() const 
{
  return m_currentHeader->open_time();
}


//Run parameter get records
unsigned int ESLMergeFile::runNumber() const {

  //  return m_enc1_internal_latest_rpr.run_number;
  return m_currentHeader->run_number();


}

     
unsigned int ESLMergeFile::maxEvents() const {
  return  m_enc1_internal_latest_rpr.max_events;
}

     
unsigned int ESLMergeFile::recEnable() const {
  return  m_enc1_internal_latest_rpr.rec_enable;
}

     
unsigned int ESLMergeFile::triggerType() const {
  return m_enc1_internal_latest_rpr.trigger_type;
}

   
std::bitset<128> ESLMergeFile::detectorMask() const {
  
  uint64_t tmp = m_enc1_internal_latest_rpr.detector_mask_4of4;
  tmp <<= 32;
  tmp |= m_enc1_internal_latest_rpr.detector_mask_3of4;
  
  std::bitset<128> mask(tmp);
  mask <<= 64;
  
  tmp = m_enc1_internal_latest_rpr.detector_mask_2of4;
  tmp <<= 32;
  tmp |= m_enc1_internal_latest_rpr.detector_mask_1of4;

  mask |= tmp;
  return mask;
}

unsigned int ESLMergeFile::beamType() const {
  return m_enc1_internal_latest_rpr.beam_type;
}

unsigned int ESLMergeFile::beamEnergy() const {
  return  m_enc1_internal_latest_rpr.beam_energy;
}

std::vector<std::string> ESLMergeFile::freeMetaDataStrings() const
{
  return m_enc1_fmdStrings;
}

EventStorage::CompressionType  ESLMergeFile::compression() const {
  return m_enc1_compression;
}

unsigned int ESLMergeFile::fileEndDate()
{
  return m_currentHeader->open_date();
}
///< Date when writing has stopped.
unsigned int ESLMergeFile::fileEndTime() 
{
  return m_currentHeader->open_time();
}
///< Time when writing has stopped.
unsigned int ESLMergeFile::eventsInFile() 
{
  //return m_last_merge_start_pattern.numEvents;
  return m_currentHeader->num_contained_events();
}
///< Number of events in this file.
unsigned int ESLMergeFile::dataMB_InFile() 
{
  return m_currentHeader->filesize()/1024/1024;
}
///< Number of megabytes in this file.
unsigned int ESLMergeFile::eventsInFileSequence() 
{
  return m_currentHeader->num_events_in_sequence();
}
///< Number of events in this file sequence written so far.
unsigned int ESLMergeFile::dataMB_InFileSequence() 
{
  return 0;
}

uint32_t ESLMergeFile::lumiblockNumber()
{
  return m_currentHeader->lumi_block_number();
}
std::string ESLMergeFile::stream()
{
  return m_currentHeader->stream();
}
std::string ESLMergeFile::projectTag()
{
  return m_currentHeader->project();
}



unsigned int ESLMergeFile::fileSizeLimitInDataBlocks() const 
{
  return 0; 
}

unsigned int ESLMergeFile::fileSizeLimitInMB() const 
{
  return m_currentHeader->file_size_limit_MB(); 
}

std::string ESLMergeFile::appName() const
{	
  std::string appName;
  if ( m_RawFileName->hasValidCore() ) 
    {
      appName = m_RawFileName->applicationName();
    }
  else 
    {
      appName =  "";
    }
  return appName;
}
	
std::string ESLMergeFile::fileNameCore() const
{
  return m_RawFileName->fileNameCore();
}
		




///< Number of MB in this file sequence written so far. 
// unsigned int ESLMergeFile::fileStatusWord() 
//{
//  return m_last_merge_start_pattern.continuation;
//}
///< Indicates an end of sequence. 

