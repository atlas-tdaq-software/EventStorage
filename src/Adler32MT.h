#ifndef ADLER32MT_H_
#define ADLER32MT_H_

#include <vector>
#include <memory>

#include "EventStorage/Adler32MTDefaults.h"
#include "Adler32Thread.h"

namespace unit_tests { struct Adler32MT_inspector; }

class Adler32MT
{
public:

  /* nb_threads: total number of threads that will compute adler32, including
   *   the calling thread.
   *   0 does not make sense but still works: only the calling thread will
   *     compute adler32.
   *   1: only the calling thread will compute adler32. No overhead except the
   *     extra function call w.r.t. calling ::adler32 directly
   *   Any positive value N: N-1 additional threads are spawned to compute
   *     adler32 in parallel with the calling thread.
   *   Any negative value: equivalent to
   *     nb_threads = std::thread::hardware_concurrency()
   *     This includes every hyper-threads available.
   *
   * multithread_threshold: There is an overhead for the multithread processing,
   *   so using multiple threads is only worth if the data is large enough.
   *   Measurements were made the determine DEFAULT_THRESHOLD, but this may
   *   depend on hardware and on the threading library implementation.
   *   Use alder32mt_threshold binary to measure thresholds for different
   *   nb_threads values on a specific machine.
   *
   *   0: all requests are split in nb_threads chunks
   *   Any strictly positive value: smallest data chunk that can be sent to an
   *     additional computing thread
   *
   * Excluding the special cases:
   * - threshold = 0: all requests split among all available threads
   * - nb threads = 0 or 1: no split
   * the input request is divided into the greatest possible number of chunks
   * in the limit of available threads such that a resulting chunk is not
   * smaller than the threshold. Or more simply:
   *   nb_chunks = min(nb_threads, request_length / threshold)
   */
  Adler32MT(
        int nb_threads = Adler32MTDefaults::NB_THREADS
      , uint32_t multithread_threshold = Adler32MTDefaults::THRESHOLD
      );

  Adler32MT(const Adler32MT &) = delete;
  ~Adler32MT();

  /* Return adler32 checksum of buffer computed by nb_threads threads
  * in parallel *including* the calling thread.
  * This function is NOT thread-safe. It can be called from 1 thread only.
  * It was designed so that each caller has its own instance of this class.
  */
  unsigned long adler32(unsigned long adler, unsigned char const * buffer, int length);

private:
  int const mt_threshold;
  int const total_nb_threads; // including the calling thread so nb_threads = threads.size + 1
  std::vector<std::unique_ptr<Adler32Thread>> threads;

  struct Stats
  {
    Stats()
      : nb_requests(0)
      , nb_mt(0)
      , total_size(0)
      , mt_size(0)
      , nb_chunks(0)
    {}

    int64_t nb_requests; // total nb of requests (calls to adler32(..))
    int64_t nb_mt; // nb of requests that have been split for multithread computing
    int64_t total_size; // total amount of data sent to adler32(..)
    int64_t mt_size; // amount of data for request split for multithread computing
    int64_t nb_chunks; // nb_chunks/nb_mt = average number of chunks per mt request

    /* nb_mt / nb_requests: fraction of requests taking advantage of MT
     * mt_size / total_size: fraction of data taking advantage of MT
     */
  };
  Stats stats;

  friend struct unit_tests::Adler32MT_inspector;
 };

#endif /* ADLER32MT_H_ */
