
#include "EventStorage/DWCBcout.h"

#include <iostream>

DWCBcout::DWCBcout(std::string appName) :
  m_appName(appName)
{}

void DWCBcout::FileWasOpened(
			     std::string logicalfileName,
			     std::string fileName,
			     std::string streamtype,
			     std::string streamname,
			     std::string sfoid,
			     std::string guid,
			     unsigned int runNumber, 
			     unsigned int filenumber,
			     unsigned int lumiblock)
{
  std::cout << "Application " << m_appName << " in run " << runNumber
	    << " has opened file with: " << std::endl
            << "logical name: " << logicalfileName << std::endl 
            << "physical name: " << fileName << std::endl 
            << "file number: " << filenumber << std::endl 
            << "guid: " << guid << std::endl 
            << "stream type: " << streamtype << std::endl 
            << "stream name: " << streamname << std::endl
            << "LB: " << lumiblock << std::endl
	    << "SFOid: " << sfoid << std::endl << std::flush;
}

void DWCBcout::FileWasClosed(
			     std::string logicalfileName,
			     std::string fileName,
			     std::string streamtype,
			     std::string streamname,
			     std::string sfoid,
			     std::string guid,
                             std::string checksum,
			     unsigned int eventsInFile, 
			     unsigned int runNumber, 
			     unsigned int filenumber,
			     unsigned int lumiblock,
			     uint64_t filesize)
{
  std::cout << "Application " << m_appName << " in run " << runNumber
	    << " has closed file with: " << std::endl
            << "logical name: " << logicalfileName << std::endl 
            << "physical name: " << fileName << std::endl 
            << "file number: " << filenumber << std::endl 
            << "guid: " << guid << std::endl 
            << "checksum: " << checksum << std::endl 
            << "stream type: " << streamtype << std::endl 
            << "stream name: " << streamname << std::endl
            << "LB: " << lumiblock << std::endl 
            << "number of events: " << eventsInFile << std::endl 
            << "file size: " << filesize 
	    << "SFOid: " << sfoid << std::endl << std::flush;
}
