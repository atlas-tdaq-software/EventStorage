
#include "EventStorage/ESCompression.h"

EventStorage::CompressionType 
EventStorage::string_to_type(const std::string& type){
  
  if(type=="reserved"){
    return EventStorage::RESERVED;
  }else if(type=="unknown"){
    return EventStorage::UNKNOWN;
  }else if(type=="none"){
    return EventStorage::NONE;
  }else if(type=="zlib"){
    return EventStorage::ZLIB;
  }else{
    return EventStorage::UNKNOWN;
  }
}

std::string 
EventStorage::type_to_string(const CompressionType& type){
  
  switch(type){
  case NONE:
    return "none";
    break;
  case RESERVED:
    return "reserved";
    break;
  case UNKNOWN:
    return "unknown";
    break;
  case ZLIB:
    return "zlib";
    break;
  }

  return "unknown";
}
