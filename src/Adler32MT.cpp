#include "Adler32MT.h"

#include "ers/ers.h"

#include "zlib.h"

#include <iostream>
#include <memory>
#include <cmath>

Adler32MT::Adler32MT(int nb_threads, uint32_t multithread_threshold)
	: mt_threshold(multithread_threshold)
	, total_nb_threads((nb_threads < 0) ? std::thread::hardware_concurrency() : nb_threads)
{
	for (int i = 0; i < total_nb_threads - 1; ++i) {
		threads.emplace_back(new Adler32Thread);
	}

	ERS_DEBUG(0, "adler32mt instantiated: nb_threads= " << total_nb_threads
			<< ", threshold= " << mt_threshold);
}

Adler32MT::~Adler32MT()
{
	ERS_DEBUG(0, "nb_requests= " << stats.nb_requests
			<< " nb_mt= " << stats.nb_mt
			<< " totalsize= " << stats.total_size
			<< " mtsize= " << stats.mt_size
			<< " avg_nb_chunks= " << ((stats.nb_mt == 0) ? 0 : static_cast<double>(stats.nb_chunks)/stats.nb_mt));
}

unsigned long Adler32MT::adler32(unsigned long adler, unsigned char const * data, int length)
{
	stats.nb_requests += 1;
	stats.total_size += length;

	// Size to compute in this thread
	int length_th1 = length;
	// Size to send to other threads
	int length_thn = 0;

	/* Into how many chunks can the input data be split?
	 * At most length / threshold (Otherwise the chunksize will be smaller than threshold)
	 * Or the number of available threads if smaller.
	 * no threads => no split
	 * 0 threshold: split all requests amongst all available threads
	 */
	int const nb_chunks = (mt_threshold == 0) ? total_nb_threads : (
			(total_nb_threads < 2) ? 1
			: std::min(total_nb_threads , length / mt_threshold));

	if (nb_chunks > 1) {
		stats.nb_mt += 1;
		stats.mt_size += length;
		stats.nb_chunks += nb_chunks;

		length_thn = length / nb_chunks;
		length_th1 = length - ((nb_chunks - 1) * length_thn);

		for (int i = 0; i < (nb_chunks - 1); ++i) {
			auto & thr = threads[i];
			thr->compute_checksum(data + length_th1 + (i * length_thn), length_thn);
		}
	}

	unsigned long checksum = ::adler32(adler, data, length_th1);

	if (nb_chunks > 1) {
		for (int i = 0; i < (nb_chunks - 1); ++i) {
			auto & thr = threads[i];
			unsigned long const tmp = thr->get_result(); // blocking call
			checksum = adler32_combine(checksum, tmp, length_thn);
		}
	}

	return checksum;
}
