2010-01-18 C.Topfel <cyril.topfel@cern.ch>
	* Summary of Changes in the last few Months
	* New Exceptions defined for DataReader:
	* ES_SquenceNextFileMissing
	* ES_AllocatingMemoryFailed
	* ES_AllocatedMemoryTooLittle
	* ES_NoEndOfFileRecord
	* ES_WrongEventSize
	* ES_NoEventFound
	* ES_OutOfFileBoundary
	* ES_WrongFileFormat
	* ES_InternalError
	* Updated MetaData Behavior
	* FileNameCallback Class to simplify the FileNameHandling (DataWriter)
	* Exceptions for DataWriter
	* ES_SingleFile
	* ES_SingleFileAlreadyExists

2009-02-25 C.Topfel <cyril.topfel@cern.ch>
        *  Fixed a bug where offset based-read crashed on the last event, because stack was already empty.
        Tag v1r25p9


2009-02-12 K.Kordas <kostas.kordas@cern.ch>
	* fix some compilation warnings
	Tag v1r25p8

2009-01-28 C.Topfel <cyril.topfel@cern.ch>
        *  Beautified code indentation
        *  added script in share to de- and reMerge
        Tag v1r25p3


2009-01-20 C.Topfel <cyril.topfel@cern.ch>
        *  testDeMerge outputs error 
	*  testDeMerge supports onlylist to look what's inside a merged file
	*  testDeMerge <mergefile> onlylist
        Tag v1r24p5


2009-01-20 C.Topfel <cyril.topfel@cern.ch>
        *  Added DeMerging Application (testDeMerge)
        Tag v1r24p4


2009-01-20 C.Topfel <cyril.topfel@cern.ch>
        *  Changed python transformation script to include event number(s) in the jobReport pickle file
        *  Added dataset support in TRF Script
        *  Fixed some typos in doc
        Tag v1r24p3


2009-01-08 C.Topfel <cyril.topfel@cern.ch>
        *  Added python-transformation for Tier-0 integration in share
        *  Fixed a bug where different input lumibock files (which however must not happen) caused strange behavior
	*  Updated doc
        Tag v1r24p2


2008-12-01 C.Topfel <cyril.topfel@cern.ch>
        *  Fixed behavior of endOfFile(), now correctly returns true if file is at the end
        *  DataReaderController now closes openend file upon Destruction
        Tag v1r23p3


2008-11-21 K.Kordas <kostas.kordas@cern.ch>
	* merge the changes which went into the tdaq-common-01-10-00_patches branch
	* (v1r22p17) back into the main branch
	Tag v1r23p2

2008-11-21 K.Kordas <kostas.kordas@cern.ch>
	* doc/RELEASE_NOTES.tdaq-common-01-10-00.html
	* - give the example trhe offline wanted: 
	*   constructing a RawFileName with any filename as they want it
	*   (there is no need this name to be interpretable for runNumber etc)
	Tag v1r22p17

2008-11-21 K.Kordas <kostas.kordas@cern.ch>
	* src/RawFileName.cxx
	*  - correct the interpretation of the fileName (boost tokenizer
	*    needs at Cstring, not a string, nor a char* as seperator;
	*    using char* before, resulted in unpredicted spliting behaviour)
	*  - since the fileNameTag is no longer used, use the nameTag() method
	*    to get the datasetName (which is the 1st part of the fileName)
	* src/MergedRawFile.cxx
	*  - fix memory leaks (non deleted objects)
	Tag v1r22p16

2008-11-19 K. Kordas <kostas.kordas@cern.ch> , C. Topfel <cyril.topfel@cern.ch>
	* make sure we commit the changes to the patch branch
	Tag v1r22p15

2008-11-19 K. Kordas <kostas.kordas@cern.ch>
	* src/RawFileName.cxx
	*  - get rid of the fileNameTag in the fileName
	*  - correct the interpretation of the fileName and avoid using the 
	*    static methods split() and convertToUINT().
	* src/DataWriter.cxx
	*   get rid of warning when the project, lumiblock, stream etc info
	*   is not filled into the meta-data strings in the file header
	* fix compilation warnings of unused parameters
	Tag v1r22p14
	
2008-11-14 C. Topfel <ctopfel@cern.ch>, K. Kordas <kostas.kordas@cern.ch>
	*  src/DataWriter.cxx
	*     Fix DataWriter callback interface to use correctely the logical 
	*     and the physical filename
	*  src/DataReaderController.cxx
	*     use RawFileName objects where appropriate for consistency with
	*     the DataWriter
	*  src/RawFileName.cxx 
	*     cure the "." problem at end of fileNameCore , 
	*     even when interpretation fails
	Tag v1r22p13

2008-11-13 Cyril Topfel <cyril.topfel@cern.ch>
        *  v1r22p11 = v1r22p10, goes to tdaq-common-01-10-00_patches branch
	Tag v1r22p11


2008-11-13 Cyril Topfel <cyril.topfel@cern.ch>
        *   Fixed non-virtualness of getFreeMetaDataStrings in EventStackLayer.h 
        Tag v1r22p10

2008-11-13 Cyril Topfel <cyril.topfel@cern.ch>
        *  Fixed a bug where fileNameCore could end with . 
        Tag v1r22p9

2008-11-12 Kostas Kordas <kostas.kordas@cern.ch>
	* RawFileName.cxx
	*  - Fix bug in spliting the filename for interpretation 
	*    (causing SEGV under some cases) 
	Tag v1r22p8

2008-11-04 Kostas Kordas <kostas.kordas@cern.ch>
	* RawFileName.h,cxx		
	*  Throw ERS_ISSUE when asked for fields of non-interpreted fileName
	* cmt/reaquirements
	*  Fix compiling and linking dependencies
	* v1r22p1-v1r22p7
	Tag v1r22p7
	
2008-11-03 Kostas Kordas <kostas.kordas@cern.ch>
	* RawFileName class (new), DataWriter, DataReader
	*  - New RawFileName class with knowledge of the fileName fields: 
	*    used in DataWriter for consistent fileName treatment. 
	*  - Get rid of AgreedFileName method, and FileNameParameters 
	*    definitions: all functionality and constants are found in the 
	*    EventStorage/RawFileName.h file
	Tag v1r22p0
	
2008-10-31 C. Topfel <ctopfel@cern.ch>, K. Kordas <kostas.kordas@cern.ch>
	* lots of files....
	*  Writing and Reading Merged Files. 
	*  Added RawMergedFile class to allow the writing of merged raw files
	*  Changed DataReader Design to read such files transparentely
	Tag v1r21p0
	
2008-10-31 Cyril Topfel <ctopfel@cern.ch>
	* DataWriter
	*  Add the project, stream and lumiBlock info into the meta-data 
	*  string record of the raw file header
	Tag v1r20p0

2008-10-31 Cyril Topfel <ctopfel@cern.ch>
	* DataReader 
	*  allow DataReader to accept preallocated memory block
	Tag v1r19p1

2008-10-27 Cyril Topfel <ctopfel@cern.ch>
	* DataWriter
	*  Optional definition of the starting file index in the constructor
	Tag v1r19p0 
	
2008-04-27 Wainer Vandelli <wainer.vandelli@cern.ch>
	* src/DataWriter.cxx
	*  Checksum calculation added
	Tag v1r17p24

2008-03-25 Wainer Vandelli <wainer.vandelli@cern.ch>
	* Old pickDataReader constructor removed
	*  Added support for checksum calculation
	Tag v1r17p22

2008-02-23 Andreas Battaglia <andreas.battaglia@cern.ch>
	*  Restore pickDataReader(readingPath,appNameWithTag,runNumber,fileNumber). 
	*  The file name constructed out of those parameters represent the DAQ file name convention from 2003 till 2007
	*  The files affected are:
	*  - DataReaderBase, pickDataReader
	
	* Tag v1r17p21

2008-02-14 Andreas Battaglia <andreas.battaglia@cern.ch>
	*  Small memory leak in readOptionalFreeMetaDataStrings() solved 
	*  The files affected are:
	*  - DataReaderFormat, DataReaderFormat2
	*  Added release notes for tdaq-common-01-09-01
	
	* Tag v1r17p20

2008-02-11 Wainer Vandelli <wainer.vandelli@cern.ch> and Andreas Battaglia <andreas.battaglia@cern.ch>
	*  Call back information extended (in FileWasOpen() and FileWasClosed())
	*  The files affected are:
	*  - DataWriterCallback, DataWriter, DWCBcout 
	*  - DataReaderFormat
	
	* Tag v1r17p19


2008-02-07 Andreas Battaglia <andreas.battaglia@cern.ch>
	*  New application FileConverter (test/FileConverter.cxx) to convert files with format 2 into files with format 5
	
	* Tag v1r17p18
		
2008-02-05 Wainer Vandelli <wainer.vandelli@cern.ch> and Andreas Battaglia <andreas.battaglia@cern.ch>
	*  Restore the backward compatibility with the old format version 2 (2003-2007)
	*  Now the Event Storage can read files with format 2 (2003-2007) and format 5 (since 2008), 
	*  and write files with format 5 (the number 5 will reflect the future updated version of the Event Storage document)
	*  The Detector Mask is:
	*  - one 64 bit word in IS
	*  - one 32 bit word in format 2 files
	*  - two 32 bit words in format 5 files.
	*  The files affected are:
	*  - DataReaderFormat1, DataReaderFormat2, EventStorageRecords2003 still present, but not used;
	*  - EventStorageRecords, DataReaderFormat, DataWriter for the main changes;
	*  - DataReaderBase, pickDataReader for minor changes
	
	* Tag v1r17p16, v1r17p17

2008-02-04 Wainer Vandelli <wainer.vandelli@cern.ch> and Andreas Battaglia <andreas.battaglia@cern.ch>
	*  Delete backward compatibility with old format version 1 (up to 2003) and 2 (up to 2007)
	*  New format version: 5
	
	* Tag v1r17p15

2008-02-02 Wainer Vandelli <wainer.vandelli@cern.ch> and Andreas Battaglia <andreas.battaglia@cern.ch>
	*  put the detector mask as 2 32bit words
	
	* Tag v1r17p14 

2008-01-21 Andreas Battaglia <andreas.battaglia@cern.ch>
	* Restore the detector_mask to be a 32-bit info
	
	* Tag v1r17p12, v1r17p13 

2007-12-06 Kostas Kordas <kostas.kordas@cern.ch>
	* Correct the detector_mask to be a 64-bit info: uint64_t
	* (the detector_mask is reported in the run parameters record of the data file)
	
	* Tag v1r17p11

2007-08-28 Kostas Kordas <kostas.kordas@cern.ch>
	* transport v1r17p9 changes into the HEAD development

	* Tag v1r17p10

2007-08-28 Wainer Vandelli <wainer.vandelli@cern.ch> and Kostas Kordas <kostas.kordas@cern.ch>
	* src/DataWriter.cxx:openNextFile()
	*  - Clear the file descriptor just before doing an open [ just added  m_cFile.clear(); ]
	*    This cures the problem of any remaining previous failure in dealing with the file:
	*    (this 'uncleared' failure was preventing us from any further attempt to do anything after 
	*    (e.g., open a new file) fails.

	* Tag v1r17p9

2007-06-12 Andreas Battaglia <andreas.battaglia@cern.ch>

	* Replace all ERS_WARNING, ERROR and FATAL with declared issues.
	
	* Tag v1r17p8.

	
2007-06-07 Andreas Battaglia <andreas.battaglia@cern.ch>

	* Change the writing data files name extention, from .writing to .data.writing
	(like in v1r17p4 in the tdaq-common-01-06-01_patches branch)
	
	* Tag v1r17p7.


2007-06-07 Andreas Battaglia <andreas.battaglia@cern.ch>

	* Dummy tag to update the nightly
	
	* Tag v1r17p6.


2007-05-04 Andreas Battaglia <andreas.battaglia@cern.ch>

	* Tag the changes which went into the tdaq-common-01-06-01_patches branch (as v1r17p2, see below) 
	into the main branch

	* Tag v1r17p3.


2007-05-04 Andreas Battaglia <andreas.battaglia@cern.ch>

	* In the method CloseFile()in DataWriter.cxx, call the Callback only when you really have an opened file.

	* Tag v1r17p2.

2007-03-28 Szymon <szymon.gadomski@cern.ch>

	* Use int64_t for all the variables related to 
	offsets of events in files.

	* Remove fReaddCache and fReadCastor. They are in offline
	releases. The code is in the Event/ByteStreamStoragePlugins
	package.

	* Tag v1r17p0.
	
2007-03-27 Szymon <szymon.gadomski@cern.ch>

	* Fix a bug in extraction of application name from 
	the file name in the DataWriter.cxx.

2007-03-26 Szymon <szymon.gadomski@cern.ch>

	* Modify the testWriting2dir to reproduce what an SFO 
	does. Compile this program again.

	* Prevent the destructor of the DataWriter from creating 
	an empty file.

	* Add a warning when a file has only metadata and no 
	events at all.
	
	* Tag v1r16p0.
	
2007-03-01 Szymon <szymon.gadomski@cern.ch>

	* Add namespace name in one place.
	* Tag v1r15p6.

2007-02-28 Szymon <szymon.gadomski@cern.ch>

	* Support old file names with file number padded to two 
	digits and not to four.
	* Tag v1r15p5.

2007-02-09 Szymon <szymon.gadomski@cern.ch> 

	* Add a virtual destructor to the DataWriterCallBack 
	interface.
	* Tag v1r15p4.

2007-02-07 Szymon <szymon.gadomski@cern.ch> 

	* A lot of doxygen.
	* Move DataWriter and it's callback interface
	into the EventStorage name space.
	* Tag v1r15p3.

2007-02-05 Szymon <szymon.gadomski@cern.ch> 

	* In DataWriter the offset returned by getPosition
	now refers to the latest event already written.
	Not to the next one to write. This is because
	putData(...) may close a file and open the next one.
	* Tag v1r15p2.

2007-02-02 Szymon <szymon.gadomski@cern.ch> 

	* Improve ERS messages to diagnose problems with 
	files not normally terminated. 
	* Add ERS warnings to Castor and dCash plugins.
	Castor could be tested. Normally they are not compiled
	here anymore.
	* Protections in the method to read GUID.
	* Warnings if there is no metadata at the end of file. 
	* Tag v1r15p1.

2007-02-01 Szymon <szymon.gadomski@cern.ch>

	* Test programs for random acccess using offsets.
	* Tests of random access on files > 2 GB.
	* Improvements of diagnostics with ERS.
	* Remove all cases of COUT_DEBUG.
	* Tag v1r15p0.

2007-01-30 Szymon <szymon.gadomski@cern.ch>

	* New convention of file names. 
	* New representation of file names in variables.
	* New API of the DataReader with respect to handling
	of file sequences.

2007-01-26  Szymon <szymon.gadomski@cern.ch>

	* Added GUID to files when writing. 

2007-01-10 Szymon Gadomski <szymon.gadomski@cern.ch>

	* A way to get current offset while writing.
	* A way to get and set offset while reading. 
	* A way ro read from a given offset.

2006-12-08 Szymon Gadomski <szymon.gadomski@cern.ch>

	* Merge patch development (v1r9p2) to head.
	Avoid reading 0 bytes from all kinds of files.
	Side effects were seen in Castor, as diagnosed
	by Tadashi. 
	
        * In the meantime Andrei was changing requirements 
	and has made tags v1r13p3, v1r13p2, v1r13p1, v1r13p0
	and v1r12p0.

	* Tag v1r14p0. 
	
2006-11-03  Szymon Gadomski <szymon.gadomski@cern.ch>

	* Do not compile the plugin for dCache in the tdaq-common.
	As agreed with Andrei and David Q, it will be in Offline.
	* Tag v1r11p0.

2006-09-08  

	* Avoid a check for end of file when reading format 2. 
	It turns out to be risky and can be avoided in this format.

	* Tolerate appNameWithTag ending with a double underscore.

	* Tag v1r10p2.

2006-08-13  Szymon Gadomski <szymon.gadomski@cern.ch>

	* Merge patch developments, done for tdaq-common-01-04-00
	tagged in a branch as v1r9p1, into the head version:
	
	- Reset the internal flags of dCache.
	
	- Deal with funny prefixes of dCache, which one needs to
	remove in some cases and not in the others.  
	The following convention for the prefixes 
	is now used by the pickDataReader function:
	'disk:' - remove the prefix, use fstream library (via libfReadPlain.so plugin) 
	'rfio:' - remove the prefix, use shift library (via libfReadCastor.so plugin)
	'dcache:' - remove the prefix, use dcap library (via libfReaddCache.so plugin)
	'dcap:' - do not remove the prefix, use dcap library (via libfReaddCache.so plugin)
	none - try all three plugins in the following order: Plain, Castor, dCache.
	
	- DataReader now own fRead and will delete it.

	* Tag v1r10p1.

2006-06-22  Szymon <szymon.gadomski@cern.ch>

	* File size limit in MB was applied in such a way that
	the file was closed after the limit was exceeded.
	It will now be closed before.
	
	* Tag v1r10p0.

2006-06-13  Szymon Gadomski  <szymon@pcatd16.cern.ch>

	* Remove /afs paths from compile options of a dCache 
	test program.

2006-06-12  Szymon Gadomski  <szymon@pcatd16.cern.ch>

	* Added a method to detect if the directory change
	has taken place. 

2006-06-09  Szymon Gadomski  <gadomski@lxplus015.cern.ch>

	* Added possiblity to change directory while writing
	a sequence of files with DataWriter. Added a test program
	to test this. (Development done 2006-06-07, merged in 
	now)

	* Catch a file name parsing problem reported by Immma.
	It was going wrong when not only number was present
	between _file and .data.

2006-06-08  Szymon Gadomski  <gadomski@lxplus057.cern.ch>

	* Compilation by using simply the TDAQCExternal
	is now made possible. Corresponding change is made
	in the requirements file.

	* This version will be used to patch the tdaq-common
	in order to make the dCache plugin possible in release 
	12 of the offline. Tag v1r9p0.

2006-06-07  Szymon Gadomski  <gadomski@lxplus057.cern.ch>

	* Added control of the dynamic loading of the plugin
	by file name prefixes like in ROOT "rfio:" "dcap:" and
	"disk:". No prefix gives the old behaviour, i.e. all 
	three plugins will be tried.

2006-06-06  Szymon Gadomski  <gadomski@lxplus067.cern.ch>

	* Added reader plugin for dCache. Linking is not
	proper yet. No tag.

	* Fix a bug in the code that checks if a file exists.
	There was an attempt to close a file also when open 
	did not succeed. This crashed in the dCache plugin.
	Now fixed in all the three plugins. 

2006-03-20  Szymon Gadomski 

	* Add a check of the DataWriter status to the
	testWritingSpeed program. Now it is like the
	SFO. Tested full disk condition by writing 
	to a floppy disk. 
	Tag v1r8p2.

2006-03-13  Szymon Gadomski <szymon.gadomski@cern.ch>

	*  Fix a bug related to getting trailing metadata 
	from the current file in DataReaderFormat2.cxx.
	* More diagnostic in the test programs.
	* Tag v1r8p1.


2006-03-08  Szymon Gadomski  <szymon.gadomski@cern.ch>

	* On request of Offline, expressed by Hong Ma,
	the pickDataReader with many parameters is put
	back and implemented.

	* Additional metadata for commissioning. Vector
	of strings optionally present in file format 2.

	* Tag v1r8p0.

2006-03-07  Szymon Gadomski <szymon.gadomski@cern.ch>

	* Remove pickDataReader with many parameters from
	the header file. The implementation does not exist
	any longer. Tag v1r7p1.

2006-02-23  Szymon Gadomski <szymon.gadomski@cern.ch>

	* Complete refactoring of the package. Add support 
	for many file acces libraries in a more systematic 
	way and using dynamic loading. This is to support 
	dCache in the future.

	* Tag v1r7p0.

2005-10-31  Szymon Gadomski  <szymon.gadomski@cern.ch>

	* Removed some redundant semicolons that were causing
	gcc 3.4.4 to complain. Compilation checked with
	i686-slc3-gcc323-opt and i686-slc3-gcc344-dbg.
	Tag v1r6p6.

2005-07-07  Szymon Gadomski  <gadomski@lxplus051.cern.ch>

	* DataWriter uses rename function instead of a system call.
	tag v1r6p5.

2005-06-14  Szymon Gadomski  <gadomski@lxplus009.cern.ch>

	* Added some diagnostics methods to DataWriter.
	
	* tag v1r6p4.

2005-04-29  Szymon Gadomski  <gadomski@lxplus069.cern.ch>

	* Added release notes for tdaq-01-02-00.

2005-04-22  Szymon Gadomski  <gadomski@lxplus065.cern.ch>

	* Added test program to measure the speed of reading.
	
	* Improved test program to measure the speed of writing.

	* Fix bug related to waiting for more data while reading.

	* Tag v1r6p2.

2005-02-17  szymon  <szymon@pcatd16.cern.ch>

	* Add Hans Peter to managers.

	* Tag  v1r6p1.

2005-02-11  szymon  <szymon@pcatd16.cern.ch>

	* Added release notes for tdaq-01-01-00.

2005-01-21  szymon  <szymon@pcatd16.cern.ch>

	* Allow arbitrary file names at reading time.
	
	* If a file name contains _file<Number>.data it
	is assumed that we may have a sequence and the data
	reader will try to go transparently to following
	files ([...]_file<Number+1>.data[...])

	Number of digits in the file number (if any)
	is deduced from the first file and should not
	change, except when the file number overflows.
	Some valid sequences for instance:
	1,2,...,9,10,11,..
	001,002,...,999,1000,1001,...

	Not valid sequences:
	1,02
	001,2

	* The above concerns offline software simulated
	byte stream data, where file sequences are made
	by renaming files.
	
	Files sequences written by the library will continue 
	with the name convention used up to now:
	daq_<Application>_<Tag>_<RunNumber>_file<FileNumber>.data

	* Tag v1r6p0.
	
2004-06-29  Szymon Gadomski  <gadomski@lxplus022.cern.ch>

	* Changes of interface as agreed with Giovanna.
	Passing run number and the number of events to 
	the methods called at file open and close.
	Tag v1r5p0.

2004-06-08  szymon  <szymon@pcatd16.cern.ch>

	* Call back interface added (DataWriterCallBack).
	Example implementation added (DWCBcout).

2004-05-31  Szymon Gadomski  <gadomski@lxplus073.cern.ch>

	* Fix a missing initialization reported by Hong.
	Tag v1r4p1.

2004-04-14  Szymon GADOMSKI  <gadomski@pcatd16.cern.ch>

	* Additions as agreed with Hong Ma and Robert 
	McPherson. A monitoring job running in Athena
	will be able to wait for more data as the data 
	is being written.

	* DataWriter now opens a new file with .writing
	extension as soon as the previous file is closed. 
	(It used to wait until such a file was needed).

	* DataReader returns a special error code when
	reading data to enable waiting. This is done 
	under three conditions:
	- we have reached an end of a file, 
	- end of run is not marked there, 
	- the next file is already open for writing
	  (it exists with .writing extension).

	* Otherwise the behavior is the same as before.
	When there is no .writing file the DataReader
	considers that this is the end of the file sequence. 
	It does that even if the end of file record does 
	not indicate that this is the end of run. One can 
	still normally analyze a part of a file sequence.

	* The test program test/readData.cxx demonstrates
	how one can wait for more data. A catch-up race 
	between writing and reading can be tested with 
	this program catching up to another test program
	testWritingSpeed.cxx. The latter can now be slowed 
	down to play the game.

	* Tag v1r4p0.

2004-02-09  Szymon GADOMSKI  <gadomski@pcatd16.cern.ch>

	* Access to meta data via DataReader interface.
	Lots of related methods. The data structures
	as written in files are not returned to the user
	directly. 

	* Rewinds forward when necessary to get info
	from file and or even from run end. Should be 
	used with care, particularly for run end. 
	Reading is not disturbed by this rewinding.

	* DataReaderCastor2003 can now be constructed with 
	file number. Reading data can start with any file
	in sequence, no longer only with the 1st.

	* Tag v1r3p4.

2004-02-06  Szymon GADOMSKI  <gadomski@pcatd16.cern.ch>

	* A constructor with one parameter when reading.
	Make pickDataReader(fileName). Add this to readData.

	* Make daq_ automatically a part of file name 
	when writing data. Applications should not add 
	this anymore.

	* Avoid a crash, present with gcc295, when files
	in new format were open with DataReaderCastor2003. 

	* Tag v1r3p3.

2004-02-03  Szymon GADOMSKI  <gadomski@pcatd16.cern.ch>

	* Improved diagnostics in case a file is missing.
	Tag v1r3p2.

2004-02-02  Szymon GADOMSKI  <gadomski@pcatd16.cern.ch>

	* Added a way to read last year's data files, 
	(DataReaderCastor2003).

	* Added a function to select the correct data 
	reader automatically. 	

2004-01-21  Szymon GADOMSKI  <gadomski@pcatd16.cern.ch>

	* Changes of includes for fix compilation errors with gcc295.
	Tag v1r3p1.

2004-01-20  Szymon GADOMSKI  <gadomski@pcatd16.cern.ch>

	* Tag v1r3p0.

2004-01-19  Szymon GADOMSKI  <gadomski@pcatd16.cern.ch>

	* Change file format to align it with the latest 
	written up proposition.

2004-01-08  Szymon GADOMSKI  <gadomski@pcatd16.cern.ch>

	* The major change of file format put in CVS after
	a few days of development and testing.
	
2003-12-19  Szymon GADOMSKI  <gadomski@pcatd16.cern.ch>

	* Fix requirements file to compile some test programs.
	Tag v1r2p4.

2003-09-25  Szymon Gadomski  <gadomski@lxplus015.cern.ch>

	* shlib added. Tag v1r2p2.

2003-09-22 Christian Haeberli
	* DCPolicy -> DataflowPolicy


2003-07-18  Szymon GADOMSKI  <gadomski@pcatd16.cern.ch>

	* On request from beam test detector people reluctantly
	added 4-byte alignment. The event data now starts at the 
	four byte boundaries thanks to padding in the event_record.
	Both readers modified to read file aligned as well as not 
	aligned, the user does not need to know. Tag v1r2p0.

2003-07-17  Szymon GADOMSKI  <gadomski@pcatd16.cern.ch>

	* Bug fix. Tag v1r1p4.

2003-07-15  Szymon GADOMSKI  <gadomski@pcatd16.cern.ch>

	* Make event_record accessible, as requested by 
	Hong Ma. Tag v1r1p3.

	* Change to <sstream>, avoiding backward includes, 
	Avoid warning about conversion of const char *.
	Tested with both compilers. Tag v1r1p2.
	
2003-06-24  Szymon GADOMSKI  <gadomski@pcatd16.cern.ch>

	* The test program called readData can now dump raw 
	data into a file. For HLT testing of byte stream.
	Tag v1r1p1.

2003-06-23  Szymon Gadomski  <gadomski@lxplus084.cern.ch>

	* Bringing DataReaderCastor into this package. 
	Two kinds of DataReader now inherit from the same 
	parent DataReaderBase. DataReader is only an interface.
	Tag v1r1p0.

2003-05-02  Szymon Gadomski  <gadomski@pcatb120.cern.ch>

	* One of the previously removed includes in testWritingC
	(<errno.h>) was needed to compile with gcc295. Put it back
	in, tag v1r0p16.

2003-04-30  Szymon Gadomski  <gadomski@lxplus065.cern.ch>

	* Added another test program, named testWritingC,
	to measure speed of writing with low-level C function
	write(...). Tag v1r0p14. Fixed to compile with gcc32,
	tag v1r0p15.

2003-04-25  Szymon Gadomski  <gadomski@pcatb120.cern.ch>

	* Added test program to measure the speed of writing
	named testWritingSpeed Tag v1r0p11.

2003-02-06  Szymon Gadomski  <gadomski@pcatb140.cern.ch>

	* Better diagnostics when writing is not possible.
	Tag v1r0p8.

2003-02-06  Szymon Gadomski  <gadomski@pcatb141.cern.ch>

	* Tag as v1r0p7.

2003-02-03  Szymon Gadomski  <gadomski@pcatb141.cern.ch>

	* File name format as agreed with Beniamino.

2003-01-31  Szymon Gadomski  <s.gadomski@cern.ch>

	* Added an example program to read data back. Tag v1r0p6.

2002-07-19  Szymon GADOMSKI  <s.gadomski@cern.ch>

	* Avoid one local dynamic array. Not in the C++ standard, 
	according to Reiner. Also added some tests and some comments 
	to test programs, added README about the test programs. 
	Tag as v1r0p3.

2002-06-28  Szymon GADOMSKI  <s.gadomski@cern.ch>

	* Both DataWriter and DataReader now working. Tag as v1r0p2

2002-06-25  Szymon GADOMSKI  <s.gadomski@cern.ch>

	* First working prototype of the DataWriter class. Safety
	mechanisms about overwriting data will be in the SFO, as discussed
	with Hanspeter and Christian. The DataWriter nevertheless saves
	data away if the user tries to overwrite the same files. 

	
	

