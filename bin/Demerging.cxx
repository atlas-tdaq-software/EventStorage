#include <stdlib.h>
#include <iostream>
#include <string>
#include <iomanip>
#include <sstream>
#include <stdint.h>
#include <sys/uio.h>
#include <fstream>

#include "../src/MergedRawFile.h"

int main (int argc, char* argv[])
{
  bool onlyls = false;
	
  if (argc != 2 && argc != 3)
    {
      std::cerr << "Usage:" << std::endl;
      std::cerr << "  testDeMerge <filename_of_merged_file> [onlylist]" << std::endl;
	
      exit(10);
    }

  if ( argc == 3 )
    {
      std::string arg2(argv[2]);
      if (arg2 == "onlylist")
	onlyls = true;
    }

  uint64_t buffersize = 100 * 1024 * 1024;
	
  std::ifstream readfile;
  readfile.open (argv[1], std::ios::binary | std::ios::in);
	
	
  int wordsize = 4 ;
	
  uint32_t marker;
  uint32_t something;
  uint32_t headersize;
	
	
  readfile.read ((char*)&marker, wordsize);
  readfile.read ((char*)&something, wordsize);
  readfile.read ((char*)&headersize, wordsize);
	
  if ( marker!=MergedRawFile::FileMarker)
    {
      std::cerr << "ERROR: Merged File Marker not found. Aborting." << std::endl;
      exit(1);
    }

  if (!onlyls) std::cout << "DeMerging " <<  argv[1] << std::endl;
  else std::cout << "Content of " <<  argv[1] << ": " << std::endl;
	
  readfile.seekg(0);
  uint32_t *head = new uint32_t[headersize];
	
  readfile.read ((char*)head, headersize*wordsize);
	
  MergedRawFile *header = new MergedRawFile(false);
  header->unpackHeader(head);
	
  std::vector < uint64_t > siz = header->filesizes();
  std::vector < uint64_t > off = header->fileoffsets();
  std::vector < std::string > nam = header->filenames();
	
  for (unsigned int f = 0 ; f < siz.size(); f++)
    {
		
      if (!onlyls) std::cout << " Unpacking encosed file " <<  nam.at(f) << std::endl;
      else std::cout << " - " <<  nam.at(f) << std::endl;
      uint64_t rem = siz.at(f);
      readfile.seekg(off.at(f));
		
      std::ofstream outF2;
		
      if (!onlyls) 
	{
	  outF2.open(nam.at(f).c_str(), std::ios::out | std::ios::binary);
	
	  char *data = new char[buffersize];
	
		
	  while (rem >0)
	    {
	      uint64_t bytes = std::min (rem, buffersize);
	      readfile.read (data, bytes);
	      rem -= bytes;
	      outF2.write (data, bytes);
		
	    }
	  outF2.close ();
	}
	
    }
	
  readfile.close ();
}

