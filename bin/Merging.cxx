/**
   \file testMergedFiles.cxx
   \author Kostas Kordas
   \brief Write a MergedFile, starting from info to construct the Header and a file as a playload.
*/

#include <iostream>
#include <ios>
#include <fstream>
#include <string>
#include <iomanip>

#include "../src/MergedRawFile.h"
#include "../src/Guid.h"
#include "EventStorage/RawFileName.h"

void usage()
{

  std::cout << std::endl << ">  Usage: " << std::endl <<
    ">  testMergedFiles <list_of_files> <MBlimitperfile> [<outputnamecore>]  " <<  std::endl <<
    ">    list_of_files: " << "newline separated list of files in current directory to merge" << std::endl <<
    ">    MBlimitperfile: " << "file size limit for merged files. Will be split if above."<< std::endl <<
    ">    outputnamecore: " << "target file name core (without .xxxx.data)" << std::endl << std::endl << "if <outputnamecore> is not given, you will be asked for it " << std::endl; 

}

bool file_exists (std::string filename) {
  std::ifstream fin;
  // We must use the string class's c_str() function to convert
  // The C++ string into an old C string (character array).
  // This is because the open() function expects a character array
  fin.open (filename.c_str());
  if (fin.fail()) return false;
  fin.close();
  return true;
}



int main (int argc, char* argv[])
{
  if (argc!=3 && argc!=4)
    {
      usage();
      exit (1);
    }
	
  std::string fin(argv[1]);
  if (!file_exists(fin))
    {
      std::cout << "List file doesnt exist: " << fin << std::endl; 
      exit (2);
    }
	
  std::cout << "Merging the following files:" << std::endl;

  std::vector<std::string> fns;

  std::ifstream txtfile(argv[1]);
  std::string str;
  while( getline( txtfile, str ) )
    {
      std::cout << "file " << str << std::endl;
      fns.push_back(str);
    }

  std::string target;

	
  if (argc==3)
    {
      //no target name was given
      std::cout << "Do you want to enter whole target file name Core? (y/n): ";
      std::string ii;
      std::cin >> ii;
		
      if (ii=="y" || ii=="")
	{
	  std::cout << "Enter file name core: ";
	  std::string nC;
	  std::cin >> nC;
		
	  target = nC;
	  daq::RawFileName tt(target);		

	  if (tt.hasValidCore())
	    {
	      std::cout << "Valid Target File Name Core" << std::endl;
	    }
	  else
	    {
	      std::cout << "WARNING: Target File Name INVALID. Using it anyway." << std::endl;
	    }
			
			
	}
      else
	{
	  std::cout << "Okay, so let's enter each part by itself! "<< std::endl;

	  std::string ProjectTag;			
	  unsigned int  RunNumber;
	  std::string StreamType;
	  std::string StreamName;
	  unsigned int LumiBlockNumber;
	  std::string ApplicationName;
	  std::string ProductionStep;
	  std::string DataType;
			
	  std::cout << "Enter ProjectTag: ";
	  std::cin >> ProjectTag;
	  std::cout << "Enter RunNumber: ";
	  std::cin >> RunNumber;
	  std::cout << "Enter StreamType: ";
	  std::cin >> StreamType;
	  std::cout << "Enter StreamName: ";
	  std::cin >> StreamName;
	  std::cout << "Enter LumiBlockNumber: ";
	  std::cin >> LumiBlockNumber;
	  std::cout << "Enter ApplicationName: ";
	  std::cin >> ApplicationName;
	  std::cout << "Enter ProductionStep: ";
	  std::cin >> ProductionStep;
	  std::cout << "Enter DataType: ";
	  std::cin >> DataType;
			
	  daq::RawFileName tt(ProjectTag, RunNumber, StreamType, StreamName, 
			      LumiBlockNumber, ApplicationName, 
			      ProductionStep, DataType);		
			
	  target = tt.fileNameCore();
	}
		
	
    }
  else
    {
      target=argv[3];
      daq::RawFileName tt(target);
      if (tt.hasValidCore())
	{
	  std::cout << "Target File Name can be interpreted (you may, or may not care!)" << std::endl;
	}
      else
	{
	  std::cout << "Target File Name can not be interpreted (you may, or may not care!)" << std::endl;
	}
    }
	
	
	

  std::string ms(argv[2]);
  uint64_t maxMB;

  std::istringstream i(ms);
  i >> maxMB;

  std::cout << "max size is " << maxMB << std::endl;
  std::string blafasel = target;
  uint64_t idi = (uint64_t)(maxMB*1024*1024);
  MergedRawFile depp(fns, blafasel, idi);

  std::vector< std::vector<uLong> > checksumOrFile = depp.getOFChecksumsAll();
	
	
  std::vector<uLong> checksumMeFile = depp.getMFChecksums();
  std::vector<std::string> oufNames = depp.getOutFileNames();
  std::vector<std::string> guids = depp.getGUIDS();

  std::vector< std::vector<uint64_t> > offs = depp.getMFOffsets();
  std::vector< std::vector<uint64_t> > sizes = depp.getOFSizesAll();


  std::vector<uint32_t> numevents = depp.numcontained_events_in_merge_files();

  std::ofstream outf;



  outf.open ("results.txt");



  std::cout << "Number of Merged Files: " << checksumMeFile.size() << std::endl;
  outf << checksumMeFile.size() << std::endl;


	
  for (unsigned int i = 0; i< checksumMeFile.size(); i++ )
    {
      outf << oufNames.at(i) << ":" << checksumMeFile.at(i) << ":" << offs.at(i).size() << ":" << guids.at(i) << ":" <<  numevents.at(i) <<std::endl;
      for ( unsigned int b = 0; b < offs.at(i).size(); b++ )
	{
	  //std::cout << offs.at(a).at(b) << " (" << sizes.at(a).at(b) << ")" <<", ";
				
	  outf << "" << checksumOrFile.at(i).at(b) << ":" << offs.at(i).at(b) << ":" << sizes.at(i).at(b) << std::endl;
	}
				 
      std::cout << "Merged File Nr. " << i << " checksum is " << checksumMeFile.at(i) << std::endl;
    }


  outf.close();
	
	
  return 0;

}
