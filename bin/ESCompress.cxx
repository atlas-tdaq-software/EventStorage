
#include <iostream>
#include <string>
#include <boost/shared_ptr.hpp>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

#include "EventStorage/DataWriter.h"
#include "EventStorage/RawFileName.h"
#include "EventStorage/DWCBcout.h"
#include "EventStorage/pickDataReader.h" 
#include "EventStorage/DataWriterCallBack.h"

#include <stdint.h>
#include <unistd.h>


namespace po = boost::program_options;


class FileCallBack: public EventStorage::DataWriterCallBack{

public:
  FileCallBack(){};
  ~FileCallBack(){};
  
  void FileWasOpened(std::string logicalfileName,
		     std::string fileName,
		     std::string streamtype,
		     std::string streamname,
		     std::string sfoid,
		     std::string guid,
		     unsigned int runNumber, 
		     unsigned int filenumber,
		     unsigned int lumiblock){};
  
  void FileWasClosed(std::string logicalfileName,
		     std::string fileName,
		     std::string streamtype,
		     std::string streamname,
		     std::string sfoid,
		     std::string guid,
		     std::string checksum,
		     unsigned int eventsInFile, 
		     unsigned int runNumber, 
		     unsigned int filenumber,
		     unsigned int lumiblock,
		     uint64_t filesize){

    std::cout << "Checksum: " << checksum << std::endl;
  }; 

};




int main(int argc, char **argv)
{

  po::options_description desc("Allowed options");
  desc.add_options()
    ("help,h", "help message")
    ("no-compression,n", "non-compressed output")
    ("input-file,i",po::value<std::string>(), "input file")
    ("output-file,o",po::value<std::string>(), "output file")
    ;

  po::positional_options_description p;
  p.add("input-file",1);
  p.add("output-file",1);

  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).
	    options(desc).positional(p).run(), vm);
  po::notify(vm);


  if(vm.count("help")){
    std::cout << desc << "\n";
    return 1;
  }
  
  EventStorage::CompressionType compression = EventStorage::ZLIB;
  if(vm.count("no-compression")){
    compression = EventStorage::NONE;
  }

  std::string infile;
  std::string outfile;

  if(vm.count("input-file") && vm.count("output-file")) {
    infile = vm["input-file"].as<std::string>();
    outfile = vm["output-file"].as<std::string>();
  } else {
    std::cout << desc << "\n";
    return 1;
  }

  DataReader *pDR = pickDataReader(infile);

  if(!pDR) {
    std::cerr << "Problem opening or reading this " << infile;
    return -1;
  }
  
  std::vector<std::string> fmds =  pDR->freeMetaDataStrings();
   
  std::vector<std::string>::iterator it = fmds.begin();
  
  for(; it != fmds.end();++it){
    if((*it).find("Compression=") == 0){
      fmds.erase(it);
      break;
    }
  }
    


  EventStorage::run_parameters_record rPar;
        
  rPar.run_number = pDR->runNumber();     
  rPar.max_events = pDR->fileSizeLimitInDataBlocks();     
  rPar.rec_enable = 1;   
  rPar.trigger_type = pDR->triggerType();
  rPar.detector_mask_LS = (pDR->detectorMask() & std::bitset<128>(0xFFFFFFFFFFFFFFFF)).to_ullong();
  rPar.detector_mask_MS = (pDR->detectorMask() >> 64).to_ullong();
  rPar.beam_type = pDR->beamType();
  rPar.beam_energy = pDR->beamEnergy();

  
  boost::filesystem::path out(outfile);
  out = boost::filesystem::system_complete(out);
  std::string stream = pDR->stream();

  std::string streamtype = stream.substr(0,stream.find_first_of('_'));
  std::string streamname = stream.substr(stream.find_first_of('_')+1);

  EventStorage::DataWriter DW(out.parent_path().string(),
			      out.filename().string(),     
			      rPar, 
			      pDR->projectTag(),
			      streamtype,
			      streamname,
			      stream,
			      pDR->lumiblockNumber(),
			      pDR->appName(),
			      fmds,
			      compression,
			      1);


  DW.setMaxFileNE(pDR->fileSizeLimitInDataBlocks());
  DW.setMaxFileMB(pDR->fileSizeLimitInMB());
  
  FileCallBack *  fb = new FileCallBack();
  DW.registerCallBack(fb);

  if (!DW.good()) {
    std::cerr << "Problem opening file " << outfile << std::endl;
    return -1;
  }

  while(pDR->good()){
    
    uint32_t eventSize;
    char * buf;
    
    pDR->getData(eventSize,&buf);
    
    DW.putData(eventSize,buf);
    
    if(!DW.good()) {
      std::cerr << "Can't write anymore!" << std::endl;
      return -1;
    }

    delete[] buf;
  }
  
  DW.closeFile();
  delete pDR;

  return 0;
}
