#!/bin/sh

echo "---- de- and reMerging Script -----"

if [ $# -ne 4 ]
then
	echo "expecting 4 arguments:"
	echo "addFile2MergedFile <mergefile> <additionaldirfile> <ouptutname> <limit>"
	echo "<mergefile> is the mergefiles to extend"
	echo "<additionaldirfile> is text file with newline separated"
	echo " list of files to merge into the <mergefile>"
	echo "<ouptutname> is the filenamecore of the new merged file."
	echo "<limit> is the file size limit of the new merged files. 0 if no limit."
	echo " --------WARNING---------"
	echo "  Be aware that this scripts can run a long time if huge files"
	echo "  are de- and reMerged"
	exit 4
fi

testDeMerge $1 > outpfiles
cat outpfiles | sed 's/ Unpacking encosed file //g' | grep -v DeMerging > newfiledir.txt
cat $2 >> newfiledir.txt
echo "Merging"
cat newfiledir.txt

testMergedFiles newfiledir.txt $4 $3
rm newfiledir.txt
rm outpfiles

