#!/usr/bin/env python

#########################################################################
##
## Tier-0 Raw -> Merged Raw Wrapper
##
##  - input parameter: file containing a pickled dictionary consisting of the key/value pairs
##     1) 'inputRAWFiles': python list of file dictionaries 
##        [{'lfn':'filename1', 'checksum':'checksum1', 'dsn':'datasetname1', ...}, 
##         {'lfn':'filename2', 'checksum':'checksum2', 'dsn':'datasetname2', ...}, ...]
##        (file checksums may be in hex, like '0x14f5a784', or string, like 'ad:14f5a784', formats)
##     2) 'outputMergedRAWFile': string 'filename' or 'datasetname#filename' 
##        (merged RAW output file) 
##     3) 'maxsizeinMB': integer 'maxSize' "
##        (optional: if merged files must not exceed a certain size, set this) "
##
##  - generates merged RAW files running file_merging in EventStorage library
##
##  (C) C. Topfel (cyril.topfel@cern.ch), K. Kordas (kostas.kordas@cern.ch),
##      A. Nairz (armin.nairz@cern.ch)
##      
#########################################################################

import sys, string, commands, os.path, os, pickle, time, pprint
from zlib import adler32


#########################################################################
# Checksum calculation function

def checkthefile(logfile, file, offsets, sizes, msum, osums, gsums):
  # logfile is file object to write to
  # file is name of merged file
  # offsets is array of offsets of original files
  # sizes is arra yof sizes of original files
  # msum is checksumof merged file
  # osums is array of checksums of original files
  # gsums is array of checksums of original files given as gpickle files from database
  
  sum = 0
  for a in range(len(sizes)):
    sum += sizes[a]
    
  mfs = offsets[0] + sum
  
  remaining = mfs
  curfile = -1
  curpos = 0
  maxbuff = 10*1024*1024

  tmpsum = 1L
  mfcheck = 1L
  checksums = []

  r = open(file, "rb")
  
  while remaining > 0:
    if curfile+1 >= len(offsets):
      nextoffset = mfs
    else:
      nextoffset = offsets[curfile+1]
        
    rb = min( nextoffset - curpos , maxbuff)
    
    data = r.read(rb)
    mfcheck = adler32(data, mfcheck)
    
    if curfile != -1:
      tmpsum = adler32(data,tmpsum)
    
    curpos = curpos + rb
  
    if nextoffset != mfs:
      if curpos == offsets[curfile+1]:
        curfile = curfile + 1
        if tmpsum < 0:
          tmpsum += pow(2,32)
        if curfile != 0:
          checksums.append(tmpsum)
          tmpsum = 1L
    
    remaining = remaining - rb

  if tmpsum < 0:
    tmpsum += pow(2,32)
  
  checksums.append(tmpsum)
  r.close()  
  
  if mfcheck < 0:
    mfcheck += pow(2,32)
  
  for a in range(len(osums)):
    logfile.write( "O-chcks: Merger " + str(osums[a]) + ", checkscript " + str(checksums[a]) + ", Pickle " + str(gsums[a]) + "\n");
  
  logfile.write("M-chcks: Merger " + str(msum) + ", checkscript " + str(mfcheck));
  if msum == mfcheck  and osums == checksums and gsums == osums:
    logfile.write(" --> OK\n")
  else:
    logfile.write(" --> FAILED\n")
  return msum == mfcheck  and osums == checksums

#########################################################################


#########################################################################
# Utility function

def getFileMap(fname, checksum, guid, dsname='', nevts=0, endtime=None) :
  if os.path.isfile(fname) :
    fmap = { 'lfn': fname,
             'dataset': dsname,
             'GUID' : guid,
             'size' : os.path.getsize(fname),
             'events' : nevts,
             'checkSum' : "ad:" + string.zfill((str(hex(checksum))[2:].replace("L","")).lower(), 8),
             'endtime_epoch' : endtime
          }
  else : 
    fmap = {}
  return fmap

#########################################################################

        
#########################################################################

def mergeRAWFiles(picklefile) :

  tstart = time.time()
  
  print " "
  print " "
  print "##################################################################"
  print "##                  ATLAS Tier-0 RAW FileMerging                ##"
  print "##################################################################"
  print " "

  # extract parameters from pickle file
  print "Using pickled file ", picklefile, " for input parameters"
  f = open(picklefile, 'r')
  parmap = pickle.load(f)
  f.close()

  print "\nFull Tier-0 run options "
  pprint.pprint(parmap)
  
  inputfilelist = parmap.get('inputRAWFiles', [])
  nfiles = len(inputfilelist) 
  
  if not nfiles :   # problem with job definition or reading picklefile
    dt = 0
    retcode = 2
    acronym = 'TRF_NOINPUT'
    txt = 'empty input file list'  
    outmap = { 'prodsys': { 'trfCode': retcode,
                            'trfAcronym': acronym,  
                            'jobOutputs': [],
                            'jobInputs': [],
                            'nevents': 0,              
                            'more': { 'num1': 0, 'num2': dt, 'txt1': txt }
                          }
             }              
    f = open('jobReport.gpickle', 'w')
    pickle.dump(outmap, f)
    f.close()
  
  else :   # proceed in file merging
  
    nicelog = open("checksums.txt", 'w')
    
    dirfile = "dir.txt"
    print "Writing Input File Names to temporary file " + dirfile + ", "
    print "extracting original checksums and determining maximum file end-time"
    dirf = open(dirfile, 'w')
    checksumsorig = []
    maxendtime = 0
    for fdict in parmap['inputRAWFiles'] :
      fname = fdict['lfn']
      dirf.write(fname + "\n")
      cksum = fdict['checksum']
      checksumsorig.append(cksum.replace('ad:','0x'))
      if fdict.has_key('endtime_epoch') :
        if fdict['endtime_epoch'] > maxendtime : 
           maxendtime = fdict['endtime_epoch']
     
    dirf.close()
    
    # set maxendtime to current epoch, if not yet set
    if maxendtime == 0 : 
      c = int(time.time())
    
    print "Maximum file end-time: ", str(maxendtime)  
    print "Temporary file " + dirfile + " written (List of file names)";
    
    if parmap.has_key("maxsizeinMB") :
      maxsize =  parmap['maxsizeinMB']
    else:
      maxsize =  0
    
    outval = parmap['outputMergedRAWFile']
    if outval.find('#') > -1 :  # check for potential 'dataset#file' input
     outdsname = outval.split('#')[0]
     outfile = outval.split('#')[1]
    else :
     outdsname = ''
     outfile = outval

    
    commandstring = "file_merging " + dirfile + " " + str(maxsize) + " " + outfile + " >& log;"
    
    cmd  = "rm -f log ; "
    cmd += commandstring
    cmd += "grep -i error log"
    print cmd    
    
    t1 = time.time()
    
    # run command, try retries+1 times
    retries = 1
    ntry = 0
    retcode = 0
    while ntry < retries+1 :
      ntry += 1
      print "Trial nr.", ntry
      (retcode1, o1) = commands.getstatusoutput(cmd)
      txt1 = o1[-1000:]
      if retcode1 == 256 : #no errors found in logfile
        print "## Now calculating all checksums"
        
        ####################
        f = open("results.txt", "r")
        
        num = int(f.readline())
        
        all = []
        mergedfilesguid = []
        checkokay = True
        poscheck = 0; 
        for i in range(num):
          ar = []
          l = f.readline()
          p = l.split(":")
          filename = p[0]
          checksum = long(p[1])
          numint = long(p[2])
          guid = p[3];
          nume = long(p[4].rstrip("\n"))
        
          checksums = []
          offsets = []
          sizes = []
          ocks = []
        
          for k in range (numint):
            indfl = f.readline()
            cos = indfl.split(":")
            ifchk = long(cos[0])  
            checksums.append(ifchk)
            ifoff = long(cos[1])
            offsets.append(ifoff)
            ifsiz = long(cos[2])
            sizes.append(ifsiz)
            if long(checksumsorig[poscheck],16) != ifchk:
              checkokay = False
              print "Error calculating checksum" 
            else:
              ocks.append(  long(checksumsorig[poscheck],16)  )
            poscheck+=1
            
          ar.append(filename)
          ar.append(checksum)
          ar.append(checksums)
          ar.append(offsets)
          ar.append(sizes)
          ar.append(guid)
          ar.append(ocks)
          ar.append(nume)
          all.append(ar)
        
        okay = True  
        for a in all:
          if not checkthefile(nicelog, a[0], a[3], a[4], a[1], a[2], a[6]):
            okay = False
        ####################
        if okay and checkokay:
          retcode = 0
          acronym = 'ALLOK'
          txt = 'trf finished OK'  
          break
        else:
          retcode = 8
          acronym = 'TRF_CHKSUMCALC_EXE'
          txt = 'Checksum calculation error! Merging failed \n'
      elif retcode1 == 0 : #errors found in logfile
        retcode = 1
        acronym = 'TRF_MERGE_EXE'
        txt = 'MERGING FILES FAILED \n' + txt1
    
    dt = int(time.time() - t1)
     
    nicelog.close() 
     
    print " "
    print "## ... RAW merge job finished with retcode = %s" % retcode
    print "## ... elapsed time:    ", dt, "sec"
    
    # print logfile fragments
    print "\n## ... logfile from RAW File merging: "
    print "--------------------------------------------------------------------------------"
    cmd = "cat log"
    (s,o) = commands.getstatusoutput(cmd)
    print o
    print "--------------------------------------------------------------------------------"
    print " "

    cmdcc = "echo checksums.txt; cat checksums.txt"
    (ccs,cco) = commands.getstatusoutput(cmdcc)

    print ccs
    print "--------------------------------------------------------------------------------"
    print cco
    print "--------------------------------------------------------------------------------"

    # assemble report pickle file
    infiles = []
    
    # get info for report gpickle file
    fullnumevs = 0L
    outfilesr = []
    if retcode == 0 :
      for a in all:
        fullnumevs += long(a[7])
        fname = a[0]
        if not maxsize :
          os.rename(a[0], outfile) 
          fname = outfile
        outfilesr.append(getFileMap(fname=fname, checksum=a[1], guid=a[5], dsname=outdsname, nevts=a[7], endtime=maxendtime))
          
    # assemble job report map, pickle it
    outmap = { 'prodsys' : { 'trfCode' : retcode,
                             'trfAcronym' : acronym,  
                             'jobOutputs': outfilesr,
                             'jobInputs' : infiles,
                             'nevents' : fullnumevs,
                             'endtime_epoch' : maxendtime,               
                             'more' : { 'num1' : fullnumevs, 'num2' : int(dt), 'txt1' : txt, 'txt2' : cco }
                           }
             }              
    f = open('jobReport.gpickle', 'w')
    pickle.dump(outmap, f)
    
    f.close()

  print " "
  print "##################################################################"
  print "## End of job."
  print "##################################################################"
  print " "

#########################################################################


########################################
## main()
########################################

if __name__ == "__main__":

  good = True

  if (len(sys.argv) != 2) : 
    good = False
  else:
    if (not sys.argv[1].startswith('--argdict=')) :
      good = False
  
  if not good :
    print "Input format wrong --- use "
    print "   --argdict=<pickled-dictionary containing input info> "
    print "   with key/value pairs: "
    print "     1) 'inputRAWFiles': python list of file dictionaries " 
    print "        [{'lfn':'filename1', 'checksum':'checksum1', 'dsn':'datasetname1', ...}, "
    print "         {'lfn':'filename2', 'checksum':'checksum2', 'dsn':'datasetname2', ...}, ...] "
    print "        (file checksums may be in hex, like '0x14f5a784', or string, like 'ad:14f5a784', format) "
    print "     2) 'outputMergedRAWFile': string 'filename' or 'datasetname#filename' " 
    print "        (merged RAWFiles output file) "
    print "     3) 'maxsizeinMB': integer 'maxSize' "
    print "        (optional: if merged files must not exceed a certain size, set this. "
    print "         Merged files will be split according to boundary) "

    sys.exit(-1)
  
  else :
    picklefile = sys.argv[1][len('--argdict='):]
    mergeRAWFiles(picklefile)

