#include <zlib.h>
#include <random>
#include "src/Adler32MT.h"

#include <iostream>
#include <chrono>
#include <vector>
#include <map>
#include <fstream>
#include <sstream>
#include <sys/stat.h>

static int const BUFFER_SIZE = 10 * 1024 * 1024;
static int const NS_PER_SEC = 1000000000;
static int const B_PER_MB = 1024 * 1024;
static int NB_MEASURES = 10000;

void generate_visualization_script(
		std::vector<int> const & datasizes,
		std::map<int, std::vector<double>> const & throughputs);

void compute_thresholds(
		std::vector<int> const & datasizes,
		std::map<int, std::vector<double>> const & throughputs);

int main(int argc, char** argv)
{
	if (argc > 1) {
		std::istringstream iss(argv[1]);
		iss >> NB_MEASURES;
		if (!iss) {
			std::cout << "usage: " << argv[0] << " [NbMeasures]" << std::endl;
			std::cout << std::endl;
			std::cout << "       NbMeasures: number of measures to do for each data point, default=1000" << std::endl;
			return -1;
		}
	}

	unsigned long checksum;

	std::cout << "Generating data..." << std::endl;
	std::random_device engine;
	// The threshold is supposed to be: 0 < threshold < 1 MB
	std::vector<unsigned char> data(BUFFER_SIZE);
	for (unsigned char & c: data) {
		c = engine() & 0xFF;
	}

	std::vector<int> datasizes;
	/* MB/s
	 * the key is the number of CPU
	 * 0 contains the reference
	 * (1 could contain a check of the MT implementation when configured to use only 1 thread)
	 */
	std::map<int, std::vector<double>> throughputs;

	/* Measure a reference throughput: adler32 computing / second
	 * Average N measures to "eliminate" environment disturbance
	 */
	std::cout << "Measuring reference... "; std::cout.flush();

	for (int datasize = 1024; datasize <= BUFFER_SIZE; datasize *= 2) {
		std::cout << datasize << ", "; std::cout.flush();

		int64_t total_time_ns = 0;
		int64_t total_computed_data = 0;

		checksum = ::adler32(0L, Z_NULL, 0);
		for (int i = 0; i < NB_MEASURES; ++i) {
			auto const startts = std::chrono::high_resolution_clock::now();
			checksum = ::adler32(checksum, data.data(), data.size());
			total_time_ns += (std::chrono::high_resolution_clock::now() - startts).count();
			total_computed_data += data.size();
		}

		datasizes.push_back(datasize);
		throughputs[0].push_back(static_cast<double>(total_computed_data) / B_PER_MB * NS_PER_SEC / total_time_ns);
	}
	std::cout << std::endl;

	/* Measure adler32mt performance, for every even number of cores and for a
	 * sample of threshold
	 */
	std::cout << "Number of CPU hardware cores: "
			<< std::thread::hardware_concurrency() << std::endl;

	for (unsigned cpus = 2; cpus <= std::thread::hardware_concurrency(); cpus += 2) {
		std::cout << "Measuring for " << cpus << " CPUs... "; std::cout.flush();

		for (int datasize = 1024; datasize <= BUFFER_SIZE; datasize *= 2) {
			std::cout << datasize << ", "; std::cout.flush();

			Adler32MT adler32mt(cpus, 0); // threshold=0: all request will be "multithreaded"
			int64_t total_time_ns = 0;
			int64_t total_computed_data = 0;

			checksum = ::adler32(0L, Z_NULL, 0);
			for (int i = 0; i < NB_MEASURES; ++i) {
				auto const startts = std::chrono::high_resolution_clock::now();
				checksum = adler32mt.adler32(checksum, data.data(), datasize);
				total_time_ns += (std::chrono::high_resolution_clock::now() - startts).count();
				total_computed_data += datasize;
			}

			throughputs[cpus].push_back(static_cast<double>(total_computed_data) / B_PER_MB * NS_PER_SEC / total_time_ns);
		}
		std::cout << std::endl;
	}

	generate_visualization_script(datasizes, throughputs);
	compute_thresholds(datasizes, throughputs);
	return 0;
}

void generate_visualization_script(
		std::vector<int> const & datasizes,
		std::map<int, std::vector<double>> const & throughputs)
{
	std::string const script_filename = "/tmp/adler32mt_threshold.py";
	std::ofstream outf(script_filename);
	outf << "#!/usr/bin/env python" << std::endl;
	outf << "import matplotlib.pyplot as pp" << std::endl;
	outf << std::endl;

	outf << "datasizes = [";
	for (auto dsz : datasizes) {
		outf << dsz << ", ";
	}
	outf << "]" << std::endl;

	for (auto const & kv: throughputs) {
		if (kv.first == 0)
			outf << "st = [";
		else
			outf << "mt" << kv.first << " = [";

		for (auto rate : kv.second) {
			outf << rate << ", ";
		}
		outf << "]" << std::endl;
	}
	outf << std::endl;

	outf << "pp.plot(datasizes, st, label='single thread')" << std::endl;
	outf << std::endl;

	for (auto const & kv: throughputs) {
		if (kv.first == 0) continue;

		/* For multithread, We don't plot throughput vs datasize, but througput
		 * vs size of the split chunk.
		 */
		outf << "pp.plot([x/" << kv.first << " for x in datasizes], mt"
				<< kv.first << ", label='"
				<< kv.first << " threads', linewidth=1)" << std::endl;
	}
	outf << std::endl;

	outf << "pp.xlim(" << datasizes.front() << ", " << datasizes.back() << ")" << std::endl;
	outf << "pp.gca().set_xscale('log')" << std::endl;
	outf << "pp.title('Adler32 computation throughput as a function of data size\\n'" << std::endl
			<< "\t\t'(resulting split chunk size for multithread)')" << std::endl;
	outf << "pp.ylabel('Computation Rate [MB/s]')" << std::endl;
	outf << "pp.xlabel('Data Size [KB]')" << std::endl;
	outf << "pp.legend(loc='upper left', bbox_to_anchor=(1.01, 1))" << std::endl;
	outf << "pp.grid(linestyle='dotted')" << std::endl;
	outf << "pp.xticks(datasizes, [str(x/1024) for x in datasizes])" << std::endl;
	outf << "pp.subplots_adjust(right=0.7)" << std::endl;
	outf << "pp.show()" << std::endl;

	outf.close();
	if (chmod(script_filename.c_str(), S_IRWXU|S_IRWXG|S_IROTH)) {
		std::cerr << "error: could not set permissions to scripts file: "
				<< script_filename << std::endl;
	}
	std::cout << "visualization script generated: " << script_filename << std::endl;
}

void compute_thresholds(
		std::vector<int> const & datasizes,
		std::map<int, std::vector<double>> const & throughputs)
{
	/* Assuming that throughput scales linearly around the reference rate
	 * (single thread used as reference), we compute the threshold as the
	 * interpolation between the two point just below and just above the
	 * reference, at the reference rate.
	 */

	auto const & reference_rates = throughputs.at(0);
	double reference_rate = std::accumulate(reference_rates.begin(), reference_rates.end(), 0);
	reference_rate /= reference_rates.size();

	for (auto const & kv: throughputs) {
		if (kv.first == 0) continue;

		std::cout << kv.first << " CPUs: ";

		int index_above = -1;
		for (int i = 0; static_cast<unsigned long>(i) < kv.second.size(); ++i) {
			if (kv.second[i] > reference_rate) {
				index_above = i;
				break;
			}
		}

		if (index_above == -1) {
			std::cout << "cannot compute threshold: no value above reference" << std::endl;
			continue;
		}

		if (index_above == 0) {
			std::cout << "cannot compute threshold: first value already above reference" << std::endl;
			continue;
		}

		// y = ax + b
		double const y1 = kv.second[index_above], y0 = kv.second[index_above - 1];
		double const x1 = static_cast<double>(datasizes[index_above]) / kv.first;
		double const x0 = static_cast<double>(datasizes[index_above - 1]) / kv.first;
		double const a = (y1 - y0) / (x1 - x0);
		double const b = y1 - (a * x1);
		double const threshold = (reference_rate - b) / a;

		std::cout << "threshold = " << threshold << " B" << std::endl;
	}
}
