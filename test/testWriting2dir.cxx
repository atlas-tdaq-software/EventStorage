#include <iostream>
#include <string>
#include <iomanip>

#include "EventStorage/DataWriter.h"
#include "EventStorage/EventStorageRecords.h"
#include "EventStorage/DWCBcout.h"
#include "clocks/Time.h"
#include "clocks/Clock.h"

#include <unistd.h>

int main ()
{
    using namespace EventStorage;

    std::cout << "Please name 1st directory to write: ";
    std::string writePath1;
    std::cin >> writePath1;

    std::cout << "Please name 2nd directory to write: ";
    std::string writePath2;
    std::cin >> writePath2;

    std::cout << "Close file immediately at directory change (y/n)? : ";
    std::string asw; bool immediateClose=false;
    std::cin >> asw; if(asw=="y") immediateClose=true;

    std::string apName="testWriteSpeed";
    
    run_parameters_record rPar;

    rPar.run_number = 100;     
    rPar.max_events = 0;     
    rPar.rec_enable = 1;     
    rPar.trigger_type = 0;   
    rPar.detector_mask_1of2 = 111;  
    rPar.detector_mask_2of2 = 0;
    rPar.beam_type = 211;      
    rPar.beam_energy = 220;    
 
    //std::cout << "rPar = \n" << std::flush;
    //std::string rps=string_record(&rPar,&run_parameters_pattern);
    //std::cout << rps << std::endl;

    EventStorage::freeMetaDataStrings fmds;
    fmds.push_back("Test metadata strings.");
    fmds.push_back("tag=value");
    fmds.push_back("key = 125");
    fmds.push_back("label = AnotherValue");
    std::cout << "freeMetaDataStrings with size " <<fmds.size() <<std::endl;

    std::cout << "Constructing the DataWriter with five params!\n" << std::flush;
    DataWriter *pdw = new DataWriter(writePath1,std::string("test2dir"),rPar,fmds);
    
    //cout << "Is DataWriter ready to write? " << dw.good() << endl;
    //cout << "Are we trying to overwrite? " << dw.alreadyExists() << endl;
    if (!pdw->good()) {
      std::cout << "Problem opening file in directory "<<writePath1<<"."<<std::endl;
      std::cout << "Is the directory there and writable for you?"<<std::endl;
      exit(1);
    }
    
    std::cout << "Event size in kB: ";
    unsigned int eventSize;
    std::cin >> eventSize; eventSize *= 1024;

    std::cout << "How many events in each file: ";
    unsigned int eventFile;
    std::cin >> eventFile; 

    std::cout << "Delay between events (float, in seconds): ";
    float evDelayF;
    std::cin >> evDelayF; 
    unsigned long evDelay = (unsigned long)(evDelayF*1000000);
    std::cout << "The delay is " << evDelay << " microseconds.\n";

    unsigned int *fill = new unsigned int[eventSize/4];
    for(unsigned int i=0; i<eventSize/4; i++) 
      { 
	fill[i]=0x1AAAAAA1;
	//std::cout << fill[i] << " ";
      }
    std::cout << std::endl;
    void *event = fill;
    
    std::cout << "Number of events: ";
    unsigned int nev;
    std::cin >> nev;
   
    //pdw->setMaxFileMB(2);
    pdw->setMaxFileNE(eventFile);

    std::cout << "How often to measure speed? Interval in seconds (<0 means no measurement):";   
    double tmeas=0.0;
    std::cin >> tmeas;
    if(tmeas<=0.0) { 
      tmeas = 0.0;
      std::cout << "No measurements of speed.\n";
    } else {
      std::cout << "Intermediate measuerement every " << tmeas << " seconds\n";
    }

    // call-back example
    std::cout << "Print file names? (y/n)" << std::endl;
    std::string ans="";
    std::cin >> ans;
    if(ans=="y") {
      std::cout << "Register call-back class to print file names." << std::endl;
      DWCBcout *pCB = new DWCBcout("testWritingSpeed");
      pdw->registerCallBack(pCB);
    } else {
      std::cout << "File names not printed." << std::endl;
    }

    RealTimeClock rt_clock;
    Time startTime=rt_clock.time();
    Time previousTime=startTime;
    unsigned int pev=0;

    // save start time
    std::ofstream tstartFile("StartTime.dat",std::ios::out);
    tstartFile << std::setprecision(12) << startTime.as_seconds() << std::endl;
    tstartFile.close();

    std::cout << "Starting the run now!\n";    

    for(unsigned int ev=0; ev<nev; ev++) 
    {
       if(evDelay) usleep(evDelay); 

       pdw->putData(eventSize,event);

       Time now = rt_clock.time();
       Time tdiff = now - previousTime;
       
       if(tmeas > 0 && tdiff.as_seconds() > tmeas) {
	 double tds = tdiff.as_seconds();
	 double mbps = (ev-pev)*eventSize/(1024*1024*tds);
	 double evps = (ev-pev)/tds;
	 std::cout << "Time " << (now-startTime).as_seconds();
	 std::cout << " interval " << tds << " s,";
	 std::cout << " event " << ev;
	 std::cout << " current rate " << mbps << " MB/s or " << evps << " Hz.\n"; 
	 previousTime = now; 
	 pev = ev;
       }

       if(ev==nev/2) {
	 pdw->cd(writePath2);
	 if(immediateClose) pdw->nextFile();
       }

       if(!pdw->good()) {
	 std::cout << "Can't write anymore! Stop after event "<<ev<<std::endl;
	 break;
       }
    }

    Time now=rt_clock.time();
    Time tdiff = now - startTime;
    double tds = tdiff.as_seconds();
    std::cout << "Time of this run in seconds is " << tds << std::endl;
    double mb = (eventSize/1024)*nev; mb /= 1024;
    std::cout << "Total data written is "<< mb << " MB or " << nev << " events.\n";
    std::cout << "Average rate is "<< mb/tds << " MB/s";
    double evhz=nev; evhz /= tds;
    std::cout << " or "<<evhz<<" Hz."<<std::endl;

    pdw->cd(writePath1);
    pdw->closeFile();

    pdw->cd(writePath2);
    pdw->closeFile();

    delete pdw;
}
