

#include <string>
#include <vector>
#include "EventStorage/EventStorageRecords.h"
#include "EventStorage/pickDataReader.h"
#include <boost/test/unit_test.hpp>



#define COMPDATA 0xFFAABBFF

static const uint32_t size = 1000000;

void get_common_params(EventStorage::run_parameters_record & rPar, 
		       std::vector<std::string>& fmds, 
		       std::string& project, 
		       std::string& streamtype, 
		       std::string& streamname,
		       unsigned int& lb,
		       std::string& app);

void check_metadata(DataReader *pDR);

void get_common_params(EventStorage::run_parameters_record & rPar, 
		       std::vector<std::string>& fmds, 
		       std::string& project, 
		       std::string& streamtype, 
		       std::string& streamname,
		       unsigned int& lb,
		       std::string& app){
 
  rPar.run_number = 100;     
  rPar.max_events = 0;     
  rPar.rec_enable = 1;     
  rPar.trigger_type = 0;   
  rPar.detector_mask_LS = 0x12345678abcdefcdLL;  
  rPar.detector_mask_MS = 0xdcfedcba00000000LL;  
  rPar.beam_type = 211;      
  rPar.beam_energy = 220;    

  
  fmds.push_back("exampleMetaData=Value");
  fmds.push_back("key=ValueValue");

  project = "data_test";
  streamtype = "type";
  streamname = "name";
  lb = 10;
  app = "app";

}

void check_metadata(DataReader *pDR){
  
  EventStorage::run_parameters_record rPar;
  std::vector<std::string> fmds;
  std::string project;
  std::string streamtype; 
  std::string streamname;
  unsigned int lb;
  std::string app;

  get_common_params(rPar ,fmds, project, streamtype, streamname, lb, app);

  BOOST_CHECK_EQUAL(pDR->lumiblockNumber(), lb);
  BOOST_CHECK_EQUAL(pDR->stream(), streamtype+"_"+streamname);
  BOOST_CHECK_EQUAL(pDR->projectTag(), project);

  auto mask = std::bitset<128>(rPar.detector_mask_MS);
  mask <<= 64;
  mask |= rPar.detector_mask_LS;

  BOOST_CHECK_EQUAL(pDR->detectorMask(), mask);
  BOOST_CHECK_EQUAL(pDR->triggerType(), rPar.trigger_type);
  BOOST_CHECK_EQUAL(pDR->runNumber(), rPar.run_number);
  BOOST_CHECK_EQUAL(pDR->beamType(), rPar.beam_type);
  BOOST_CHECK_EQUAL(pDR->beamEnergy(), rPar.beam_energy);
  BOOST_CHECK_EQUAL(pDR->recEnable(), rPar.rec_enable);
  BOOST_CHECK_EQUAL(pDR->appName(), app);
  
  BOOST_CHECK_EQUAL(pDR->freeMetaDataStrings().size(), fmds.size());
  BOOST_CHECK((pDR->freeMetaDataStrings()==fmds));
}

void check_merge_metadata(DataReader *pDR){
  
  EventStorage::run_parameters_record rPar;
  std::vector<std::string> fmds;
  std::string project;
  std::string streamtype; 
  std::string streamname;
  unsigned int lb;
  std::string app;

  get_common_params(rPar ,fmds, project, streamtype, streamname, lb, app);

  BOOST_CHECK_EQUAL(pDR->lumiblockNumber(), lb);
  BOOST_CHECK_EQUAL(pDR->stream(), streamtype+"_"+streamname);
  BOOST_CHECK_EQUAL(pDR->projectTag(), project);

  auto mask = std::bitset<128>(rPar.detector_mask_MS);
  mask <<= 64;
  mask |= rPar.detector_mask_LS;

  BOOST_CHECK_EQUAL(pDR->detectorMask(), mask);
  BOOST_CHECK_EQUAL(pDR->triggerType(), rPar.trigger_type);
  BOOST_CHECK_EQUAL(pDR->runNumber(), rPar.run_number);
  BOOST_CHECK_EQUAL(pDR->beamType(), rPar.beam_type);
  BOOST_CHECK_EQUAL(pDR->beamEnergy(), rPar.beam_energy);
  BOOST_CHECK_EQUAL(pDR->recEnable(), rPar.rec_enable);
    
  BOOST_CHECK_EQUAL(pDR->freeMetaDataStrings().size(), fmds.size());
  BOOST_CHECK((pDR->freeMetaDataStrings()==fmds));
}

