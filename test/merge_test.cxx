//Dear emacs, this is -*- c++ -*-

/**
 * @file merge_test.cxx
 * @author <a href="mailto:Wainer.Vandelli@cern.ch">Wainer.Vandelli@cern.ch</a> 
 * $Author: $
 * $Revision: $
 * $Date: $
 * 
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE Merge Tests
#define BOOST_TEST_MAIN

#include <boost/test/unit_test.hpp>

#include <iostream>
#include <stdio.h>

#include "./test_common.h"
#include "../src/MergedRawFile.h"
#include "EventStorage/EventStorageIssues.h"



BOOST_AUTO_TEST_CASE( write_merged_file ){
  
  std::string fname = "merge";

  /* Remove file to avoid issues */
  ::remove((fname+"._0001.data").c_str());

  std::vector<std::string> fns;
  fns.push_back("test.data");
  fns.push_back("test.data");
  
  MergedRawFile merge(fns, fname, 1000*1000*1000);

  std::vector<std::string> outNames = merge.getOutFileNames();

  BOOST_CHECK_EQUAL(outNames.size(), 1);

}


BOOST_AUTO_TEST_CASE( write_merged_file_too_small ){
  
  std::string fname = "merge-small";

  /* Remove file to avoid issues */
  ::remove((fname+"._0001.data").c_str());

  std::vector<std::string> fns;
  fns.push_back("test.data");
  fns.push_back("test.data");
  
  BOOST_CHECK_THROW(MergedRawFile merge(fns, fname, 10), 
		    EventStorage::WritingIssue);
}


BOOST_AUTO_TEST_CASE( write_merged_file_comp ){
  
  std::string fname = "merge-comp";

  /* Remove file to avoid issues */
  ::remove((fname+"._0001.data").c_str());

  std::vector<std::string> fns;
  fns.push_back("test-comp.data");
  fns.push_back("test-comp.data");
  
  MergedRawFile merge(fns, fname, 1000*1000*1000);

  std::vector<std::string> outNames = merge.getOutFileNames();

  BOOST_CHECK_EQUAL(outNames.size(), 1);
}

BOOST_AUTO_TEST_CASE( write_merged_file_mix1 ){
  
  std::string fname = "merge-mix1";

  /* Remove file to avoid issues */
  ::remove((fname+"._0001.data").c_str());

  std::vector<std::string> fns;
  fns.push_back("test.data");
  fns.push_back("test-comp.data");
  
  MergedRawFile merge(fns, fname, 1000*1000*1000);

  std::vector<std::string> outNames = merge.getOutFileNames();

  BOOST_CHECK_EQUAL(outNames.size(), 1);
}

BOOST_AUTO_TEST_CASE( write_merged_file_mix2 ){
  
  std::string fname = "merge-mix2";

  /* Remove file to avoid issues */
  ::remove((fname+"._0001.data").c_str());

  std::vector<std::string> fns;
  fns.push_back("test-comp.data");
  fns.push_back("test.data");
  
  MergedRawFile merge(fns, fname, 1000*1000*1000);

  std::vector<std::string> outNames = merge.getOutFileNames();

  BOOST_CHECK_EQUAL(outNames.size(), 1);
}
