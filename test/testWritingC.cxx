#include <iostream>
#include "clocks/Time.h"
#include "clocks/Clock.h"

#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

int main (int argc, char *argv[])
{
    std::cout << "Please name file to write, including path: ";
    std::string fname;
    std::cin >> fname;
    
    int fd = open(fname.c_str(),O_WRONLY | O_CREAT);    

    if(fd == -1) {
      std::cout<<"failed opening the file. errno: "<<errno<<" EISDIR: "<<EISDIR<<std::endl;
    }

    std::cout << "Event size in kB: ";
    unsigned int eventSize;
    std::cin >> eventSize; eventSize *= 1024;

    char *fill = new char[eventSize];
    for(unsigned int i=0; i<eventSize; i++) fill[i]='d';
    void *event = fill;
    
    std::cout << "Number of events: ";
    unsigned int nev;
    std::cin >> nev;
   
    //dw.setMaxFileMB(2);
    //dw.setMaxFileNE(5);

    std::cout << "How many intermediate measurements in this run? ";   
    unsigned int intv;
    std::cin >> intv;
    if(intv<=1) { 
      intv = 0;
      std::cout << "No intermediate measurements.\n";
    } else {
      intv = nev/intv;
      if(intv==0) intv=1;
      std::cout << "Intermediate measuerement every " << intv << " events\n";
    }

    RealTimeClock rt_clock;
    Time startTime=rt_clock.time();
    Time previousTime=startTime;

    std::cout << "Starting the run now!\n";

    for(unsigned int ev=0; ev<nev; ev++) 
    {
       write(fd, reinterpret_cast<const char*>(event),eventSize);
       //dw.putData(eventSize,event);

       if(intv and !((ev+1)%intv)) {
	 std::cout << "Event " << ev+1;
	 Time now = rt_clock.time();
	 Time tdiff = now - previousTime;
	 previousTime = now; 
	 double tds = tdiff.as_seconds();
	 double mbps = intv*eventSize; mbps /= 1024*1024*tds;
	 double evps = intv; evps /= tds;
	 std::cout << ", interval " << tds << " s,";
	 std::cout << " current rate is " << mbps << " MB/s or " << evps << " Hz.\n"; 
       }
    }

    Time now=rt_clock.time();
    Time tdiff = now - startTime;
    double tds = tdiff.as_seconds();
    std::cout << "Time of this run in seconds is " << tds << std::endl;
    double mb = (eventSize/1024)*nev; mb /= 1024;
    std::cout << "Total data written is "<< mb << " MB or " << nev << " events.\n";
    std::cout << "Average rate is "<< mb/tds << " MB/s";
    double evhz=nev; evhz /= tds;
    std::cout << " or "<<evhz<<" Hz."<<std::endl;

    //optionally, to test the reader
    //dw.closeFile();
}
