// 
// read events from specified offsets
// list of offsets is given in a file *.index
// given as the 2nd parameter
// .index can be produced by testWritingSpeed or by readData
// 

#include <iostream>
#include <fstream>
#include <memory>
#include <string>
#include <stdint.h>
#include <cstdlib> 

#include "EventStorage/pickDataReader.h"     
// function to get the universal reader

int main (int argc, char *argv[])
{
    using namespace std;

    string dataFileName(""), indexFileName("");


    if(argc>2) {
      dataFileName = argv[1];
      indexFileName = argv[2];
    } else {
      cout << "Usage:\n";
      cout << "testOffsets <data file name> <index file name>\n";
      ::exit(0);
    }

    cout << "Get DataReader for file " << dataFileName << endl;
    DataReader *pDR = pickDataReader(dataFileName);

    cout << "pickDataReader returned with pointer " << pDR << endl << flush;
    
    if(!pDR) {
      cout << "Problem opening or reading this file!\n"<<flush;
      return -1;
    }

    cout << "Trying to read file " << pDR->fileName() << endl;
    if(!pDR->good()) {
      cout << "There is a problem!" << endl;
      return -1;
    } 
    cout << "DataReader setup OK!\n";


    // This info (for instance) is available without penalty at start time.
    cout << "File start time " << pDR->fileStartTime() << endl;
    cout << "File start date " << pDR->fileStartDate() << endl;

    // Getting this info will cost a rewind to the end of file and back."
    cout << "There are " << pDR->eventsInFile() << " events in this file.\n";
    cout << "There are " << pDR->dataMB_InFile() << " MB in this file.\n";

    ifstream indexFile(indexFileName.c_str(),ios::in);

    unsigned int eventCounter=0;

    cout << "Start the event loop.\n\n";

    while(indexFile.good()) 
    {
      unsigned int evNo; int64_t pos=-1;
      indexFile >> evNo >> pos;
      if(pos<0) break; // end of index file

      cout << "Expecting event "<<evNo <<" at offset "<<pos<<endl<<flush;
      
      //Method taken out after testing. It is too risky and recovery
      // in case of error is tricky.  
      //pDR->setPosition(pos);
      //cout <<"Offset before read "<<pDR->getPosition()<<endl;

      unsigned int eventSize;    
      char *buf;
	
      DRError ecode = pDR->getData(eventSize,&buf,pos);
      cout << "Error code after reading "<<ecode<<endl<<flush;
      
      if(DRWOFF == ecode) {
	cout << "Event not found. Try next one.\n\n" << flush;
	continue;
      } else if(DROK != ecode) {
	cout << "DataReader is not OK. Exit.\n\n" << flush;
	break; 
      }

      ++eventCounter; 

      unsigned int * fill = reinterpret_cast<unsigned int *>(buf);
      unsigned int evFound = fill[1];
      
      cout << "Found event " << evFound 
	   << " with size " << eventSize
	   << " bytes." << endl;
      cout << "Offset after read "<<pDR->getPosition()<<endl<<endl;

      delete [] buf;
    }

    if(pDR->endOfFileSequence()) {
      cout << "Found end of file sequence.\n";
    } 

    cout << "Retrived " << eventCounter << " events.\n";
    cout << "Last one has number " << pDR->latestDataNumber() << "." << endl;

    cout << "Events in file sequence up to this file " 
	      << pDR->eventsInFileSequence() << endl;
    //cout << "MB in file sequence up to this file " 
	   //   << pDR->dataMB_InFileSequence() << endl;
    cout << "File end time " << pDR->fileEndTime() << endl;
    cout << "File end date " << pDR->fileEndDate() << endl;

    delete pDR;
    cout << "Reader deleted. The end.\n\n";

}
