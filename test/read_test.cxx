//Dear emacs, this is -*- c++ -*-

/**
 * @file read_test.cxx
 * @author <a href="mailto:Wainer.Vandelli@cern.ch">Wainer.Vandelli@cern.ch</a> 
 * $Author: $
 * $Revision: $
 * $Date: $
 * 
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE Read Tests
#define BOOST_TEST_MAIN

#include <iostream>

#include <boost/test/unit_test.hpp>
#include "EventStorage/pickDataReader.h"
#include "EventStorage/EventStorageIssues.h"

#include "./test_common.h"
#include <algorithm>


BOOST_AUTO_TEST_CASE( read_file_compressed )
{
  
  std::string fname = "test-comp.data";
  
  DataReader *pDR = pickDataReader(fname);

  BOOST_CHECK(pDR);
  BOOST_CHECK(pDR->good());
  
  BOOST_CHECK_EQUAL(pDR->fileName(), fname);

  check_metadata(pDR);

  BOOST_CHECK_EQUAL(pDR->compression(), EventStorage::ZLIB);

  std::vector<char *> events;
  std::vector<uint32_t> sizes;

  while(pDR->good()){
    uint32_t eventSize;
    char * buf;
    
    DRError err = pDR->getData(eventSize,&buf);
    BOOST_CHECK_EQUAL(err, EventStorage::DROK);

    events.push_back(buf);
    sizes.push_back(eventSize);
  }

  std::vector<uint32_t>::const_iterator sizeit = sizes.begin();
  
  while(sizeit != sizes.end()){

    BOOST_CHECK_EQUAL(*sizeit,size);
    ++sizeit;
  }

  std::vector<char *>::const_iterator evit = events.begin();

  while(evit != events.end()){

    uint32_t * p = (uint32_t*)(*evit);
    for(unsigned int i=0; i<(*sizes.begin())/4; ++i){
      BOOST_CHECK_EQUAL(COMPDATA,p[i]);
    }
    delete[] *evit;
    ++evit;
  }

  delete pDR;

}


BOOST_AUTO_TEST_CASE( read_file_compressed_at_random_offset )
{
  
  std::string fname = "test-comp.data";
  
  DataReader *pDR = pickDataReader(fname);

  BOOST_CHECK(pDR);
  BOOST_CHECK(pDR->good());
  
  BOOST_CHECK_EQUAL(pDR->fileName(), fname);

  check_metadata(pDR);

  BOOST_CHECK_EQUAL(pDR->compression(), EventStorage::ZLIB);

  std::vector<int64_t> offsets;

  while(pDR->good()){
    uint32_t eventSize;
    char * buf;
    
    offsets.push_back(pDR->getPosition());
    DRError err = pDR->getData(eventSize,&buf);
    BOOST_CHECK_EQUAL(err, EventStorage::DROK);
    delete[] buf;
  }

  delete pDR;
  
  pDR = pickDataReader(fname);
  
  //Randomize offsets
  std::random_shuffle(offsets.begin(),offsets.end());

  for(const auto &offset: offsets){
    char * buf;
    uint32_t eventSize;
    DRError err = pDR->getData(eventSize,&buf, offset);
    BOOST_CHECK_EQUAL(err, EventStorage::DROK);
    
    uint32_t * p = (uint32_t*)(buf);
    for(unsigned int i=0; i<(eventSize)/4; ++i){
      BOOST_CHECK_EQUAL(COMPDATA,p[i]);
    }
    delete[] buf;
  }

  delete pDR;
}



BOOST_AUTO_TEST_CASE( read_file )
{
  
  std::string fname = "test.data";
  
  DataReader *pDR = pickDataReader(fname);

  BOOST_CHECK(pDR);
  BOOST_CHECK(pDR->good());
  
  check_metadata(pDR);
  
  BOOST_CHECK_EQUAL(pDR->compression(), EventStorage::NONE);

  std::vector<char *> events;
  std::vector<uint32_t> sizes;

  while(pDR->good()){
    uint32_t eventSize;
    char * buf;
    
    DRError err = pDR->getData(eventSize,&buf);
    BOOST_CHECK_EQUAL(err, EventStorage::DROK);
        
    events.push_back(buf);
    sizes.push_back(eventSize);
  }

  std::vector<uint32_t>::const_iterator sizeit = sizes.begin();
  
  while(sizeit != sizes.end()){
    
    BOOST_CHECK_EQUAL(*sizeit,size);
    ++sizeit;
  }

  std::vector<char *>::const_iterator evit = events.begin();
  
  while(evit != events.end()){

    uint32_t * p = (uint32_t*)(*evit);
    for(unsigned int i=0; i<(*sizes.begin())/4; ++i){
      BOOST_CHECK_EQUAL(COMPDATA,p[i]);
    }
    delete[] *evit;
    ++evit;
  }

  delete pDR;
}


BOOST_AUTO_TEST_CASE( read_file_prealloc )
{
  
  std::string fname = "test.data";
  
  DataReader *pDR = pickDataReader(fname);

  BOOST_CHECK(pDR);
  BOOST_CHECK(pDR->good());
  
  check_metadata(pDR);

  char * buf = new char[size];
  
  while(pDR->good()){
    uint32_t eventSize;
      
    DRError err = pDR->getDataPreAlloced(eventSize,&buf, size);
    BOOST_CHECK_EQUAL(err, EventStorage::DROK);
    
  }
  
  delete pDR;
  delete[] buf;
}

BOOST_AUTO_TEST_CASE( read_file_prealloc_comp )
{
  
  std::string fname = "test-comp.data";
  
  DataReader *pDR = pickDataReader(fname);

  BOOST_CHECK(pDR);
  BOOST_CHECK(pDR->good());
  
  check_metadata(pDR);

  char * buf = new char[size];
  
  while(pDR->good()){
    uint32_t eventSize;
      
    DRError err = pDR->getDataPreAlloced(eventSize,&buf, size);
    BOOST_CHECK_EQUAL(err, EventStorage::DROK);

  }

  delete pDR;
  delete[] buf;
}

BOOST_AUTO_TEST_CASE( read_file_prealloc_too_small )
{
  
  std::string fname = "test.data";
  
  DataReader *pDR = pickDataReader(fname);

  BOOST_CHECK(pDR);
  BOOST_CHECK(pDR->good());
  
  check_metadata(pDR);

  uint32_t bufsize = size/2;
  char * buf = new char[bufsize];
  
  unsigned int i = 0;
  while(pDR->good() && i < 10){
    uint32_t eventSize;
    
    BOOST_CHECK_THROW(pDR->getDataPreAlloced(eventSize,&buf,bufsize), 
		      EventStorage::ES_AllocatedMemoryTooLittle);
    i++;
  }
  BOOST_CHECK_EQUAL(10,i);
 
  delete[] buf;
  bufsize = size;
  buf = new char[bufsize];
  while(pDR->good()){
    uint32_t eventSize;
  
    DRError err = pDR->getDataPreAlloced(eventSize,&buf, size);
    BOOST_CHECK_EQUAL(err, EventStorage::DROK);
  }


  delete pDR;
  delete[] buf;

}

BOOST_AUTO_TEST_CASE( read_file_prealloc_too_small_comp )
{
  
  std::string fname = "test-comp.data";
  
  DataReader *pDR = pickDataReader(fname);

  BOOST_CHECK(pDR);
  BOOST_CHECK(pDR->good());
  
  check_metadata(pDR);

  uint32_t bufsize = size/2;
  char * buf = new char[bufsize];
  
  unsigned int i = 0;
  while(pDR->good() && i<10){
    uint32_t eventSize;
    
    BOOST_CHECK_THROW(pDR->getDataPreAlloced(eventSize,&buf,bufsize), 
		      EventStorage::ES_AllocatedMemoryTooLittle);
    i++;
  }
  
  BOOST_CHECK_EQUAL(10,i);
 
  delete[] buf;
  bufsize = size;
  buf = new char[bufsize];
  while(pDR->good()){
    uint32_t eventSize;
  
    DRError err = pDR->getDataPreAlloced(eventSize,&buf, size);
    BOOST_CHECK_EQUAL(err, EventStorage::DROK);
  }

  delete pDR;
  delete[] buf;
}
