/**
   \file testWritingStreams.cxx
   \author Szymon Gadomski
   \brief Write several sequences of files and measure disk writing speed.

   Like testWritingSpeed but with multiple streams.
   Written for tests of SFO hardware in Nov 06
   reads streams-config.txt to find directories
   and relative rates for the streams
*/

#include <iostream>
#include <string>
#include <iomanip>
#include <vector>
#include <sstream>
#include <stdlib.h>
#include <chrono>

#include "EventStorage/DataWriter.h"
#include "EventStorage/RawFileName.h"
#include "EventStorage/EventStorageRecords.h"
#include "EventStorage/DWCBcout.h"

#include <unistd.h>

// for rand()
#include <stdlib.h>

std::vector<std::string> dirs;
std::vector<float> probs;

void config_streams() {
  using namespace std;

  dirs.clear();
  probs.clear();
  int nstream=0;
  float totalRate=0.;

  cout << "Configure the streams from streams-config.txt.\n";
  ifstream inf("streams-config.txt");
  if(!inf.good()) {
    cout << "Problem opening streams-config.txt. File not found.\n'";
    exit(0);
  }
  const char *comline="#"; 
  while(!inf.eof()) {
    if(inf.peek()==*comline) {
      // cout << "skip comment line\n";
     inf.ignore(500,'\n');
    } else {
      string dir; float rate=0;
      inf >> dir >> rate;
      inf.ignore(500,'\n');
      if(rate>0) {
	cout << "Add stream with directory "<<dir<<" and rate "<<rate<<endl;
	dirs.push_back(dir);
	probs.push_back(rate);
	++nstream; totalRate+=rate;
      }
    }
  }

  cout << "Streams configured:\n";
  for(int i=0; i<nstream; ++i) {
    probs[i]=probs[i]/totalRate;
    cout << dirs[i] << " " << probs[i] << endl;
  }
}

int stream() {
  using namespace std;

  float shot=((float)rand()/RAND_MAX);
  //cout << "shotEx "<<shot<<" ";

  unsigned int is=0;
  while(is!=probs.size()) {
    shot-=probs[is];
    if(shot<0) break;
    ++is;
  }

  if(is==probs.size()) {
    //cout << "Exclusive stream not calculated! ";
    return -1;
  }
  return is;
}

int iinpar(std::string vnam, int def) 
{
  using namespace std;
  cout << vnam << " (default "<<def<<") : ";
  string ans=""; getline(cin,ans);
  int vl=def;
  if(ans.size()>0) vl=atoi(ans.c_str());
  cout <<"Set to "<<vl<<endl;
  return vl;
}

float finpar(std::string vnam, float def) 
{
  using namespace std;
  cout << vnam << " (default "<<def<<") : ";
  string ans=""; getline(cin,ans);
  float vl=def;
  if(ans.size()>0) vl=atof(ans.c_str());
  cout <<"Set to "<<vl<<endl;
  return vl;
}

int main ()
{
  using namespace std;
  using namespace EventStorage;

  config_streams();

  unsigned int runNumber=1;
  string StreamType("Test");
  unsigned int LumiBlockNumber=0;
  string apName("testWritingStreams");
    
  run_parameters_record rPar;

  rPar.run_number = 100;     
  rPar.max_events = 0;     
  rPar.rec_enable = 1;     
  rPar.trigger_type = 0;   
  rPar.detector_mask_LS = 111;  
  rPar.beam_type = 211;      
  rPar.beam_energy = 220;    
 
  //cout << "rPar = \n" << flush;
  //string rps=string_record(&rPar,&run_parameters_pattern);
  //cout << rps << endl;

  EventStorage::freeMetaDataStrings fmds;
  fmds.push_back("Test metadata strings.");
  fmds.push_back("tag=value");
  fmds.push_back("key = 125");
  fmds.push_back("label = AnotherValue");
  cout << "freeMetaDataStrings with size " <<fmds.size() <<endl;

  cout << "Constructing the DataWriters\n" << flush;
  vector<DataWriter *> pWriters;
  pWriters.clear();

  for(unsigned int i=0;i<dirs.size();++i) {
    ostringstream nt;
    nt << "Stream" << i;
    string StreamName = nt.str();

    string project("data_test");
    daq::RawFileName my_fname(project,runNumber,StreamType,
                              StreamName,LumiBlockNumber,apName);

    string fName = my_fname.fileNameCore();

    DataWriter *pdw = new DataWriter(dirs[i],fName,rPar,fmds);
 
    if (!pdw->good()) {
      cout << "Problem opening file in directory "<<dirs[i]<<"."<<endl;
      cout << "Is the directory there and writable for you?"<<endl;
      exit(1);
    }
    pWriters.push_back(pdw);
  }

  unsigned int eventSize=iinpar("Event size in kB",1600);
  eventSize*=1024;

  unsigned int eventFile=iinpar("How many events in each file (0 means no limit)",1000);

  float evDelayF = finpar("Delay between events (float, in ms)",0);

  unsigned long evDelay = (unsigned long)(evDelayF*1000);
  cout << "The delay is " << evDelay << " microseconds.\n";

  unsigned int *fill = new unsigned int[eventSize/4];
  for(unsigned int i=0; i<eventSize/4; i++) { 
    fill[i]=0x1AAAAAA1;
    //cout << fill[i] << " ";
  }
  void *event = fill;
    
  unsigned int nev = iinpar("Number of events",10000);
 
  for(unsigned int i=0;i!=pWriters.size();++i) (pWriters[i])->setMaxFileNE(eventFile);

  double tmeas=finpar("How often to measure speed? Interval in seconds (<=0 means no measurement)",10);
  if(tmeas<=0.0) { 
    tmeas = 0.0;
    cout << "No measurements of speed.\n";
  } else {
    cout << "Intermediate measuerement every " << tmeas << " seconds\n";
  }

  // call-back example
  cout << "Print file names? (y/n) (default n)" << endl;
  string ans=""; getline(cin,ans);
  if(ans=="y") {
    cout << "Register call-back class to print file names." << endl;
    DWCBcout *pCB = new DWCBcout("testWritingSpeed");
    for(unsigned int i=0;i!=pWriters.size();++i) (pWriters[i])->registerCallBack(pCB);
  } else {
    cout << "File names not printed." << endl;
  }

  auto startTime = std::chrono::high_resolution_clock::now();
  auto previousTime = startTime;
  unsigned int pev=0;

  // save start time
  ofstream tstartFile("StartTime.dat",ios::out);
  tstartFile << setprecision(12) << startTime.time_since_epoch().count() 
	     << endl;
  tstartFile.close();
  
  cout << "Starting the run now!\n";    

  for(unsigned int ev=0; ev<nev; ev++) {
    if(evDelay) usleep(evDelay); 
  
    int is=stream();
    (pWriters[is])->putData(eventSize,event);

    auto now = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> tdiff = now - previousTime;
       
    if(tmeas > 0 && tdiff.count() > tmeas) {
      double tds = tdiff.count();
      double mbps = (ev-pev)*eventSize/(1024*1024*tds);
      double evps = (ev-pev)/tds;
      std::chrono::duration<double> sincestart = (now-startTime);
      cout << "Time " << sincestart.count();
      cout << " interval " << tds << " s,";
      cout << " event " << ev;
      cout << " current rate " << mbps << " MB/s or " << evps << " Hz.\n"; 
      previousTime = now; 
      pev = ev;
    }

    if(!((pWriters[is])->good())) {
      cout << "Can't write anymore! Stop after event "<<ev<<endl;
      break;
    }
  }

  auto now = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> tdiff = now - startTime;
  double tds = tdiff.count();
  cout << "Time of this run in seconds is " << tds << endl;
  double mb = (eventSize/1024)*nev; mb /= 1024;
  cout << "Total data written is "<< mb << " MB or " << nev << " events.\n";
  cout << "Average rate is "<< mb/tds << " MB/s";
  double evhz=nev; evhz /= tds;
  cout << " or "<<evhz<<" Hz."<<endl;

  for(unsigned int i=0;i!=pWriters.size();++i) delete pWriters[i];

  //optionally, to test the reader
  //dw.closeFile();
}
