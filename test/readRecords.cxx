#include <iostream>

#include <fcntl.h>
#include <shift.h>

#include "EventStorage/EventStorageRecords.h"

FILE *pifs;

int unfile_record(void *ri, const void *pi) {

  unsigned int *record = (unsigned int *)ri;
  unsigned int *pattern = (unsigned int *)pi;
  unsigned int size=pattern[1];
  //std::cout << "\nread size = " << size << std::endl;

  for(unsigned int i=0; i<size; i++) {
    if(pattern[i] != 0) {
      unsigned int tst=0;
      rfio_fread((char *)(&tst),sizeof(unsigned int),1,pifs);
      if(tst==pattern[i]) {
	//std::cout << "pattern " << pattern[i] << " OK" << std::endl;
      } else {
	//std::cout << "pattern " << pattern[i] << " != " << tst << "abort!" << std::endl;
	return 0;
      }
    } else {
      rfio_fread((char *)(record+i),sizeof(unsigned int),1,pifs);
      //std::cout << "record " << record[i] << std::endl;
    }
  }
  return size;
}

void* unfile_record(const void *pi) {

  unsigned int *pattern = (unsigned int *)pi;
  unsigned int size=pattern[1];
  //std::cout << "\nread size = " << size << std::endl;

  unsigned int *record = new uint32_t[size];

  for(unsigned int i=0; i<size; i++) {
    if(pattern[i] != 0) {
      unsigned int tst=0;
      rfio_fread((char *)(&tst),sizeof(unsigned int),1,pifs);
      if(tst==pattern[i]) {
	//std::cout << "pattern " << pattern[i] << " OK" << std::endl;
      } else {
	//std::cout << "pattern " << pattern[i] << " != " << tst << "abort!" << std::endl;
	return NULL;
      }
    } else {
      rfio_fread((char *)(record+i),sizeof(unsigned int),1,pifs);
      //std::cout << "record " << record[i] << std::endl;
    }
  }
  
  return record;
}

EventStorage::file_name_strings unfile_name_strings() {

  uint32_t tst;

  EventStorage::file_name_strings nst;
  nst.appName="there was"; nst.fileNameTag="a problem";

  rfio_fread((char *)(&tst),sizeof(uint32_t),1,pifs);
  if(tst != EventStorage::file_name_strings_marker) return nst;

  uint32_t size=0;
  rfio_fread((char *)(&size),sizeof(uint32_t),1,pifs);
  
  char *csn=new char[size];
  rfio_fread(csn,size,1,pifs);
  std::string name(csn);
  delete [] csn;

  char buf[4];
  char ns = size % 4;
  if(ns) rfio_fread(buf,4-ns,1,pifs);

  rfio_fread((char *)(&size),sizeof(uint32_t),1,pifs);
  
  char *cst=new char[size];
  rfio_fread(cst,size,1,pifs);
  std::string tag(cst);
  delete [] cst;

  ns = size % 4;
  if(ns) rfio_fread(buf,4-ns,1,pifs);
  
  nst.appName = name;
  nst.fileNameTag = tag;

  return nst;
}

std::string unfile_string() {

  uint32_t tst;
  rfio_fread((char *)(&tst),sizeof(uint32_t),1,pifs);
  if(tst != EventStorage::single_string_marker) return "A problem!";

  uint32_t size=0;
  rfio_fread((char *)(&size),sizeof(uint32_t),1,pifs);
  
  //std::cout << "/n read string size " << size ;

  char *cs=new char[size];
  rfio_fread(cs,size,1,pifs);

  std::string s(cs);
  delete [] cs;
  //std::cout << " value " << s;

  char ns = size % 4;
  //std::cout << " with padding " << 4-ns << std::endl;
  if(ns) {
    char buf[4];
    rfio_fread(buf,4-ns,1,pifs);
  }
  
  return s;
}

int main ()
{
    using namespace EventStorage;

    //pifs = rfio_fopen("blah.dat",(char *)"r");
    pifs = rfio_fopen("EventStorage.test_records.data","r");

    file_start_record *rstart = (file_start_record *)unfile_record(&file_start_pattern);

    //read_record(&rstart,&file_start_pattern);
    //std::cout << "rstart.time " << rstart->time << std::endl;
    //std::cout << "rstart.date " << rstart->date << std::endl;

    file_name_strings nst = unfile_name_strings();

    run_parameters_record *rpar = (run_parameters_record *)unfile_record(&run_parameters_pattern);

    std::string tag = unfile_string();

    data_separator_record *rev = (data_separator_record *)unfile_record(&data_separator_pattern);

    file_end_record *rend = (file_end_record *)unfile_record(&file_end_pattern);

    rfio_fclose(pifs);

    std::cout << "The records found:\n";
    std::cout << string_record(rstart,&file_start_pattern) << std::endl;
    std::cout << string_record(nst) << std::endl;
    std::cout << string_record(rpar,&run_parameters_pattern) << std::endl;
    std::cout << string_record(tag) << std::endl;
    std::cout << string_record(rev,&data_separator_pattern) << std::endl;
    std::cout << string_record(rend,&file_end_pattern) << std::endl;
 
    file_start_record rstart2 = *rstart;
    delete rstart;
    std::cout << "copy of start " << string_record(&rstart2,&file_start_pattern) << std::endl;

}
