/**
   \file testFileNames.cxx
   \author Kostas Kordas
   \brief Create and interpret raw file names
*/

#include <iostream>
#include <string>
#include <iomanip>
#include <boost/tokenizer.hpp>

#include "EventStorage/RawFileName.h"

int main ()
{

  std::cout << "==============================================" << std::endl;
  std::cout << "============ Test RawFileName class ==========" << std::endl;
  std::cout << "==============================================" << std::endl;

  std::cout << "" << std::endl;
  std::cout << " ======== Test spliting of strings: ========" << std::endl;

  std::string mystring = "_.KK.test1.test2.data";
  std::cout << "Size of mystring: " << mystring.size() << std::endl;

  std::string myname   = daq::RawFileName::split(mystring,'_')[0];
 
  std::cout << " ----> mystring: " << mystring 
	    << "  , split(mystring,'_')[0]: " << myname << std::endl;
  
  char mydot = myname.c_str()[0];
  if ( mydot == daq::RAWFILENAME_DELIMITER.c_str()[0] ) {
    std::cout << " myname starts with a dot:  " << myname << std::endl;
  } else {
    std::cout << " myname has no dot in front:" << myname << std::endl;
  }

  mystring = "90A";	
  std::cout << " atoi(" << mystring << ") = " << atoi( mystring.c_str() ) 
	    << std::endl;
  mystring = "A-90";	
  std::cout << " atoi(" << mystring << ") = " << atoi( mystring.c_str() ) 
	    << std::endl;
  mystring = "_90";	
  std::cout << " atoi(" << mystring << ") = " << atoi( mystring.c_str() ) 
	    << std::endl;


  std::cout << "" << std::endl;
  mystring ="proj.00012345._sName.daq.RAW._lb0001._appName._00001.data";
  std::cout << "  Using boost::tokenizer on the name: " << mystring 
	    << std::endl;

  std::vector<std::string> container;
  typedef boost::tokenizer<boost::char_separator<char> >  tokenizer;
  std::string mysep = ".";
  boost::char_separator<char> sep(mysep.c_str(), "", boost::keep_empty_tokens);

  tokenizer tokens(mystring, sep);
  for ( tokenizer::iterator tok_iter = tokens.begin(); 
	tok_iter != tokens.end(); ++tok_iter) {
    container.push_back(*tok_iter);
    std::cout << "field: " << *tok_iter << std::endl;
  }

  if ( container.size() >= 3 ) {
    std::cout << " spliting further the field " << container[2] << std::endl;
    boost::char_separator<char> sep2("_", "", boost::keep_empty_tokens);
    tokenizer tokens2(container[2], sep2);
    for ( tokenizer::iterator tok_iter2 = tokens2.begin(); 
	  tok_iter2 != tokens2.end(); ++tok_iter2) {
      std::cout << "field: " << *tok_iter2 << std::endl;
    }
  }

  std::cout << "" << std::endl;
  std::cout << "============ Test the Constructors: ========" << std::endl;

  std::cout << std::endl;
  std::cout << " -- CONSTRUCTOR 1a with Core ingredients: " << std::endl;
  std::cout << "    -------------- " << std::endl;
  daq::RawFileName fn1a("",12345,"stype","sname",0,
			   "KSK");
  fn1a.print();
  fn1a.setTailers(1821,daq::RAWFILENAME_EXTENSION_FINISHED+daq::RAWFILENAME_EXTENSION_UNFINISHED);
  fn1a.print();
  fn1a.setFileSequenceNumber(8);
  fn1a.print();
  fn1a.setExtension(".mydata.extra");
  fn1a.print();
  fn1a.interpretFileName( fn1a.fileName());
  fn1a.print();
  fn1a.interpretFileName( fn1a.fileNameCore());
  fn1a.print();

  std::cout << std::endl;
  std::cout << " -- CONSTRUCTOR 1b with all ingredients: " << std::endl;
  std::cout << "    -------------- " << std::endl;
  daq::RawFileName fn1b("project_08",12345,"stype","sname",9,
			   "KSK","daq","RAW",11,".myext");
  fn1b.print();
  fn1b.setFileSequenceNumber(8);
  fn1b.print();
  fn1b.setExtension(".mydata");
  fn1b.print();
  fn1b.setExtension(".mydata.extra");
  fn1b.print();
  fn1b.interpretFileName( fn1b.fileName());
  fn1b.print();
  fn1b.interpretFileName( fn1b.fileNameCore());
  fn1b.print();


  std::cout << std::endl;
  std::cout << " -- CONSTRUCTOR 2a with core and seqNum only: " << std::endl;
  std::cout << "    -------------- " << std::endl;
  daq::RawFileName fn2a("project_08.00012345.stype_sname.daq.RAW._lb0008._KSK",10);
  fn2a.print();
  fn2a.setExtension(".mydata.extra");
  fn2a.print();
  fn2a.setFileSequenceNumber(8);
  fn2a.print();
  fn2a.setExtension(".mydata");
  fn2a.print();
  fn2a.interpretFileName( fn2a.fileName());
  fn2a.print();
  fn2a.interpretFileName( fn2a.fileNameCore());
  fn2a.print();


  std::cout << std::endl;
  std::cout << " -- CONSTRUCTOR 2b with core and both seqNum and Extension: " << std::endl;
  std::cout << "    -------------- " << std::endl;
  daq::RawFileName fn2b("project_08.00012345.stype_sname.daq.RAW._lb0007._KSK",11,".testdata");
  fn2b.print();
  fn2b.setFileSequenceNumber(8);
  fn2b.print();
  fn2b.setExtension(".mydata");
  fn2b.print();
  fn2b.setExtension(".mydata.extra");
  fn2b.print();
  fn2b.interpretFileName( fn2b.fileName());
  fn2b.print();
  fn2b.interpretFileName( fn2b.fileNameCore());
  fn2b.print();
  
  std::cout << std::endl;
  std::cout << " -- CONSTRUCTOR 3a with a full name: " << std::endl;
  std::cout << "    -------------- " << std::endl;
  daq::RawFileName fn3a("project_08.00012345.stype_sname.daq.RAW._lb0006._KSK._0012.mydata.mywriting");
  fn3a.print();
  try {
    fn3a.setExtension(".mydata");
  } catch (std::exception& ex) {
    std::cout << "Exception in trying to change the file extension: "
              << ex.what() << std::endl;
  }
  fn3a.print();
  fn3a.setFileSequenceNumber(8);
  fn3a.print();
  fn3a.setExtension(".mydata.extra");
  fn3a.print();
  try {
    fn3a.interpretFileName(fn3a.fileName() );
  } catch (std::exception& ex) {
    std::cout << "Exception in interpreting filename given: " 
	      << ex.what() << std::endl;
  }
  fn3a.print();
  try {
    fn3a.interpretFileName(fn3a.fileNameCore() );
  } catch (std::exception& ex) {
    std::cout << "Exception in interpreting filename given: " 
	      << ex.what() << std::endl;
  }
  fn3a.print();
  std::cout << " hasValidCore() = " << fn3a.hasValidCore() << std::endl;
  std::cout << " hasValidTrailer() = " << fn3a.hasValidTrailer() << std::endl;
  std::cout << " run number = " << fn3a.runNumber() << std::endl;
  std::cout << " project = " << fn3a.project() << std::endl;
  std::cout << " datasetName = " << fn3a.datasetName() << std::endl;
  std::cout << " appName = " << fn3a.applicationName() << std::endl;
  std::cout << " lumi = " << fn3a.lumiBlockNumber() << std::endl;
  std::cout << " fileNameCore = " << fn3a.fileNameCore() << std::endl;
  std::cout << " fileName     = " << fn3a.fileName() << std::endl;

  std::cout << std::endl;
  std::cout << " -- CONSTRUCTOR 3b with fileNameCore only: " << std::endl;
  std::cout << "    -------------- " << std::endl;
  daq::RawFileName fn3b("project_08.00012345.stype_lar_emb.daq.RAW._lb0005._KSK");
  fn3b.print();
  fn3b.setFileSequenceNumber(8);
  fn3b.print();
  fn3b.setExtension(".mydata");
  fn3b.print();
  fn3b.setExtension(".mydata.extra");
  fn3b.print();
  fn3b.interpretFileName( fn3b.fileNameCore());
  fn3b.print();
  fn3b.interpretFileName( fn3b.fileName());
  fn3b.print();

  std::cout << std::endl;
  std::cout << " -- CONSTRUCTOR 3c with unknown fileName: " << std::endl;
  std::cout << "    -------------- " << std::endl;
  daq::RawFileName fn3c("project_08.00012345.stype_sname.daq.aRAW.userTag._lb0005._KSK._0012.mydata.mywriting");
  fn3c.print();
  fn3c.setExtension(".mydata");
  fn3c.print();
  fn3c.setFileSequenceNumber(8);
  fn3c.print();
  fn3c.setExtension(".mydata.extra");
  fn3c.print();
  try {
    fn3c.interpretFileName(fn3c.fileName() );
  } catch (std::exception& ex) {
    std::cout << "Exception in interpreting filename given: " 
	      << ex.what() << std::endl;
  }
  fn3c.print();
  try {
    fn3c.interpretFileName(fn3c.fileNameCore() );
  } catch (std::exception& ex) {
    std::cout << "Exception in interpreting filename given: " 
	      << ex.what() << std::endl;
  }
  fn3c.print();
  std::cout << " hasValidCore() = " << fn3c.hasValidCore() << std::endl;
  std::cout << " hasValidTrailer() = " << fn3c.hasValidTrailer() << std::endl;

  std::string my_project;
  try {
    my_project = fn3c.project();
    std::cout << " project = " << my_project << std::endl;  
    std::cout << " run number = " << fn3c.runNumber() << std::endl;
    std::cout << " datasetName = " << fn3c.datasetName() << std::endl;
    std::cout << " appName = " << fn3c.applicationName() << std::endl;
    std::cout << " lumi = " << fn3c.lumiBlockNumber() << std::endl;
    std::cout << " fileNameCore = " << fn3c.fileNameCore() << std::endl;
    std::cout << " fileName     = " << fn3c.fileName() << std::endl;

  } catch (std::exception& ex) {
    std::cout << "Exception in interpreting the given filename given: " 
	      << ex.what() << std::endl;
    my_project = "unknown";
  }


  std::cout << std::endl;
  std::cout << " -- CONSTRUCTOR 3d with unknown fileNameCore: " << std::endl;
  std::cout << "    -------------- " << std::endl;
  daq::RawFileName fn3d("project_08.00012345.stype_sname.daq.aRAW.userTag._lb0005._KSK");
  fn3d.print();
  fn3d.setTailers(8,".mydata");
  fn3d.print();
  fn3d.setExtension(".mydata.extra");
  fn3d.print();
  try {
    fn3d.interpretFileName(fn3d.fileName() );
  } catch (std::exception& ex) {
    std::cout << "Exception in interpreting filename given: " 
	      << ex.what() << std::endl;
  }
  fn3d.print();
  try {
    fn3d.interpretFileName(fn3d.fileNameCore() );
  } catch (std::exception& ex) {
    std::cout << "Exception in interpreting filename given: " 
	      << ex.what() << std::endl;
  }
  fn3d.print();

  std::cout << std::endl;
  std::cout << " -- CONSTRUCTOR 3e with a non-standard fileName: " << std::endl;
  std::cout << "    -------------- " << std::endl;
  daq::RawFileName fn3e("myTestFile");
  fn3e.print();
  fn3e.setTailers(88,".mydata");
  fn3e.print();
  try {
    fn3e.interpretFileName(fn3e.fileName() );
  } catch (std::exception& ex) {
    std::cout << "Exception in interpreting filename given: " 
	      << ex.what() << std::endl;
  }
  fn3e.print();
  try {
    fn3e.interpretFileName(fn3e.fileNameCore() );
  } catch (std::exception& ex) {
    std::cout << "Exception in interpreting filename given: " 
	      << ex.what() << std::endl;
  }
  fn3e.print();
  std::cout << " hasValidCore() = " << fn3e.hasValidCore() << std::endl;
  std::cout << " hasValidTrailer() = " << fn3e.hasValidTrailer() << std::endl;
  try {
    std::cout << " run number = " << fn3e.runNumber() << std::endl;
    std::cout << " project = " << fn3e.project() << std::endl;
    std::cout << " datasetName = " << fn3e.datasetName() << std::endl;
    std::cout << " appName = " << fn3e.applicationName() << std::endl;
    std::cout << " lumi = " << fn3e.lumiBlockNumber() << std::endl;
    std::cout << " fileNameCore = " << fn3e.fileNameCore() << std::endl;
    std::cout << " fileName     = " << fn3e.fileName() << std::endl;
  } 
  catch (std::exception& ex) {
    std::cout << "Exception in interpreting filename given: " 
	      << ex.what() << std::endl;
  }


  std::cout << std::endl;
  std::cout << std::endl;

  std::cout << std::endl;
  std::cout << " -- CONSTRUCTOR 3f with fileNameCore from DataWriter: " << std::endl;
  std::cout << "    -------------- " << std::endl;
  daq::RawFileName fn3f("data_test.00000001.Single_LAR_Stream.daq.RAW._lb0000._testWriting");
  fn3f.print();
  fn3f.setTailers(81,".mydata");
  fn3f.print();
  fn3f.setExtension(".mydata.extra");
  fn3f.print();
  try {
    fn3f.interpretFileName(fn3f.fileName() );
  } catch (std::exception& ex) {
    std::cout << "Exception in interpreting filename given: " 
	      << ex.what() << std::endl;
  }
  fn3f.print();
  try {
    fn3f.interpretFileName(fn3f.fileNameCore() );
  } catch (std::exception& ex) {
    std::cout << "Exception in interpreting filename given: " 
	      << ex.what() << std::endl;
  }
  fn3f.print();

  std::cout << "" << std::endl;
  std::cout << "==============================================" << std::endl;

  return 0;
}
