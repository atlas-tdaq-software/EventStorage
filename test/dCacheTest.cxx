#define SIZE 5000000
#include <stdlib.h>

#include "dcap.h"

int main()
{

   int in_fd, out_fd;
   char data_buffer[SIZE];

   in_fd = dc_open("input.data", O_RDONLY);

   if( in_fd < 0 ) {
      dc_error("fail to open data file");
      exit(1);
   }

   out_fd = dc_creat("output.data", 0644);

   if( out_fd < 0 ) {
      dc_error("fail to open output file");
      dc_close(in_fd);
      exit(2);
   }

   while(1) {
      int n = dc_read(in_fd, data_buffer, SIZE);
      if( n < 0 ) {
         dc_error("failed to read data");
         dc_close(in_fd);
         dc_close(out_fd);
         exit(3);
      }

      if(n == 0) {
         printf("end of data\n");
         break;
      }


      if( dc_write(out_fd, data_buffer, n) < 0 ) {
         dc_error("failed to write data");
         dc_close(in_fd);
         dc_close(out_fd);
         exit(4);
      }

   }

   dc_close(in_fd);
   dc_close(out_fd);
   exit(0);
}
