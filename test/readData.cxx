/**
   \file readData.cxx
   \author Szymon Gadomski
   \brief Read one data file or a sequence and check validity of the file format.
*/


#include <iostream>
#include <fstream>
#include <memory>
#include <string>
#include <stdint.h>
#include <unistd.h>
#include <cstdlib>

#include "EventStorage/pickDataReader.h"     
#include "EventStorage/EventStorageIssues.h"     
// function to get the universal reader

int main (int argc, char *argv[])
{
    using namespace std;

    string fName="";

    string fnm="PreviousDataFile.Name";
    ifstream defParFile(fnm.c_str(),ios::in);

    if(argc>1) {
      fName = argv[1];
    } else {
      bool defDone=false;

      defParFile >> fName; 
      if(fName!="") {
	defDone = true;
	cout << "Got previous file name from PreviousDataFile.Name\n";
	defParFile.close();
      } else {
 	cout << "File named PreviousDataFile.Name not found.\n";
	cout << "It will be created if and when a data file is opened by this program\n"
		  << "(if only this directory is writable for you).\n";

	cout << "You can also give the file name in the command line\n";
	cout << "> readData MyPath/MyDataFileName\n";
       }

      // get input parameters
      string inpar="";
      cout << "Where is the data? Please specify the file name with path. \n";
      if(defDone) cout << "(or enter \"y\" to accept the default \n" 
			    << fName << ")\n";
      cin >> inpar;
      if(inpar != "y") fName = inpar;
    }

    cout << "Get DataReader for file " << fName << endl;
    DataReader *pDR = pickDataReader(fName);

    cout << "pickDataReader returned with pointer " << pDR << endl << flush;
    
    if(!pDR) {
      cout << "Problem opening or reading this file!\n"<<flush;
      return -1;
    }

    cout << "Trying to read file " << pDR->fileName() << endl;
    if(!pDR->good()) {
      cout << "There is a problem!" << endl;
      return -1;
    } else {
      string fnm="PreviousDataFile.new";
      ofstream newParFile(fnm.c_str(),ios::out);
      newParFile << fName << endl; 
      newParFile.close();
      system("mv PreviousDataFile.new PreviousDataFile.Name");
    }

    cout << "DataReader setup OK!\n";

    // This info (for instance) is available without penalty at start time.
    cout << "File start time " << pDR->fileStartTime() << endl;
    cout << "File start date " << pDR->fileStartDate() << endl;

    // Getting this info will cost a rewind to the end of file and back."
    
    try
    {
	    cout << "There are " << pDR->eventsInFile() << " events in this file.\n";
  	  cout << "There are " << pDR->dataMB_InFile() << " MB in this file.\n";
		}
		catch(...)
		{
  	  cout << "PANIIIIIICCC.\n";
		}


    cout << " LumiBlock: " << pDR->lumiblockNumber() << std::endl;

    cout << " Project Tag: " << pDR->projectTag() << std::endl;
    cout << " Stream: " << pDR->stream() << std::endl;


    cout << " App Name: " << pDR->appName() << std::endl;
    cout << " Core Name: " << pDR->fileNameCore() << std::endl;

		cout << "GUID " <<  pDR->GUID() << endl;


		std::vector<std::string> MD = pDR->freeMetaDataStrings();

		cout << "MetaData" << endl;
	
		for (size_t z = 0 ; z< MD.size(); z++)
			cout <<  MD.at(z) << endl;
		

    cout << "Write an index file? (y/n):";
    string inp("");
    cin >> inp; 

    ofstream indexFile; bool indexChoice(false);
    if(inp=="y") {
      cout << "Open index file in the current directory.\n";
      indexFile.open("reading.index",ios::out);
      indexChoice=true;
    } else {
      cout << "Do not write an index file.\n";
    } 

    bool seqRead=false;
    if(!indexChoice) {
      cout << "Read a sequence of files? (y/n):"; // <<endl;
      string inp("");
      cin >> inp;

      if("y" == inp) {
	cout << "Activate sequence reading."<<endl;
	pDR->enableSequenceReading();
	seqRead=true;
      }
    }
    
    unsigned int eventCounter=0, dumpCounter=0;
    while(pDR->good()) 
    {
      unsigned int eventSize;    
      char *buf;

      if(indexChoice) 
      {
				int64_t pos=pDR->getPosition();
				indexFile << eventCounter << " " << pos << endl;
      }

			buf = new char[40000000];

			//file number must be asked before getting data, because at the end of a file, 
			//DataReader already jumps to next file
			int currentfilenumner = pDR->currentFileNumber();
		
			//char *buf;
			DRError ecode = DRNOOK;
			try
			{
				ecode = pDR->getDataPreAlloced(eventSize,&buf, 40000000);
			}
			catch (EventStorage::ES_NoEndOfFileRecord &e)
			{
				cout << "Recovering from exception: This file has no End OF File Record" << std::endl;;
			}
			catch (EventStorage::ES_WrongEventSize &e)
			{
				cout << " --> Found truncated event in between, continuing " << std::endl;;
			}	
			catch (EventStorage::ES_OutOfFileBoundary &e)
			{
				cout << " --> Found truncated event at the end, continuing " << std::endl;;
			}	

			
      //DRError ecode = pDR->getData(eventSize,&buf, -1, true, 10000000);
      //DRError ecode = pDR->getData(eventSize,&buf);


      while(DRWAIT == ecode) // wait for more data to come 
      { 
				dumpCounter = 0;
				usleep(500000);
				cout << "Waiting for more data.\n" << flush;
				ecode = pDR->getData(eventSize,&buf);
      }

      if(DRNOOK == ecode) 
      {
					cout << "Not expecting any more data. Exit.\n" << flush;
					break; 
      }


      ++eventCounter; ++dumpCounter;
      
      if((dumpCounter<20) || !(dumpCounter % 100)) 
      { 
				cout << "Event at counter "<< eventCounter;
				cout << " from file " << currentfilenumner;
				cout << " has size " << eventSize;
				cout << " bytes." << endl;
      }

      if(pDR->endOfFile()) 
      {
				if(seqRead) 
				{
	  			cout << "Data file number "
	       	<< currentfilenumner
	       	<< " has ended."<<endl<<flush;
				} 
				else 
				{
	  				cout << "Data file has ended."<<endl<<flush;
				}
      }
      delete [] buf;
    }
    indexFile.close();

    if(pDR->endOfFileSequence()) 
    {
      cout << "Found end of file sequence.\n";
    } 

    cout << "Retrived " << eventCounter << " events.\n";
    cout << "Last one has number " << pDR->latestDataNumber() << "." << endl;

		try
		{

    cout << "Events in file sequence up to this file " 
	      << pDR->eventsInFileSequence() << endl;
    cout << "MB in file sequence up to this file " 
	      << pDR->dataMB_InFileSequence() << endl;
	      
	      
    	cout << "File end time " << pDR->fileEndTime() << endl;
    	cout << "File end date " << pDR->fileEndDate() << endl;
		}
		catch(EventStorage::ES_NoEndOfFileRecord &e)
		{
				cout << " --> Found no end of file record " << std::endl;;
		}

    delete pDR;
    cout << "Reader deleted. The end.\n\n";

}
