//Dear emacs, this is -*- c++ -*-

/**
 * @file write_test.cxx
 * @author <a href="mailto:Wainer.Vandelli@cern.ch">Wainer.Vandelli@cern.ch</a> 
 * $Author: $
 * $Revision: $
 * $Date: $
 * 
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE Write Tests
#define BOOST_TEST_MAIN

#include <boost/test/unit_test.hpp>

#include <stdio.h>
#include <strings.h>

#include "EventStorage/DataWriter.h"
#include "EventStorage/SimpleFileName.h"
#include "EventStorage/ESCompression.h"
#include <zlib.h>

#include <iostream>

#include "./test_common.h"
#include "compression/compression.h"
#include "compression/DataBuffer.h"


BOOST_AUTO_TEST_CASE( write_file_free_name )
{
  std::string fname = "test.data";

  /* Remove file to avoid issues */
  ::remove(fname.c_str());

  EventStorage::run_parameters_record rPar;
  std::vector<std::string> fmds;
  std::string project;
  std::string streamtype; 
  std::string streamname;
  unsigned int lb;
  std::string app;

  get_common_params(rPar ,fmds, project, streamtype, streamname, lb, app);

  EventStorage::DataWriter * dw = new
    EventStorage::DataWriter(".",
			     fname,
			     rPar,
			     project,
			     streamtype, 
			     streamname,
			     streamtype+"_"+streamname,
			     lb,
			     app,fmds);

  BOOST_CHECK(dw->good());

  unsigned int nevents = 0;
  unsigned char *event = new unsigned char[size];
  
  for(unsigned int i=0; i<(size/sizeof(uint32_t)); i++){
    ((uint32_t*)event)[i] = COMPDATA;
  }

  //Write a single buffer
  EventStorage::DWError err = dw->putData(size,event);
  nevents++;

  BOOST_CHECK_EQUAL(err, EventStorage::DWOK);
  BOOST_CHECK(dw->good());
  BOOST_CHECK_EQUAL(nevents,dw->eventsInFile());

  struct iovec * iov = new struct iovec[2];

  iov[0].iov_base = event;
  iov[0].iov_len = size/2;
  iov[1].iov_base = event+size/2;
  iov[1].iov_len = size/2;

  //Write an iov
  err = dw->putData(2,iov);
  nevents++;

  BOOST_CHECK_EQUAL(err, EventStorage::DWOK);
  BOOST_CHECK(dw->good());
  BOOST_CHECK_EQUAL(nevents,dw->eventsInFile());

  //Check returned event size
  unsigned int todisk =0;
  err = dw->putData(size,event,todisk);
  nevents++;

  BOOST_CHECK_EQUAL(err, EventStorage::DWOK);
  BOOST_CHECK(dw->good());
  BOOST_CHECK_EQUAL(nevents,dw->eventsInFile());
  BOOST_CHECK_EQUAL(size,todisk);


  delete dw;
  delete[] event;
  delete[] iov;
}

BOOST_AUTO_TEST_CASE( write_file_compressed )
{
  std::string fname = "test-comp.data";

  /* Remove file to avoid issues */
  ::remove(fname.c_str());

  
  EventStorage::run_parameters_record rPar;
  std::vector<std::string> fmds;
  std::string project;
  std::string streamtype; 
  std::string streamname;
  unsigned int lb;
  std::string app;

  get_common_params(rPar ,fmds, project, streamtype, streamname, lb, app);
    
  EventStorage::DataWriter * dw = new
    EventStorage::DataWriter(".",
			     fname,
			     rPar,
			     project,
			     streamtype, 
			     streamname,
			     streamtype+"_"+streamname,
			     lb,
			     app,fmds,
			     EventStorage::ZLIB);

  BOOST_CHECK(dw->good());

  unsigned int nevents = 0;
  unsigned char *event = new unsigned char[size];
  
  uint32_t * ev = (uint32_t*) event;

  for(unsigned int i=0; i< (size/4); ++i){
    ev[i] = COMPDATA;
  }

  EventStorage::DWError err;
  
  //Write a single buffer
  err = dw->putData(size,event);
  nevents++;

  BOOST_CHECK_EQUAL(err, EventStorage::DWOK);
  BOOST_CHECK(dw->good());
  BOOST_CHECK_EQUAL(nevents,dw->eventsInFile());
  
  struct iovec * iov = new struct iovec[2];

  iov[0].iov_base = event;
  iov[0].iov_len = size/2;
  iov[1].iov_base = event+size/2;
  iov[1].iov_len = size/2;
  

  //Write an iov
  err = dw->putData(2,iov);
  nevents++;
  
  BOOST_CHECK_EQUAL(err, EventStorage::DWOK);
  BOOST_CHECK(dw->good());
  BOOST_CHECK_EQUAL(nevents,dw->eventsInFile());
 
  //Check returned event size
  unsigned int todisk =0;
  err = dw->putData(size,event,todisk);
  nevents++;

  BOOST_CHECK_EQUAL(err, EventStorage::DWOK);
  BOOST_CHECK(dw->good());
  BOOST_CHECK_EQUAL(nevents,dw->eventsInFile());
  BOOST_CHECK((size > todisk));
  BOOST_CHECK_EQUAL(0,todisk%sizeof(uint32_t));

  //put pre-compressed data
  unsigned long int outsize = size*1.1+12;
  unsigned char * outbuf = new unsigned char[outsize];

  ::compress2((Bytef *)outbuf, &outsize, 
	      (const Bytef *)event, size, 1);
  //pad at 4 byte alignment
  uint32_t pad = sizeof(uint32_t)-outsize%(sizeof(uint32_t));
  if(pad>0 && pad < sizeof(uint32_t)){
    ::bzero(outbuf+outsize,pad);
    outsize += pad;
  }

  BOOST_CHECK_EQUAL(todisk,outsize);
  
  err = dw->putPrecompressedData(outsize,outbuf);
  nevents++;

  BOOST_CHECK_EQUAL(err, EventStorage::DWOK);
  BOOST_CHECK(dw->good());
  BOOST_CHECK_EQUAL(nevents,dw->eventsInFile());
 

  uint32_t compsize = 0;
  uint32_t iovsize = 2;
  compression::DataBuffer comp(10);

  compression::zlibcompress(comp,
			    compsize,
			    iovsize, iov,
			    size,1);
  
  //Manually compressed and internally compressed
  //should have the same size
  BOOST_CHECK_EQUAL(outsize, compsize);
    
  err = dw->putPrecompressedData(compsize,comp.handle());
  nevents++;

  BOOST_CHECK_EQUAL(err, EventStorage::DWOK);
  BOOST_CHECK(dw->good());
  BOOST_CHECK_EQUAL(nevents,dw->eventsInFile());
 
  
  struct iovec compiov;

  compiov.iov_base = comp.handle();
  compiov.iov_len = compsize;

  err = dw->putPrecompressedData(1, &compiov);
  nevents++;

  BOOST_CHECK_EQUAL(err, EventStorage::DWOK);
  BOOST_CHECK(dw->good());
  BOOST_CHECK_EQUAL(nevents,dw->eventsInFile());

  delete[] iov;
  delete[] outbuf;
  delete dw;
  delete[] event;
}

BOOST_AUTO_TEST_CASE( write_file_raw_name_many )
{

  
  EventStorage::run_parameters_record rPar;
  std::vector<std::string> fmds;
  std::string project;
  std::string streamtype; 
  std::string streamname;
  unsigned int lb;
  std::string app;

  get_common_params(rPar ,fmds, project, streamtype, streamname, lb, app);


  boost::shared_ptr<daq::RawFileName> 
    fname(new daq::RawFileName(project,rPar.run_number,streamtype,
			       streamname,lb,app));

  
  EventStorage::DataWriter * dw = new
    EventStorage::DataWriter(".",
			     fname,
			     rPar,
			     project,
			     streamtype, 
			     streamname,
			     streamtype+"_"+streamname,
			     lb,
			     app,fmds);
  
  BOOST_CHECK(dw->good());

  std::string f1 = fname->getCurrentFileName(false);
  BOOST_CHECK_EQUAL(fname->getIndex(),1);
  
  dw->closeFile();
  
  BOOST_CHECK(dw->good());

  unsigned char *event = new unsigned char[size];

  //This will open a new file
  EventStorage::DWError err = dw->putData(size,event);
  BOOST_CHECK_EQUAL(err, EventStorage::DWOK);
  BOOST_CHECK(dw->good());
  BOOST_CHECK_EQUAL(fname->getIndex(),2);

  std::string f2 = fname->getCurrentFileName(false);

  dw->nextFile();

  /* Remove file to avoid issues */
  BOOST_CHECK_EQUAL(::remove(f1.c_str()),0);
  BOOST_CHECK_EQUAL(::remove(f2.c_str()),0);
  
  std::string f3 = fname->getCurrentFileName(false);

  delete dw;
  delete[] event;

  ::remove(f3.c_str());
}


BOOST_AUTO_TEST_CASE( write_file_free_name_only_one )
{
  std::string fname = "test-one.data";

  /* Remove file to avoid issues */
  ::remove(fname.c_str());

  
  EventStorage::run_parameters_record rPar;
  std::vector<std::string> fmds;
  std::string project;
  std::string streamtype; 
  std::string streamname;
  unsigned int lb;
  std::string app;

  get_common_params(rPar ,fmds, project, streamtype, streamname, lb, app);
 
    
  EventStorage::DataWriter * dw = new
    EventStorage::DataWriter(".",
			     fname,
			     rPar,
			     project,
			     streamtype, 
			     streamname,
			     streamtype+"_"+streamname,
			     lb,
			     app,fmds);
  
  BOOST_CHECK(dw->good());

  dw->closeFile();
  
  BOOST_CHECK(dw->good());

  unsigned char *event = new unsigned char[size];

  //This will try opening a new file. it should fail
  //throwing 
  BOOST_CHECK_THROW(dw->putData(size,event),EventStorage::ES_SingleFileAlreadyExists);
}

BOOST_AUTO_TEST_CASE( write_file_cd )
{

  
  EventStorage::run_parameters_record rPar;
  std::vector<std::string> fmds;
  std::string project;
  std::string streamtype; 
  std::string streamname;
  unsigned int lb;
  std::string app;

  get_common_params(rPar ,fmds, project, streamtype, streamname, lb, app);
  
  std::string dir1 = ".";
  
  
  boost::shared_ptr<daq::RawFileName> 
    fname(new daq::RawFileName(project,rPar.run_number,streamtype,
			       streamname,lb,app));

  
  EventStorage::DataWriter * dw = new
    EventStorage::DataWriter(dir1,
			     fname,
			     rPar,
			     project,
			     streamtype, 
			     streamname,
			     streamtype+"_"+streamname,
			     lb,
			     app,fmds);
  
  BOOST_CHECK(dw->good());

  std::string f1 = fname->getCurrentFileName(false);

  std::string dir2 = "/tmp";
  dw->cd(dir2);
  
  dw->nextFile();
  
  std::string f2 = fname->getCurrentFileName(false);

  delete dw;

  BOOST_CHECK_EQUAL(::remove((dir1+"/"+f1).c_str()),0);
  BOOST_CHECK_EQUAL(::remove((dir2+"/"+f2).c_str()),0);
}
