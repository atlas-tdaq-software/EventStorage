/**
   \file readMetaData.cxx
   \author Szymon Gadomski
   \brief Open a data file and print some metadata from it.
*/

#include <iostream>
#include <fstream>
#include <memory>
#include <string>
#include <unistd.h>

#include "EventStorage/pickDataReader.h"     // universal reader


int main (int argc, char *argv[])
{
  using namespace std;
  DataReader *pDR = 0;

  if(argc>1) {
    string fName = argv[1];
    pDR = pickDataReader(fName);
  } 

  if(!pDR) {
    cout << "Problem opening or reading the file!\n";
    return -1;
  }

  cout << "Trying to read file " << pDR->fileName() << endl;
  if(!pDR->good()) {
    cout << "There is a problem!" << endl;
    return -1;
  }

  cout << "DataReader setup OK!\n";

  // This info (for instance) is available without penalty at start time.
  cout << "File start time " << pDR->fileStartTime() << endl;
  cout << "File start date " << pDR->fileStartDate() << endl;

  // Getting this info will cost a rewind to the end of file and back."
  cout << "There are " << pDR->eventsInFile() << " events in this file.\n";
  cout << "There are " << pDR->dataMB_InFile() << " MB in this file.\n";

  // Optional meta data in free strings
  vector<string> fmds =  pDR->freeMetaDataStrings();
  cout << "Found " << fmds.size()
	    << " strings with meta data.\n";

  vector<string>::iterator it;
  unsigned int i=0;
  for(it=fmds.begin(); it!=fmds.end(); ++it) 
    cout << "String "<<++i<<" <"<<*it<<">\n";

  string guid=pDR->GUID();
  cout << "GUID: "<<guid<<endl;
    
  delete pDR;
  cout << "Reader deleted. The end.\n\n";
}
