/**
   \file testReadingSpeed.cxx
   \author Szymon Gadomski
   \brief Read a sequence of files and measure how fast we can read data.
*/

#include <iostream>
#include <fstream>
#include <memory>
#include <string>
#include <cstdlib> 
#include <chrono>

#include <unistd.h>

#include "EventStorage/pickDataReader.h"     
// function to get the universal reader

int main (int argc, char *argv[])
{
    std::string fName="";

    std::string fnm="PreviousDataFile.Name";
    std::ifstream defParFile(fnm.c_str(),std::ios::in);

    if(argc>1) {
      fName = argv[1];
    } else {
      bool defDone=false;

      defParFile >> fName; 
      if(fName!="") {
	defDone = true;
	std::cout << "Got previous file name from PreviousDataFile.Name\n";
	defParFile.close();
      } else {
	std::cout << "File named PreviousDataFile.Name not found.\n";
	std::cout << "It will be created if and when a data file is opened by this program\n"
		  << "(if only this directory is writable for you).\n";

	std::cout << "You can also give the file name in the command line\n";
	std::cout << "> testReadingSpeed MyPath/MyDataFileName\n";
      }

      // get input parameters
      std::string inpar="";
      std::cout << "Where is the data? Please specify the file name with path. \n";
      if(defDone) std::cout << "(or enter \"y\" to accept the default \n" 
			    << fName << ")\n";
      std::cin >> inpar;
      if(inpar != "y") fName = inpar;
    }

    std::cout << "Open file " << fName << std::endl;
    DataReader *pDR = pickDataReader(fName);
    
    if(!pDR) {
      std::cout << "Problem opening or reading this file!\n";
      return -1;
    }

    std::cout << "Trying to read file " << pDR->fileName() << std::endl;
    if(!pDR->good()) {
      std::cout << "There is a problem!" << std::endl;
      return -1;
    } else {
      std::string fnm="PreviousDataFile.new";
      std::ofstream newParFile(fnm.c_str(),std::ios::out);
      newParFile << fName << std::endl; 
      newParFile.close();
      system("mv PreviousDataFile.new PreviousDataFile.Name");
    }

    std::cout << "DataReader setup OK!\n";

    // Be careful. The ststisics below may require stanging all files.
    // Asking the same after you processed all the run is for free.
    // std::cout << " Events in run " << pDR->eventsInRun() << std::endl; 
    // std::cout << " MB in run " << pDR->dataMB_InRun() << std::endl;
    
    // This info (for instance) is available without penalty at start time.
    std::cout << "File start time " << pDR->fileStartTime() << std::endl;
    std::cout << "File start date " << pDR->fileStartDate() << std::endl;

    std::cout << "How often to measure speed? Interval in seconds (<0 means no measurement):";   
    double tmeas=0.0;
    std::cin >> tmeas;
    if(tmeas<=0.0) { 
      tmeas = 0.0;
      std::cout << "No measurements of speed.\n";
    } else {
      std::cout << "Intermediate measuerement every " << tmeas << " seconds\n";
    }

    // get reference start time 
    // synchronize measurements with writing
    double writingStartTimeSec=0; 
    std::ifstream tstartFile("StartTime.dat",std::ios::in);
    tstartFile >> writingStartTimeSec;
    tstartFile.close();

    std::cout << "Print event info? (y/n)" << std::endl;
    std::string ans="";
    std::cin >> ans;
    bool printInfo=false;
    if(ans=="y") printInfo=true;

    auto startTime = std::chrono::high_resolution_clock::now();
    auto previousTime = startTime;
    long unsigned int dataCounter = 0, previousDataCounter = 0;
    unsigned int previousEventCount = 0;
    if(writingStartTimeSec==0) 
      writingStartTimeSec=startTime.time_since_epoch().count();

    std::cout << "Starting the reading loop!\n";
 
    unsigned int eventCounter=0, dumpCounter=0;
    while(pDR->good()) 
    {
      unsigned int eventSize;    
      char *buf;
	
      DRError ecode = pDR->getData(eventSize,&buf);

      while(DRWAIT == ecode) // wait for more data to come 
      { 
	dumpCounter = 0;
	usleep(5000000);
	std::cout << "Waiting for more data.\n" << std::flush;
	ecode = pDR->getData(eventSize,&buf);
	auto now = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> tdiff = now - previousTime;
	double tds = tdiff.count();
	std::cout << "Time " << now.time_since_epoch().count()-writingStartTimeSec;
	std::cout << " interval " << tds << " s,";
	std::cout << " event " <<  eventCounter;
	std::cout << " data " <<  dataCounter/1024;
	std::cout << " current rate is " << 0.0 << " MB/s or " << 0.0 << " Hz.\n"; 	
	previousTime = now; 
      }

      if(DRNOOK == ecode) {
	std::cout << "Not expecting any more data. Exit.\n" << std::flush;
	break; 
      }

      ++eventCounter; ++dumpCounter;
      dataCounter+=eventSize/1024;
      
      if(printInfo && ((dumpCounter<20) || !(dumpCounter % 100))) { 
	std::cout << "Event " << pDR->latestDataNumber();
	std::cout << " from file " << pDR->currentFileNumber();
	std::cout << " has size " << eventSize;
	std::cout << " bytes." << std::endl;
      }

      auto now = std::chrono::high_resolution_clock::now();
      std::chrono::duration<double> tdiff = now - previousTime;
       
      if(tmeas > 0 && tdiff.count() > tmeas) {
	double tds = tdiff.count();
	double mbps = (dataCounter-previousDataCounter)/(1024*tds);
	double evps = (eventCounter-previousEventCount)/tds;
	std::cout << "Time " << now.time_since_epoch().count()-writingStartTimeSec;
	std::cout << " interval " << tds << " s,";
	std::cout << " event " <<  eventCounter;
	std::cout << " data " <<  dataCounter/1024;
	std::cout << " current rate is " << mbps << " MB/s or " << evps << " Hz.\n"; 
	previousTime = now; 
	previousEventCount = eventCounter;
	previousDataCounter = dataCounter;
      }

      if(pDR->endOfFile()) {
	std::cout << "Data file number "
		  << pDR->currentFileNumber()
		  << " has ended." 
		  << std::endl << std::flush;	
      }
      delete [] buf;
    }

    auto now = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> tdiff = now - startTime;
    double tds = tdiff.count();
    std::cout << "Time of this run in seconds is " << tds << std::endl;
    double mb = dataCounter/1024;
    std::cout << "Total data read is "<< mb << " MB or " << eventCounter << " events.\n";
    std::cout << "Average rate is "<< mb/tds << " MB/s";
    double evhz=eventCounter; evhz /= tds;
    std::cout << " or "<<evhz<<" Hz."<<std::endl;

    if(pDR->endOfFileSequence()) {
      std::cout << "Found end of file sequence.\n";
    }

    std::cout << "Retrived " << eventCounter << " events.\n";
    std::cout << "Last one has number " << pDR->latestDataNumber() << "." << std::endl;

    std::cout << "Events in file sequence up to this file " 
	      << pDR->eventsInFileSequence() << std::endl;
    //std::cout << "MB in file sequence up to this file " 
	  //    << pDR->dataMB_InFileSequence() << std::endl;
    std::cout << "File end time " << pDR->fileEndTime() << std::endl;
    std::cout << "File end date " << pDR->fileEndDate() << std::endl;
    

    delete pDR;
    std::cout << "Reader deleted. The end.\n\n";

}
