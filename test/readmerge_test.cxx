//Dear emacs, this is -*- c++ -*-

/**
 * @file readmerge_test.cxx
 * @author <a href="mailto:Wainer.Vandelli@cern.ch">Wainer.Vandelli@cern.ch</a> 
 * $Author: $
 * $Revision: $
 * $Date: $
 * 
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE ReadMerge Tests
#define BOOST_TEST_MAIN

#include <iostream>
#include <algorithm>

#include <boost/test/unit_test.hpp>
#include "EventStorage/pickDataReader.h"
#include "EventStorage/EventStorageIssues.h"

#include "./test_common.h"


void basic_read_test(std::string fname, 
		     EventStorage::CompressionType comptype){
  
  DataReader *pDR = pickDataReader(fname);
  
  BOOST_CHECK(pDR);
  BOOST_CHECK(pDR->good());
  
  check_merge_metadata(pDR);

  BOOST_CHECK_EQUAL(pDR->compression(), comptype);
    
  std::vector<char *> events;
  std::vector<uint32_t> sizes;

  while(pDR->good()){
    uint32_t eventSize;
    char * buf;
    
    DRError err = pDR->getData(eventSize,&buf);
    BOOST_CHECK_EQUAL(err, EventStorage::DROK);

    events.push_back(buf);
    sizes.push_back(eventSize);
  }
  
  std::vector<uint32_t>::const_iterator sizeit = sizes.begin();
  
  while(sizeit != sizes.end()){
    
    BOOST_CHECK_EQUAL(*sizeit,size);
    ++sizeit;
  }

  std::vector<char *>::const_iterator evit = events.begin();
  
  while(evit != events.end()){

    uint32_t * p = (uint32_t*)(*evit);
    for(unsigned int i=0; i<(*sizes.begin())/4; ++i){
      BOOST_CHECK_EQUAL(COMPDATA,p[i]);
    }
    delete[] *evit;
    ++evit;
  }
  
  delete pDR;
}


void check_nevent(std::vector<std::string> org, std::string fname){

  unsigned int nevent_orig = 0;

  std::vector<std::string>::const_iterator it = org.begin();
  while(it != org.end()){
    DataReader *pDR = pickDataReader(*it);

    nevent_orig += pDR->eventsInFile();

    delete pDR;
    ++it;
  }

  DataReader * pDR = pickDataReader(fname);

  unsigned int nevent = pDR->eventsInFile();

  delete pDR;

  BOOST_CHECK_EQUAL(nevent_orig, nevent);
}


BOOST_AUTO_TEST_CASE( read_file_merged )
{
  
  std::string fname = "merge._0001.data";
  
  basic_read_test(fname, EventStorage::NONE);

}

BOOST_AUTO_TEST_CASE( read_file_merged_comp )
{
  
  std::string fname = "merge-comp._0001.data";
  
  basic_read_test(fname, EventStorage::ZLIB);
}

BOOST_AUTO_TEST_CASE( read_file_merged_mix1 )
{
  
  std::string fname = "merge-mix1._0001.data";
  
  basic_read_test(fname, EventStorage::NONE);
}


BOOST_AUTO_TEST_CASE( read_file_merged_mix2 )
{

  std::string fname = "merge-mix2._0001.data";
  
  basic_read_test(fname, EventStorage::ZLIB);
}


BOOST_AUTO_TEST_CASE( read_file_merged_nevents ){

  std::vector<std::string> org;
  org.push_back("test.data");
  org.push_back("test.data");

  std::string fname = "merge._0001.data";

  check_nevent(org,fname);

}


BOOST_AUTO_TEST_CASE( read_file_merged_comp_nevents ){

  std::vector<std::string> org;
  org.push_back("test-comp.data");
  org.push_back("test-comp.data");

  std::string fname = "merge-comp._0001.data";

  check_nevent(org,fname);

}



BOOST_AUTO_TEST_CASE( read_file_merged_mix1_nevents ){

  std::vector<std::string> org;
  org.push_back("test.data");
  org.push_back("test-comp.data");

  std::string fname = "merge-mix1._0001.data";

  check_nevent(org,fname);

}


BOOST_AUTO_TEST_CASE( read_file_merged_mix2_nevents ){

  std::vector<std::string> org;
  org.push_back("test.data");
  org.push_back("test-comp.data");

  std::string fname = "merge-mix2._0001.data";

  check_nevent(org,fname);

}


BOOST_AUTO_TEST_CASE( read_file_merged_random_offset )
{

  std::string fname = "merge-mix2._0001.data";
  
  DataReader *pDR = pickDataReader(fname);
  
  BOOST_CHECK(pDR);
  BOOST_CHECK(pDR->good());
  
  check_merge_metadata(pDR);

  std::vector<int64_t> offsets;

  while(pDR->good()){
    uint32_t eventSize;
    char * buf;
    
    offsets.push_back(pDR->getPosition());
    DRError err = pDR->getData(eventSize,&buf);
    BOOST_CHECK_EQUAL(err, EventStorage::DROK);
    delete[] buf;
  }

  delete pDR;

  pDR = pickDataReader(fname);
  
  //Randomize offsets
  std::random_shuffle(offsets.begin(),offsets.end());

  for(const auto &offset: offsets){
    char * buf;
    uint32_t eventSize;
    DRError err = pDR->getData(eventSize,&buf, offset);
    BOOST_CHECK_EQUAL(err, EventStorage::DROK);
    
    uint32_t * p = (uint32_t*)(buf);
    for(unsigned int i=0; i<(eventSize)/4; ++i){
      BOOST_CHECK_EQUAL(COMPDATA,p[i]);
    }
    delete[] buf;
  }

  delete pDR;
}
