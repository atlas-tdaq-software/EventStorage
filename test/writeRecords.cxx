
#include <iostream>
#include <fstream>
#include "EventStorage/EventStorageRecords.h"

std::ofstream ofs;

void file_record(void *ri, const void *pi) {

  uint32_t *record = (uint32_t *)ri;
  uint32_t *pattern = (uint32_t *)pi;
  int size=pattern[1];

  //std::cout << "\nwrite record size = " << size << " words." << std::endl;
  for(int i=0; i<size; i++) {
    if(pattern[i] != 0) {
      //std::cout << "pattern " << pattern[i] << std::endl;
      ofs.write((char *)(pattern+i),sizeof(uint32_t));
    } else {
      //std::cout << "record " << record[i] << std::endl;
      ofs.write((char *)(record+i),sizeof(uint32_t));
    }
  }
}

void file_record(std::string is) {
  
  const char *cs = is.c_str();

  uint32_t size = is.size();
  //std::cout << "\nwrite string "<< is << " size = " << size << " bytes ";

  ofs.write((char *)(&EventStorage::single_string_marker),sizeof(uint32_t));
  ofs.write((char *)(&size),sizeof(uint32_t));
  ofs.write(cs,size);

  char ns = size % 4;
  //std::cout << "with padding " << 4-ns << std::endl;
  if(ns) ofs.write("    ",4-ns);

}

void file_record(EventStorage::file_name_strings nst) {
  
  const char *cName = nst.appName.c_str();
  const char *cTag = nst.fileNameTag.c_str();

  uint32_t sizeName =  nst.appName.size();
  uint32_t sizeTag =  nst.fileNameTag.size();
  //std::cout << "\nwrite string "<< is << " size = " << size << " bytes ";

  ofs.write((char *)(&EventStorage::file_name_strings_marker),sizeof(uint32_t));

  ofs.write((char *)(&sizeName),sizeof(uint32_t));
  ofs.write(cName,sizeName);

  char ns = sizeName % 4;
  if(ns) ofs.write("    ",4-ns);

  ofs.write((char *)(&sizeTag),sizeof(uint32_t));
  ofs.write(cTag,sizeTag);

  ns = sizeTag % 4;
  if(ns) ofs.write("    ",4-ns);

}

int main ()
{
    using namespace EventStorage;

    file_start_record rstart;

    //std::cout << "file_start_pattern.marker " << EventStorage::file_start_pattern.marker << std::endl;

    ofs.open("EventStorage.test_records.data");

    rstart.file_number=17;
    rstart.date=2005;
    rstart.time=1604;
    rstart.sizeLimit_dataBlocks=5000000;
    rstart.sizeLimit_MB=500;

    file_record(&rstart,&file_start_pattern);

    file_name_strings nst;
    nst.appName = "myApp-1"; nst.fileNameTag = "testRecords";
    file_record(nst);

    run_parameters_record rpar = {0,0,1001,50000000,1,111,222,211,180};
    data_separator_record rev = {0,0,12062004,947};
    file_end_record rend = {0,0,12062004,948,1,1024,2,2048,17,0};
    std::string tag("testStringStorage");

    file_record(&rpar,&run_parameters_pattern);
    file_record(tag);
    file_record(&rev,&data_separator_pattern);
    file_record(&rend,&file_end_pattern);

    ofs.close();

    std::cout << "The records written:\n";
    std::cout << string_record(&rstart,&file_start_pattern) << std::endl;
    std::cout << string_record(nst) << std::endl;
    std::cout << string_record(&rpar,&run_parameters_pattern) << std::endl;
    std::cout << string_record(tag) << std::endl;
    std::cout << string_record(&rev,&data_separator_pattern) << std::endl;
    std::cout << string_record(&rend,&file_end_pattern) << std::endl;
    
}
