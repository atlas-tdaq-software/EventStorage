/**
   \file testWritingSpeed.cxx
   \author Szymon Gadomski
   \brief Write a sequence of files and measure disk writing speed.
*/

#include <iostream>
#include <string>
#include <iomanip>
#include <boost/shared_ptr.hpp>
#include <chrono>

#include "EventStorage/DataWriter.h"
#include "EventStorage/RawFileName.h"
#include "EventStorage/EventStorageRecords.h"
#include "EventStorage/DWCBcout.h"
#include <stdint.h>
#include <unistd.h>

int main ()
{
    using namespace std;
    using namespace EventStorage;

    cout << "Please name directory to write: ";
    string writePath;
    cin >> writePath;

    unsigned int runNumber=1;
    string StreamType("Single");
    string StreamName("Stream");  
    unsigned int LumiBlockNumber=0;
    string apName("testWriting");
    string project("data_test");

    boost::shared_ptr<daq::RawFileName> my_fname( new daq::RawFileName(project,runNumber,StreamType,
			      StreamName,LumiBlockNumber,apName));
    // daq::RawFileName my_fname("testFile");

    string fName = my_fname->fileNameCore();

    cout << "Open file "<<fName<<"\n in "<<writePath<<endl;

    run_parameters_record rPar;
        
    rPar.run_number = runNumber;     
    rPar.max_events = 0;     
    rPar.rec_enable = 1;     
    rPar.trigger_type = 0;   
    rPar.detector_mask_LS = 0x12345678abcdefcdLL;  
    rPar.detector_mask_MS = 0x12345678abcdefcdLL;  
    rPar.beam_type = 211;      
    rPar.beam_energy = 220;    
 
    //cout << "rPar = \n" << flush;
    //string rps=string_record(&rPar,&run_parameters_pattern);
    //cout << rps << endl;

    //EventStorage::freeMetaDataStrings fmds;
    std::vector<std::string> fmds;
    //fmds.push_back("Test=metadatastrings.");
    //fmds.push_back("tag=value");
    //fmds.push_back("key=125");
    fmds.push_back("exampleMetaData=Value");
    fmds.push_back("key=ValueValue");
    cout << "freeMetaDataStrings with size " <<fmds.size() <<endl;

    cout << "Constructing the DataWriter. " << flush;

    
    
    cout << "Using the new Constructor " << endl;
    
    //This is the prefered way to do it for TDAQ online
    DataWriter dw(writePath,my_fname,rPar,project,StreamType,StreamName,my_fname->stream(),LumiBlockNumber,apName,fmds);

    
    //this is the backward compatibility way for tdaq
   // cout << "Using the old Constructor " << endl;
   // DataWriter dw(writePath,fName,rPar,fmds,1);


   //this is the offline way to do it
  // cout << "Using the new online Constructor " << endl;
  // DataWriter dw(writePath,fName,rPar,project,StreamType,StreamName,my_fname_>stream(),LumiBlockNumber,apName,fmds);



    //uncomment this for testing of freeName Constructor
   // DataWriter dw(writePath,fName,rPar,project,StreamType,StreamName,my_fname->stream(),LumiBlockNumber,apName,fmds);
    

    
    
    if (!dw.good()) {
      cout << "Problem opening file in directory "<<writePath<<"."<<endl;
      cout << "Is the directory there and writable for you?"<<endl;
      exit(1);
    }
    
    cout << "Event size in kB: ";
    unsigned int eventSize;
    cin >> eventSize; eventSize *= 1024;

    cout << "How many events in each file (0 means no limit): ";
    unsigned int eventFile=0;
    cin >> eventFile; 

    cout << "Maximum MB per file? (0 means no limit): ";
    unsigned int maxMB=0;
    cin >> maxMB; 

    cout << "Delay between events (float, in seconds): ";
    float evDelayF;
    cin >> evDelayF; 
    unsigned long evDelay = (unsigned long)(evDelayF*1000000);
    cout << "The delay is " << evDelay << " microseconds.\n";

    cout << "Write an index file? (y/n):";
    string inp("");
    cin >> inp; 

    ofstream indexFile; bool indexChoice(false);
    if(inp=="y") {
      cout << "Open index file in the current directory.\n";
      indexFile.open("writing.index",ios::out);
      indexChoice=true;
    } else {
      cout << "Do not write an index file.";
    } 

    std::vector<unsigned int *> events;
    for (int i = 0; i < 1000; ++i) {
      events.push_back(new unsigned int[eventSize/4]);
      for (unsigned int j=0; j<eventSize/4; ++j) { 
	events[i][j] = 0x1AAAAAA1+j+(i<<16);
      }
    }

    cout << endl;
    auto event = events.begin();
    
    cout << "Number of events: ";
    unsigned int nev;
    cin >> nev;
   
    dw.setMaxFileNE(eventFile);
    dw.setMaxFileMB(maxMB);

    cout << "How often to measure speed? Interval in seconds (<0 means no measurement):";   
    double tmeas=0.0;
    cin >> tmeas;
    if(tmeas<=0.0) { 
      tmeas = 0.0;
      cout << "No measurements of speed.\n";
    } else {
      cout << "Intermediate measuerement every " << tmeas << " seconds\n";
    }

    // call-back example
    cout << "Print file names? (y/n):"; // << endl;
    string ans="";
    cin >> ans;
    if(ans=="y") {
      cout << "Register call-back class to print file names." << endl;
      DWCBcout *pCB = new DWCBcout("testWritingSpeed");
      dw.registerCallBack(pCB);
    } else {
      cout << "File names not printed." << endl;
    }

    auto startTime = std::chrono::high_resolution_clock::now();
    auto previousTime = startTime;
    unsigned int pev=0;

    // save start time
    ofstream tstartFile("StartTime.dat",ios::out);
    tstartFile << setprecision(12) 
	       << startTime.time_since_epoch().count()
	       << endl;
    tstartFile.close();

    cout << "Starting the run now!\n";    

    for(unsigned int ev=0; ev<nev; ev++) 
    {
       if(evDelay) usleep(evDelay); 

       
       dw.putData(eventSize, *event);

       if(indexChoice) {
	 int64_t pos=dw.getPosition();
	 indexFile << ev << " " << pos << endl;
       }

       auto now = std::chrono::high_resolution_clock::now();
       std::chrono::duration<double> tdiff = now - previousTime;
       
       if(tmeas > 0 && tdiff.count() > tmeas) {
	 double tds = tdiff.count();
	 double mbps = (ev-pev)*eventSize/(1024*1024*tds);
	 double evps = (ev-pev)/tds;
	 std::chrono::duration<double> sincestart = (now-startTime);
	 cout << "Time " << sincestart.count();
	 cout << " interval " << tds << " s,";
	 cout << " event " << ev;
	 cout << " current rate " << mbps << " MB/s or " << evps << " Hz.\n"; 
	 previousTime = now; 
	 pev = ev;
       }

       ++event;
       if (event == events.end())
	 event = events.begin();

       if(!dw.good()) {
	 cout << "Can't write anymore! Stop after event "<<ev<<endl;
	 break;
       }
    }

    indexFile.close();

    auto now = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> tdiff = now - startTime;
    double tds = tdiff.count();
    cout << "Time of this run in seconds is " << tds << endl;
    double mb = (eventSize/1024)*nev; mb /= 1024;
    cout << "Total data written is "<< mb << " MB or " << nev << " events.\n";
    cout << "Average rate is "<< mb/tds << " MB/s";
    double evhz=nev; evhz /= tds;
    cout << " or "<<evhz<<" Hz."<<endl;


    for(auto& ev: events)
      delete[] ev;

    events.clear();

    //optionally, to test the reader
    //dw.closeFile();
}
