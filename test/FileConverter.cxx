/**
   \application to convert files with file format 2 into files with file format 5
*/


#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <stdint.h>

using namespace std;


int main (int argc, char *argv[]) {
  
  string inName="";
  
  if(argc>1) {
    inName = argv[1];
  } else {
    cout << "Please give file name" << endl;
    return 0;
  }
    
  ifstream infile(inName.c_str(), ios::binary);
  string outName = inName + ".converted";
  ofstream outfile (outName.c_str(),ios::out | ios::app | ios::binary);
    
  // get size of file
  ifstream::pos_type file_size;
  infile.seekg(0,ios::end);
  file_size=infile.tellg();
  infile.seekg(0,ios::beg);
  
  // allocate memory for file content
  uint32_t data = 0;
  uint32_t * buffer = &data; 
  char * new_buffer = 0;

  uint32_t format2=2;
  uint32_t format5=5;
  uint32_t params_size=10;
  uint32_t detMask2=0;

  int pos_format2=2;       // format version position in file_start_record
  int pos_params_size=1;   // record size position in run_parameters_record
  int pos_detMask2=7;      // detector_mask_2of2 position in run_parameters_record
  
  int counter1 = 0;
  int counter2 = 0;
  bool flag1 = false;
  bool flag2 = false;

      
  while(1){ 
    
    if(counter1 == pos_format2){
      infile.read ((char *)buffer,sizeof(uint32_t));
      if(*buffer==format2) {
	outfile.write((char *)&format5,sizeof(uint32_t));
	infile.seekg(0,ios::cur);
	counter1++;
	flag1 = false;
	continue;
      } else {
	cout << "Input file format " << *buffer << ", not 2 (2003-2007)" << endl;
	cout << "This converter cannot help you!" << endl;
	outfile.close();
	remove(outName.c_str());
	break;
      }
    }
    
    if(counter2==pos_params_size){
      infile.read ((char *)buffer,sizeof(uint32_t));
      outfile.write((char *)&params_size,sizeof(uint32_t));
      infile.seekg(0,ios::cur);
      counter2++;
      continue;
    }

    if(counter2==pos_detMask2){
      outfile.write ((char *)&detMask2,sizeof(uint32_t));
      ifstream::pos_type size = file_size - infile.tellg();
      new_buffer = new char [size];
      infile.read (new_buffer,size);
      outfile.write (new_buffer,size);
      break;
    }
    
    infile.read ((char *)buffer,sizeof(uint32_t));
    if(*buffer==0x1234aaaa){ flag1 = true; }
    if(*buffer==0x1234bbbb){ flag2 = true; }
    outfile.write ((char *)buffer,sizeof(uint32_t));        
    infile.seekg(0,ios::cur);
        
    if(flag1) counter1++;
    if(flag2) counter2++;
  }

  // release dynamically-allocated memory
  if(outfile.is_open()) {
    delete[] new_buffer;
    outfile.close();
  }
  
  infile.close();
  return 0;
  
}

