
#include "EventStorage/pickDataReader.h"
#include <iostream>
#include <string>

int main (void) {

  std::cout << "File to read: ";
  std::string fName;
  std::cin >> fName;

  EventStorage::DataReader* d = pickDataReader(fName);

  std::cout << "DataReader is ";
  if (d->good()) std::cout << "good";
  else std::cout << "bad";
  std::cout << std::endl;

  auto mask = d->detectorMask();

  uint64_t top = (mask >> 64).to_ullong();
  uint64_t bottom = (mask & std::bitset<128>(0xffffffffffffffff)).to_ullong(); 

  std::cout << "Detector mask " << std::hex <<  top 
	    << " " << bottom
	    << std::endl;

  return 0;

}
