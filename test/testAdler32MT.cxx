#include <zlib.h>
#include <random>
#include "src/Adler32MT.h"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE Write Tests
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <iostream>

namespace unit_tests {

// Friend of Adler32MT class for private members inspection
struct Adler32MT_inspector
{
	Adler32MT_inspector(Adler32MT & a32mt) : adler32mt(a32mt) {}
	Adler32MT & adler32mt;
	int64_t stats_nb_requests() const { return adler32mt.stats.nb_requests; }
	int64_t stats_nb_mt() const { return adler32mt.stats.nb_mt; }
	int64_t stats_total_size() const { return adler32mt.stats.total_size; }
	int64_t stats_mt_size() const { return adler32mt.stats.mt_size; }
	int64_t stats_nb_chunks() const { return adler32mt.stats.nb_chunks; }
	int mt_threshold() const { return adler32mt.mt_threshold; }
};

BOOST_AUTO_TEST_CASE(adler32mt_default_params)
{
	unsigned long checksum_reference, checksum_mt;
	std::random_device engine;

	// Generate random data
	// make sure to generate enough to trigger mt impl (i.e. more than threshold)
	std::vector<unsigned char> data(2 * Adler32MTDefaults::THRESHOLD);
	for (unsigned char & c: data) {
		c = engine() & 0xFF;
	}

	// Compute checksum via Zlib's adler32
	checksum_reference = ::adler32(0L, Z_NULL, 0);
	checksum_reference = ::adler32(checksum_reference, data.data(), data.size());

	// Compute checksum via multithread implementation
	/* By default Adler32MT object does not create additional threads for
	 * computation, for backward "compatibility"
	 */
	Adler32MT adler32mt;
	checksum_mt = ::adler32(0L, Z_NULL, 0);
	checksum_mt = adler32mt.adler32(checksum_mt, data.data(), data.size());

	// Check that both results are equal
	BOOST_CHECK_EQUAL(checksum_reference, checksum_mt);

	// Check stats
	Adler32MT_inspector inspector(adler32mt);
	BOOST_CHECK_EQUAL(1, inspector.stats_nb_requests());
	BOOST_CHECK_EQUAL(0, inspector.stats_nb_mt());
	BOOST_CHECK_EQUAL(data.size(), inspector.stats_total_size());
	BOOST_CHECK_EQUAL(0, inspector.stats_mt_size());
}

BOOST_AUTO_TEST_CASE(adler32mt_multithread)
{
	unsigned long checksum_reference, checksum_mt;
	std::random_device engine;

	// Generate random data
	// make sure to generate enough to trigger mt impl (i.e. more than threshold)
	std::vector<unsigned char> data(200 * Adler32MTDefaults::THRESHOLD);
	for (unsigned char & c: data) {
		c = engine() & 0xFF;
	}

	// Compute checksum via Zlib's adler32
	checksum_reference = ::adler32(0L, Z_NULL, 0);
	auto startts = std::chrono::high_resolution_clock::now();
	checksum_reference = ::adler32(checksum_reference, data.data(), data.size());
	auto endts = std::chrono::high_resolution_clock::now();
	std::cout << "alder32 reference (ns): " << (endts - startts).count() << std::endl;

	// Compute checksum via multithread implementation
	/* By default Adler32MT object does not create additional threads for
	 * computation, for backward "compatibility"
	 */
	Adler32MT adler32mt(std::thread::hardware_concurrency());
	checksum_mt = ::adler32(0L, Z_NULL, 0);
	startts = std::chrono::high_resolution_clock::now();
	checksum_mt = adler32mt.adler32(checksum_mt, data.data(), data.size());
	endts = std::chrono::high_resolution_clock::now();
	std::cout << "alder32 multithread (ns): " << (endts - startts).count() << std::endl;

	// Check that both results are equal
	BOOST_CHECK_EQUAL(checksum_reference, checksum_mt);

	// Check stats
	Adler32MT_inspector inspector(adler32mt);
	BOOST_CHECK_EQUAL(1, inspector.stats_nb_requests());
	BOOST_CHECK_EQUAL(1, inspector.stats_nb_mt());
	BOOST_CHECK_EQUAL(data.size(), inspector.stats_total_size());
	BOOST_CHECK_EQUAL(data.size(), inspector.stats_mt_size());
}

BOOST_AUTO_TEST_CASE(adler32mt_race_condition)
{
	unsigned long checksum_reference, checksum_mt;
	std::random_device engine;

	// Generate random data
	// make sure to generate enough to trigger mt impl (i.e. more than threshold)
	std::vector<unsigned char> data(200 * Adler32MTDefaults::THRESHOLD);
	for (unsigned char & c: data) {
		c = engine() & 0xFF;
	}

	// Compute checksum via Zlib's adler32
	checksum_reference = ::adler32(0L, Z_NULL, 0);
	checksum_reference = ::adler32(checksum_reference, data.data(), data.size());

	// Compute checksum via multithread implementation
	/* By default Adler32MT object does not create additional threads for
	 * computation, for backward "compatibility"
	 */
	Adler32MT adler32mt(std::thread::hardware_concurrency());

	for (int i = 0; i < 100; ++i) {
		checksum_mt = ::adler32(0L, Z_NULL, 0);
		checksum_mt = adler32mt.adler32(checksum_mt, data.data(), data.size());
		BOOST_CHECK_EQUAL(checksum_reference, checksum_mt);
	}
}

BOOST_AUTO_TEST_CASE(adler32mt_nb_chunks)
{
	std::array<unsigned char, 1024*1024> const data{};

	{
		Adler32MT adler32mt(2, 0);
		adler32mt.adler32(0, data.data(), 1);
		Adler32MT_inspector inspector(adler32mt);
		// no matter the data size: with a 0 threshold, nb_chunks must be the number of threads
		BOOST_CHECK_EQUAL(2, inspector.stats_nb_chunks());
	}

	{
		Adler32MT adler32mt(1, 10);
		adler32mt.adler32(0, data.data(), 100);
		// as long as data size != 0, as nb threads is 1, there must not be multithreading
		Adler32MT_inspector inspector(adler32mt);
		BOOST_CHECK_EQUAL(0, inspector.stats_nb_mt());
		BOOST_CHECK_EQUAL(0, inspector.stats_nb_chunks());
	}

	{
		Adler32MT adler32mt(2, 10);
		adler32mt.adler32(0, data.data(), 100);
		Adler32MT_inspector inspector(adler32mt);
		// 100/10 = 10, but nb threads is 2 so nb_chunks must not be greater than 2
		BOOST_CHECK_EQUAL(2, inspector.stats_nb_chunks());

		adler32mt.adler32(0, data.data(), 20);
		BOOST_CHECK_EQUAL(4, inspector.stats_nb_chunks());

		adler32mt.adler32(0, data.data(), 15);
		// too small resulting chunk
		BOOST_CHECK_EQUAL(4, inspector.stats_nb_chunks());

		adler32mt.adler32(0, data.data(), 30);
		BOOST_CHECK_EQUAL(6, inspector.stats_nb_chunks());
	}
}

} //namespace unit_tests
