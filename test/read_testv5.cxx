//Dear emacs, this is -*- c++ -*-

/**
 * @file read_test.cxx
 * @author <a href="mailto:Wainer.Vandelli@cern.ch">Wainer.Vandelli@cern.ch</a> 
 * $Author: $
 * $Revision: $
 * $Date: $
 * 
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE Read V5 Tests
#define BOOST_TEST_MAIN

#include <iostream>

#include <boost/test/unit_test.hpp>
#include "EventStorage/pickDataReader.h"
#include "EventStorage/EventStorageIssues.h"

#include "./test_common.h"
#include <algorithm>

BOOST_AUTO_TEST_CASE( read_file )
{
  
  std::string fname = "../test/datafile_v5.data";
  
  DataReader *pDR = pickDataReader(fname);

  BOOST_CHECK(pDR);
  BOOST_CHECK(pDR->good());
  
  BOOST_CHECK_EQUAL(pDR->detectorMask(), 0x12345678abcdefcd);
  BOOST_CHECK_EQUAL(pDR->triggerType(), 8);
  BOOST_CHECK_EQUAL(pDR->runNumber(), 123456);
  BOOST_CHECK_EQUAL(pDR->beamType(), 0);
  BOOST_CHECK_EQUAL(pDR->beamEnergy(), 14);

  BOOST_CHECK_EQUAL(pDR->compression(), EventStorage::NONE);

  while(pDR->good()){
    uint32_t eventSize;
    char * buf;
    
    DRError err = pDR->getData(eventSize,&buf);
    BOOST_CHECK_EQUAL(err, EventStorage::DROK);

    delete[] buf;
  }

  delete pDR;
}

